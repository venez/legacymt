% [s] = int_operator2_Laplace(level,dimInt,nInt,dimGreen,elemPts,nVecO,nVecS,elemSize)
% 
%   This function evaluates Green's function for specified dimension
%   'dimGreen' in 2D or 3D
%       'dimInt' = 1: \iint_T ( d�G([x;0;0])/dnO/dnS ) dTj
%       'dimInt' = 2: \iint_T ( d�G([x;y;0])/dnO/dnS ) dTj
%       'dimInt' = 3: \iint_T ( d�G([x;y;z])/dnO/dnS ) dTj
%	with the solution 's'.
%   Therefore it uses the Gaussian quadrature of order 'nInt' where T is a
%   triangle with vertices saved in 'elemPts'.
% 
%   Example:
%       s = int_operator2_Laplace('triangle',3,8,2,elemPts,nVecO,nVecS,elemSize)
% 
function [s] = int_operator2_Laplace(type,dimInt,nInt,dimGreen,elemPts,nVecO,nVecS,elemSize)


% Error handling
[mC,nC] = size(elemPts);
if (mC > nC)
    error('Last input argument must have a column major orientation');
end
if (dimInt > mC)
    error(['Integration problem exceeds given dimension ' ...
    '(specified argument dimInt <= rows of last but two input argument)']);
end


% Function assignment
if (mC == 2)
	fun = @(x,y) greenFct_LaplaceDerivative2(dimGreen,[x;y;0],nVecO,nVecS);
elseif (mC == 3)
    fun = @(x,y,z) greenFct_LaplaceDerivative2(dimGreen,[x;y;z],...
                                               nVecO,nVecS);
else
    error('Unknown format for last but two input argument');
end


% Execution
switch type
    case {'Line','LinE','LiNE','LINE','LINe','LIne','LiNe','LInE','lINE','line'}
        if (nargin < 8)
            s = int_line(fun,dimInt,nInt,elemPts(:,1), ...
                                         elemPts(:,2));
        else
            s = int_line(fun,dimInt,nInt,elemPts(:,1), ...
                                         elemPts(:,2), ...
                                         elemSize);
        end 
    case {'Triangle','triangle','TRI','Tri','tRi','trI','tri'}
        if (nargin < 8)
            s = int_triangle(fun,dimInt,nInt,elemPts(:,1), ...
                                             elemPts(:,2), ...
                                             elemPts(:,3));
        else
            s = int_triangle(fun,dimInt,nInt,elemPts(:,1), ...
                                             elemPts(:,2), ...
                                             elemPts(:,3), ...
                                             elemSize);
        end
    otherwise
        error('Unknown type for integration method');
end


end