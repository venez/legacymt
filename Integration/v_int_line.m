% [s] = int_line(fun,dim,N,a,b,L)
% 
%   Function integrates a given function_handle 'fun' for
%       'dim' = 1  :  \iint_T fun(x)     dTj
%       'dim' = 2  :  \iint_T fun(x,y)   dTj
%       'dim' = 3  :  \iint_T fun(x,y,z) dTj
%	with the solution 's'.
%   Therefore it uses the Gaussian quadrature of order 'N' (<= 12) for the 
%   line elements dTj between the group of vertices at points 'a' and 'b'.  
%   The element lengths 'L' can be specified optional.
% 
%   Example:
%   fun = @(x,y,i) 1/(x.*y);
%   a = [0 ; 1 ; 0
%        1 ; 2 ; 0];
%   b = [2 ; -3 ; 0
%        7 ; -5 ; 0];
%   s = int_line(fun,2,5,a,b);
% 
%   NOTE: function uses advantages matrix handling by MatLab, which means 
%         that a quantity of vertices can be specified instead of only 3.
%         However, this requires for the 'fun' argument an element-wise
%         calculation.
%         E.g.  false: fun=@(x,y) x*y
%               right: fun=@(x,y) x.*y
% 
function [s] = v_int_line(fun,dim,N,a,b,L)

% get quadrature points and weights as well as number of Gauss points
[pts,wts] = GaussPointsLine(N);
NP = length(wts); 

% identify dimension
[~,n] = size(a);

% calculate line length
if (nargin < 6)
    L = v_mag(b-a);
end

% evaluate given function at specified line points
fValue = zeros(n,NP);
switch dim
    case 1
        for i = 1:n
            gridPts = a(:,i).*(ones(1,NP) - pts) + ...
                      b(:,i).*pts;
            fValue(i,:) = fun(gridPts(1,:),zeros(1,NP),zeros(1,NP),...
                              NP).*wts;
        end
    case 2
        for i = 1:n
            gridPts = a(:,i).*(ones(1,NP) - pts) + ...
                      b(:,i).*pts;
            fValue(i,:) = fun(gridPts(1,:),gridPts(2,:),zeros(1,NP),...
                              NP).*wts;
        end
	case 3
        for i = 1:n
            gridPts = a(:,i).*(ones(1,NP) - pts) + ...
                      b(:,i).*pts;
            fValue(i,:) = fun(gridPts(1,:),gridPts(2,:),gridPts(3,:),...
                              NP).*wts;
        end
    otherwise
        error('Unknown dimension for integration function');
end
s = L.*sum(fValue,2)';
    
end