% [s] = int_line(fun,dim,N,a,b,L)
% 
%   Function integrates a given function_handle 'fun' for
%       'dim' = 1  :  \iint_T fun(x)     dTj
%       'dim' = 2  :  \iint_T fun(x,y)   dTj
%       'dim' = 3  :  \iint_T fun(x,y,z) dTj
%	with the solution 's'.
%   Therefore it uses the Gaussian quadrature of order 'N' (<= 12) for the 
%   line element dTj between vertices at points 'a' and 'b'. The element 
%   length 'L' can be specified optional.
% 
%   Example:
%   fun = @(x,y) 1/(x+y);
%   a = [0 ; 1 ; 0];
%   b = [2 ; -3 ; 0];
%   s = int_line(fun,2,5,a,b);
% 
function [s] = int_line(fun,dim,N,a,b,L)

% get quadrature points and weights
[pts,wts] = GaussPointsLine(N);
NP = length(wts); 

% identify dimension
[dimV,n] = size(a);
if (dimV == 2)
    a = [a;zeros(1,n)];
    b = [b;zeros(1,n)];
end

% calculate line length
if (nargin < 6)
    L = norm(b-a);
end

% evaluate given function at specified line points
s = 0.0;
switch dim
    case 1
        for i = 1:NP
            x = a(1)*(1-pts(i)) + b(1)*pts(i);
            s = s + fun(x)*wts(i);
        end
    case 2
        for i = 1:NP
            x = a(1)*(1-pts(i)) + b(1)*pts(i);
            y = a(2)*(1-pts(i)) + b(2)*pts(i);
            s = s + fun(x,y)*wts(i);
        end
    case 3
        for i = 1:NP
            x = a(1)*(1-pts(i)) + b(1)*pts(i);
            y = a(2)*(1-pts(i)) + b(2)*pts(i);
            z = a(3)*(1-pts(i)) + b(3)*pts(i);
            s = s + fun(x,y,z)*wts(i);
        end
    otherwise
        error('Unknown dimension for integration function');
end
s = L*s;
    
end