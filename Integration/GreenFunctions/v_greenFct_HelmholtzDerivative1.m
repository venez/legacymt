% [s] = greenFct_HelmholtzDerivative1(dim,x,k,n,i)
% 
%   Function gives the solution 's' as first derivative of Green's function 
%   G as fundamental solution for the Helmholtz equation in one, two or 
%   three dimensions 'dim' for specified values of 'x' and wavenumber 'k' 
%   in the direction of the normalized vector 'n' (at point 'x').
% 
%   Green function in use:      
%       d(G_1D)/dn = -1/2 exp(i * k * magnitude(x)) * v_dot(n,x) / ...
%                    magnitude(x)
%       d(G_2D)/dn = -1/k besselh(1,1,k * magnitude(x)) * v_dot(n,x) / ...
%                    magnitude(x)
%       d(G_3D)/dn = -1/4/pi * v_dot(n,x) / magnitude(x)^3 * ...
%                    (1 + i * k * magnitude(x)) * exp(-i * k * r)
% 
%   NOTE: function uses advantages of matrix handling by MatLab, which 
%         means that a group of position for 'x' can be specified. 
% 
function [s] = v_greenFct_HelmholtzDerivative1(dim,x,k,n,i)

if (nargin < 5)
	m = 1;
else
    m = i;
end
    
tmp = repmat(n,1,m);
r = v_mag(x);
switch dim
    case 1
        fac = -1/2;
        s   = fac .* exp(-1i .* k .* r) .* v_dot(tmp,x)./r;
    case 2
        fac = -1/k;
        s	= fac .* besselh(1,1,k .* r) .* v_dot(tmp,x)./r;
    case 3
        fac = -1/4/pi;
        s	= fac .* v_dot(tmp,x)./r.^3 .* (1 + 1i .* k .* r) .* ...
              exp(-1i .* k .* r);
    otherwise
        error('Unknown definition for Green dimension (dim argument)');
end

end