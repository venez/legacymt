% [s] = v_greenFct_Laplace(dim,x)
% 
%   Function Gives the solution 's' of Green's function G as fundamental 
%   solution for the Laplace equation in two or three dimensions 'dim' for 
%   a specified value of 'x'.
% 
%   Green functions in use:
%           G_2D = -1/2/pi .* ln(magnitude(x))
%           G_3D = 1/(4 pi magnitude(x))
% 
%   NOTE: function uses advantages of matrix handling by MatLab, which 
%         means that a group of position for 'x' can be specified.
% 
function [s] = v_greenFct_Laplace(dim,x)

switch dim
    case 2
        fac = -1/2/pi;
        s	= fac.*log(v_mag(x));
    case 3
        fac = +1/4/pi;
        s   = fac./v_mag(x);
    otherwise
    error('Unknown definition for Green dimension (dim argument)');
end

end