% [s] = v_greenFct_LaplaceDerivative2(dim,x,nO,nS,i,j)
% 
%   Function gives the solution 's' as second derivative of Green's 
%   function G as fundamental solution for the Laplace equation in two or 
%   three dimensions 'dim' for a specified value of 'x' in the direction of 
%   normalized vector 'nO' (at observer point) and 'nS' (at source point).
% 
%   Green functions in use:
%       d�(G_2D)/(dnO dnS) = +1/2/pi/magnitude(x)^3 * (dot(nS,nO) + ...
%                            2 * dot(x,nS)/magnitude(x) * ...
%                            dot(x,nO)/magnitude(x)
%       d�(G_3D)/(dnO dnS) = +1/4/pi/magnitude(x)^3 * (dot(nS,nO) + ...
%                            3 * dot(x,nS)/magnitude(x) * ...
%                            dot(x,nO)/magnitude(x)
% 
%   NOTE: function uses advantages of matrix handling by MatLab, which 
%         means that a group of position for 'x' can be specified. 
% 
function [s] = v_greenFct_LaplaceDerivative2(dim,x,nO,nS,i,j)

tmpO = repmat(nO(:,j),1,i);
tmpS = repmat(nS,1,i);

tmp = v_mag(x);
switch dim
    case 2
        fac = +1/2/pi./tmp.^2;
        s	= fac.* (v_dot(tmpO,tmpS) + ...
              2 .* v_dot(x,tmpS)./tmp .* v_dot(x,tmpO)./tmp);
    case 3
        fac = +1/4/pi./tmp.^3;
        s	= fac.* (v_dot(tmpO,tmpS) + ...
              3 .* v_dot(x,tmpS)./tmp .* v_dot(x,tmpO)./tmp);
    otherwise
        error('Unknown definition for Green dimension (dim argument)');
end

end