% [s] = greenFct_Helmholtz(dim,x,k)
% 
%   Function gives the solution 's' of Green's function G as fundamental 
%   solution for the Helmholtz equation in one, two, three or n dimensions 
%   'dim' for specified values of 'x' and wavenumber 'k'.
% 
%   Green function in use:      
%       G_2D = -i/4/pi * besselh(0,1,k*x)
%       G_3D = -1/4/pi * exp(-i k x)
%       G_nD = -(2*pi)^(-dim/2) * (k/x)^(dim/2-1) * besselY(dim/2-1,k*x);
% 
function [s] = greenFct_Helmholtz(dim,x,k)

r = norm(x);
if ((r == 0) && (dim == 2))
    error('Argument x will result in not a defined function');
end

switch dim
    case 1
        fac = +1i / 2 / k;
        s = fac * exp(1i * k * r);
    case 2
        fac = +1i / 4;
        s = fac * besselh(0,1,k * r);
    case 3
        fac = +1 / 4/pi;
        s   = fac * exp(-1i * k * r) / r;
    otherwise
        if (dim == 0)
            error('Unknown definition for Green dimension (dim argument)');
        end
        nu = dim/2 - 1;
        s = -(2*pi)^(-dim/2) * (k/r)^nu * bessely(nu,k*r);
end

end