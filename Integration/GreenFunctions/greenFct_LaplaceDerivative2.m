% [s] = greenFct_LaplaceDerivative2(dim,x,nO,nS)
% 
%   Function gives the solution 's' as second derivative of Green's 
%   function G as fundamental solution for the Laplace equation in two or 
%   three dimensions 'dim' for a specified value of 'x' in the direction of 
%   normalized vectors 'nO' (at observer point) and 'nS' (at source point).
% 
%	Green function in use:
%       d(G_2D)/(dnO dnS) = +1/2/pi/norm(x)^3 * (dot(nS,nO) + ...
%                           2 * dot(x,nS)/norm(x) * dot(x,nO)/norm(x)
%       d(G_3D)/(dnO dnS) = +1/4/pi/norm(x)^3 * (dot(nS,nO) + ...
%                           3 * dot(x,nS)/norm(x) * dot(x,nO)/norm(x)
% 
function [s] = greenFct_LaplaceDerivative2(dim,x,nO,nS)

r = norm(x);
switch dim
    case 2
        fac = +1/2/pi./r.^2;
        s	= fac .*(dot(nS,nO) + 2 .* dot(x,nO)./r .* dot(x,nS)./r);
    case 3
        fac = +1/4/pi./r.^3;
        s	= fac .*(dot(nS,nO) + 3 .* dot(x,nO)./r .* dot(x,nS)./r);
    otherwise
        error('Unknown definition for Green dimension (dim argument)');
end
    
end