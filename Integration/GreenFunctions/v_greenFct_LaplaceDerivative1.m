% [s] = v_greenFct_LaplaceDerivative1(dim,x,n,i)
% 
%   Function gives the solution 's' as first derivative of Green's function 
%   G as fundamental solution for the Laplace equation in two or three 
%   dimensions 'dim' for a specified value of 'x' in the direction of 
%   normalized vector 'n'.
% 
%   Green functions in use:
%       d(G_2D) / dn = -1/2/pi * dot(n,x) / magnitude(x)^2
%       d(G_3D) / dn = -1/4/pi * dot(n,x) / magnitude(x)^3
% 
%   NOTE: function uses advantages of matrix handling by MatLab, which 
%         means that a group of position for 'x' can be specified. 
% 
function [s] = v_greenFct_LaplaceDerivative1(dim,x,n,i)

if (nargin < 4)
	m = 1;
else
    m = i;
end

tmp = repmat(n,1,m);
switch dim
    case 2
        fac = -1/2/pi;
        s	= fac.*v_dot(tmp,x)./v_mag(x).^2;
    case 3
        fac = -1/4/pi;
        s	= fac.*v_dot(tmp,x)./v_mag(x).^3;
    otherwise
        error('Unknown definition for Green dimension (dim argument)');
end
    
end