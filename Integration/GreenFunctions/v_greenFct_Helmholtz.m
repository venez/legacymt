% [s] = v_greenFct_Helmholtz(dim,x,k)
% 
%   Function gives the solution 's' of Green's function G as fundamental 
%   solution for the Helmholtz equation in one, two or three or n 
%   dimensions 'dim' for specified values of 'x' and wavenumber 'k'.
% 
%   Green function in use:      
%       G_2D = -i/4/pi * besselh(0,1,k*magnitude(x))
%       G_3D = -1/4/pi * exp(-i k x)
%       G_nD = -(2*pi)^(-dim/2) * (k/magnitude(x))^(dim/2-1) * ...
%              bessely(dim/2-1,k*magnitude(x));
% 
%   NOTE: function uses advantages of matrix handling by MatLab, which 
%         means that a group of position for 'x' can be specified. 
% 
function [s] = v_greenFct_Helmholtz(dim,x,k)

r = v_mag(x);
switch dim
    case 1
        fac = +1i ./ 2 ./ k;
        s = fac * exp(1i .* k .* r);
    case 2
        fac = -1i / 4 / pi;
        s   = fac .* besselh(0,1,k*r);
    case 3
        fac = -1 / 4 / pi;
        s   = fac .* exp(-1i .* k .* r) ./ r;
    otherwise
        if (dim == 0)
            error('Unknown definition for Green dimension (dim argument)');
        end
        nu = dim/2 - 1;
        s = -(2*pi)^(-dim/2) * (k./r)^nu .* bessely(nu,k.*r);
end

end