% [s] = greenFct_HelmholtzDerivative1(dim,x,k,n)
% 
%   Function gives the solution 's' as first derivative of Green's function 
%   G as fundamental solution for the Helmholtz equation in one, two, three 
%   or n dimensions 'dim' for specified values of 'x' and wavenumber 'k' in 
%   the direction of the normalized vector 'n' (at point 'x').
% 
%   Green function in use:      
%       d(G_1D)/dn = -1/2 exp(i * k * norm(x)) * dot(n,x) / norm(x)
%       d(G_2D)/dn = -1/k besselh(1,1,k * norm(x)) * dot(n,x) / norm(x)
%       d(G_3D)/dn = -1/4/pi * dot(n,x) / norm(x)^3 * ...
%                    (1 + i * k * norm(x)) * exp(-i * k * r)
% 
function [s] = greenFct_HelmholtzDerivative1(dim,x,k,n)

r = norm(x);
switch dim
    case 1
        fac = -1/2;
        s   = fac* exp(-1i * k * r) * dot(n,x)/r;
    case 2
        fac = -1/k;
        s	= fac * besselh(1,1,k * r) * dot(n,x)/r;
    case 3
        fac = -1/4/pi;
        s	= fac*dot(n,x)/r^3 * (1 + 1i * k * r)*exp(-1i * k * r);
    otherwise
        error('Unknown definition for Green dimension (dim argument)');
end

end