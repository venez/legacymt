% [s] = greenFct_Laplace(dim,x)
% 
%   Function gives the solution 's' of Green's function G as fundamental 
%   solution for the Laplace equation in two or three dimensions 'dim' for 
%   a specified value of 'x'.
% 
%   Green function in use:
%           G_2D = -1/2/pi * ln(norm(x))
%           G_3D = 1/(4 pi norm(x))
% 
function [s] = greenFct_Laplace(dim,x)

r = norm(x);
switch dim
    case 2
        fac = -1/2/pi;
        s	= fac*log(r);
    case 3
        fac = +1/4/pi;
        s   = fac/r;
    otherwise
        error('Unknown definition for Green dimension (dim argument)');
end

end