% [s] = greenFct_LaplaceDerivative1(dim,x,n)
% 
%   Function gives the solution 's' as first derivative of Green's function
%   G as fundamental solution for the Laplace equation in two or three 
%   dimensions 'dim' for a specified value of 'x' in the direction of the 
%   normalized vector 'n' (at point 'x').
% 
%	Green function in use:
%       d(G_2D) / dn = -1/2/pi * dot(n,x) / norm(x)^2
%       d(G_3D) / dn = -1/4/pi * dot(n,x) / norm(x)^3
% 
function [s] = greenFct_LaplaceDerivative1(dim,x,n)

r = norm(x);
switch dim
    case 2
        fac = -1/2/pi;
        s	= fac*dot(n,x)/r.^2;
    case 3
        fac = -1/4/pi;
        s	= fac*dot(n,x)/r.^3;
    otherwise
        error('Unknown definition for Green dimension (dim argument)');
end
    
end