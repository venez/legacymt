% [s] = int_triangle(fun,dim,N,a,b,c,S)
% 
%   Function integrates a given function_handle 'fun' for
%       'dim' = 1  :  \iint_T fun(x)     dTj
%       'dim' = 2  :  \iint_T fun(x,y)   dTj
%       'dim' = 3  :  \iint_T fun(x,y,z) dTj
%	with the solution 's'.
%   Therefore it uses the Gaussian quadrature of order 'N' for the triangle
%   element dTj between vertices at points 'a', 'b' and 'c'. The element 
%   size 'S' can be specified optional.
% 
%   Example:
%   fun = @(x,y) 1/(x+y);
%   a = [0 ; 1 ; 0];
%   b = [2 ; -3 ; 0];
%   c = [7 ; 4 ; 0];
%   s = int_triangle(fun,2,5,a,b,c);
% 
function [s] = int_triangle(fun,dim,N,a,b,c,S)

% get quadrature points and weights as well as number of Gauss points
[pts,wts] = GaussPointsTriangle(N);
NP = length(wts); 

% identify dimension
[dimV,~] = size(a);
if (dimV == 2)
    a = [a;0];
    b = [b;0];
    c = [c;0];
end

% calculate triangle size
if (nargin < 7) 
    S = norm(cross(b-a,c-a)) / 2.0;
end

% evaluate given function at specified triangle points
s = 0.0;
switch dim
    case 1
        for i = 1:NP
            x = a(1)*(1-pts(1,i) - pts(2,i)) + ...
                b(1)*pts(1,i) + c(1)*pts(2,i);
            s = s + fun(x)*wts(i);
        end
    case 2
        for i = 1:NP
            x = a(1)*(1-pts(1,i) - pts(2,i)) + ...
                b(1)*pts(1,i) + c(1)*pts(2,i);
            y = a(2)*(1-pts(1,i) - pts(2,i)) + ...
                b(2)*pts(1,i) + c(2)*pts(2,i);
            s = s + fun(x,y)*wts(i);
        end
    case 3
        for i = 1:NP
            x = a(1)*(1-pts(1,i) - pts(2,i)) + ...
                b(1)*pts(1,i) + c(1)*pts(2,i);
            y = a(2)*(1-pts(1,i) - pts(2,i)) + ...
                b(2)*pts(1,i) + c(2)*pts(2,i);
            z = a(3)*(1-pts(1,i) - pts(2,i)) + ...
                b(3)*pts(1,i) + c(3)*pts(2,i);
            s = s + fun(x,y,z)*wts(i);
        end
    otherwise
        error('Unknown dimension for integration function');
end

s = S*s;

    
end