% [s] = v_int_triangle(fun,dim,N,a,b,c,S)
% 
%   Function integrates a given function_handle 'fun' for
%       'dim' = 1  :  \iint_T fun(x)     dTj
%       'dim' = 2  :  \iint_T fun(x,y)   dTj
%       'dim' = 3  :  \iint_T fun(x,y,z) dTj
%	with the solution 's'.
%   Therefore it uses the Gaussian quadrature of order 'N' (<= 12) for the 
%   triangle elements dTj between the group of vertices at points 'a', 'b' 
%   and 'c'. The element sizes 'S' can be specified optional.
% 
%   Example:
%   fun = @(x,y,i) 1/(x.*y);
%   a = [0 ; 1 ; 0
%        1 ; 2 ; 0];
%   b = [2 ; -3 ; 0
%        7 ; -5 ; 0];
%   c = [7 ; 4 ; 0
%        4 ; 8 ; 0];
%   s = int_triangle(fun,2,5,a,b,c);
% 
%   NOTE: function uses advantages matrix handling by MatLab, which means 
%         that a quantity of vertices can be specified instead of only 3.
%         However, this requires for the 'fun' argument an element-wise
%         calculation.
%         E.g.  false: fun=@(x,y)   x*y
%               right: fun=@(x,y,i) x.*y
% 
function [s] = v_int_triangle(fun,dim,N,a,b,c,S)

% get quadrature points and weights as well as number of Gauss points
[pts,wts] = GaussPointsTriangle(N);
NP = length(wts);
pts = [pts; (ones(1,NP) - pts(1,:) - pts(2,:))];

% identify dimension
[~,n] = size(a);

% calculate triangle size
if (nargin < 7) 
    S = v_mag(v_cross(b-a,c-a)) ./ 2.0;
end

% evaluate given function at specified triangle points
fValue = zeros(n,NP);
switch dim
    case 1
        for i = 1:n
            gridPts = a(:,i).*pts(3,:) + ...
                      b(:,i).*pts(1,:) + ...
                      c(:,i).*pts(2,:);
            fValue(i,:) = fun(gridPts(1,:),zeros(1,NP),zeros(1,NP),...
                              NP,i).*wts;
        end
	case 2
        for i = 1:n
            gridPts = a(:,i).*pts(3,:) + ...
                      b(:,i).*pts(1,:) + ...
                      c(:,i).*pts(2,:);
            fValue(i,:) = fun(gridPts(1,:),gridPts(2,:),zeros(1,NP),...
                              NP,i).*wts;
        end
	case 3
        for i = 1:n            
            gridPts = a(:,i).*pts(3,:) + ...
                      b(:,i).*pts(1,:) + ...
                      c(:,i).*pts(2,:);
            fValue(i,:) = fun(gridPts(1,:),gridPts(2,:),gridPts(3,:),...
                              NP,i).*wts;
        end
    otherwise
    	error('Unknown dimension for integration function');
end

s = S.*sum(fValue,2)';

end