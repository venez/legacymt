% [s] = int_operator0_Helmholtz(level,dimInt,nInt,dimGreen,elemPts,elemSize,waveN)
% 
%   This function evaluates Green's function for specified dimension
%   'dimGreen' in 2D or 3D
%       'dimInt' = 1: \iint_T G([x;0;0]) dTj
%       'dimInt' = 2: \iint_T G([x;y;0]) dTj
%       'dimInt' = 3: \iint_T G([x;y;z]) dTj
%	with the solution 's'.
%   Therefore it uses the Gaussian quadrature of order 'nInt' where T is a
%   triangle with vertices saved in 'elemPts'.
% 
%   Example:
%       k = 0.5
%       s = int_operator0_Helmholtz('triangle',3,8,2,elemPts,elemSize,k)
% 
function [s] = int_operator0_Helmholtz(type,dimInt,nInt,dimGreen,elemPts,elemSize,waveN)


% Error handling
[mC,nC] = size(elemPts);
if (mC > nC+1)
    error('elemPts input argument must have a column major orientation');
end
if (dimInt > mC)
    error(['Integration problem exceeds given dimension ' ...
    '(specified argument dimInt <= rows of last input argument)']);
end


% Function assignment
if (mC == 2)
	fun = @(x,y) greenFct_Helmholtz(dimGreen,[x;y;0],waveN);
elseif (mC == 3)
    fun = @(x,y,z) greenFct_Helmholtz(dimGreen,[x;y;z],waveN);
else
    error('Unknown format for last input argument');
end


% Execution
switch type
    case {'Line','LinE','LiNE','LINE','LINe','LIne','LiNe','LInE','lINE','line'}
        if (nargin < 6)
            s = int_line(fun,dimInt,nInt,elemPts(:,1), ...
                                         elemPts(:,2));
        else
            s = int_line(fun,dimInt,nInt,elemPts(:,1), ...
                                         elemPts(:,2), ...
                                         elemSize);
        end
    case {'Triangle','triangle','TRI','Tri','tRi','trI','tri'}
        if (nargin < 6)
            s = int_triangle(fun,dimInt,nInt,elemPts(:,1), ...
                                             elemPts(:,2), ...
                                             elemPts(:,3));
        else
            s = int_triangle(fun,dimInt,nInt,elemPts(:,1), ...
                                             elemPts(:,2), ...
                                             elemPts(:,3), ...
                                             elemSize);
        end
    otherwise
        error('Unknown type for integration method');
end


end