% [s] = int_operator1_Helmholtz(type,dimInt,nInt,dimGreen,elemPts,elemVec,elemSize,waveN)
% 
%   This function evaluates Green's function for specified dimension
%   'dimGreen' in 2D or 3D
%       'dimInt' = 1: \iint_T ( dG([x;0;0])/dNj ) dTj
%       'dimInt' = 2: \iint_T ( dG([x;y;0])/dNj ) dTj
%       'dimInt' = 3: \iint_T ( dG([x;y;z])/dNj ) dTj
%	with the solution 's'.
%   Therefore it uses the Gaussian quadrature of order 'nInt' where Tj is a
%   triangle with vertices saved in 'ptsTri'. As a unit vector Nj is normal
%   to Tj and stored in 'nVecTri'.
% 
%   Example:
%       k = 0.5
%       s = int_operator1_Helmholtz('triangle',3,8,2,elemPts,elemVec,elemSize,k)
% 
function [s] = int_operator1_Helmholtz(type,dimInt,nInt,dimGreen,elemPts,elemVec,elemSize,waveN)


% Error handling
[mC,nC] = size(elemPts);
if (mC > nC)
    error('Last input argument must have a column major orientation');
end
if (dimInt > mC)
    error(['Integration problem exceeds given dimension ' ...
    '(specified argument dimInt <= rows of penultimate input argument)']);
end


% Function assignment
if (mC == 2)
    fun = @(x,y) greenFct_HelmholtzDerivative1(dimGreen,[x;y;0], ...
                                               waveN,elemVec);
elseif (mC == 3)
    fun = @(x,y,z) greenFct_HelmholtzDerivative1(dimGreen,[x;y;z], ...
                                               waveN,elemVec);
else
    error('Unknown format for penultimate input argument');
end


% Execution
switch type
    case {'Line','LinE','LiNE','LINE','LINe','LIne','LiNe','LInE','lINE','line'}
        if (nargin < 7)
            s = int_line(fun,dimInt,nInt,elemPts(:,1), ...
                                         elemPts(:,2));
        else
            s = int_line(fun,dimInt,nInt,elemPts(:,1), ...
                                         elemPts(:,2), ...
                                         elemSize);
        end
    case {'Triangle','triangle','TRI','Tri','tRi','trI','tri'}
        if (nargin < 7)
            s = int_triangle(fun,dimInt,nInt,elemPts(:,1), ...
                                             elemPts(:,2), ...
                                             elemPts(:,3));
        else
            s = int_triangle(fun,dimInt,nInt,elemPts(:,1), ...
                                             elemPts(:,2), ...
                                             elemPts(:,3), ...
                                             elemSize);
        end
    otherwise
        error('Unknown type for integration method');
end


end