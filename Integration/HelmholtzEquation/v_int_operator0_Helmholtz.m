% [s] = v_int_operator0_Helmholtz(type,dimInt,nInt,dimGreen,elemPts,elemSize,waveN)
% 
%   NOTE: function uses advantages matrix handling by MatLab, which means 
%         that a quantity of vertices can be specified instead of only 3.
%         However, this requires for the 'fun' argument an element-wise
%         calculation.
%                   E.g.  false: fun=@(x,y) x*y
%                         right: fun=@(x,y) x.*y
%         Furthermore the argument 'element' must be a matrix of '6 x n'
%         (2D) or '9 x n' (3D) shape.
% 
%   This function evaluates Green's function for specified dimension
%   'dimGreen' in 2D or 3D
%       'dimInt' = 1: \iint_T G([x]) dTj
%       'dimInt' = 2: \iint_T G([x;y]) dTj
%       'dimInt' = 3: \iint_T G([x;y;z]) dTj
%	with the solution 's'.
%   Therefore it uses the Gaussian quadrature of order 'nInt' where T is a
%   triangle with vertices saved in 'elemPts'.
% 
%   Example:    
%       k = 0.5
%       s = v_int_operator0_Helmholtz('triangle',3,8,2,elemPts,elemSize,k)
% 
function [s] = v_int_operator0_Helmholtz(type,dimInt,nInt,dimGreen,elemPts,elemSize,waveN)


% Error handling
[mC,nC] = size(elemPts);
if (mC > 3*nC)
    error('elemPts input argument must have a column major orientation');
end
if (dimInt > mC/2)
    error(['Integration problem exceeds given dimension ' ...
    '(specified argument dimInt <= rows of last input argument)']);
end


% Function assignment
fun = @(x,y,z,i,j) v_greenFct_Helmholtz(dimGreen,[x;y;z],waveN);


% Execution
errorMsg = 'Unknown format for elemPts input argument';
switch type
    case {'Line','LinE','LiNE','LINE','LINe','LIne','LiNe','LInE','lINE','line'}
        if (mC == 4) || (mC == 6)
            twoQ = (mC == 4);
        else
            error(errorMsg);
        end
        
        if (nargin < 6)
            s = v_int_line(fun,dimInt,nInt,...
                            elemPts(1-0*twoQ:3-1*twoQ,:), ...
                            elemPts(4-1*twoQ:6-2*twoQ,:));
        else
            s = v_int_line(fun,dimInt,nInt,...
                            elemPts(1-0*twoQ:3-1*twoQ,:), ...
                            elemPts(4-1*twoQ:6-2*twoQ,:), ...
                            elemSize);
        end
    case {'Triangle','triangle','TRI','Tri','tRi','trI','tri'}
        if (mC == 6) || (mC == 9)
            twoQ = (mC == 6);
        else
            error(errorMsg);
        end
        
        if (nargin < 6)
            s = v_int_triangle(fun,dimInt,nInt,...
                                elemPts(1-0*twoQ:3-1*twoQ,:), ...
                                elemPts(4-1*twoQ:6-2*twoQ,:), ...
                                elemPts(7-2*twoQ:9-3*twoQ,:));
        else
            s = v_int_triangle(fun,dimInt,nInt,...
                                elemPts(1-0*twoQ:3-1*twoQ,:), ...
                                elemPts(4-1*twoQ:6-2*twoQ,:), ...
                                elemPts(7-2*twoQ:9-3*twoQ,:), ...
                                elemSize);
        end
    otherwise
        error('Unknown type for integration method');
end


end