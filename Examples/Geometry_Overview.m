clearvars
close all
clc


%% Add nessessary MATLAB files 
workDir = 'F:\MatLab\#Workspace\';
addpath(genpath([workDir 'Geometry\']));


%% SETTINGS
category='sphere';

shape='segment';

options.showBarQ = false;
options.showCenQ = false;
options.showGeoQ = true;
options.showPtsQ = false;
options.showVecQ = false;


%% PREPARATIONS
% declare possible categories
categories.arc = {'arc','Arc'};
categories.arch = {'arch','Arch'};
categories.contour = {'contour','Contour'};
categories.cuboid = {'cuboid','Cuboid'};
categories.custom = {'custom','Custom'};
categories.cylinder = {'cylinder','Cylinder'};
categories.grid = {'grid','Grid'};
categories.plate = {'plate','Plate'};
categories.sphere = {'sphere','Sphere'};

% declare possible shapes
shapes.circle = {'circle','Circle'};
shapes.circleAffined = {'circleAffined','CircleAffined'};
shapes.coneTruncated = {'coneTruncated','ConeTruncated'};
shapes.digon = {'digon','Digon'};
shapes.ellipse = {'ellipse','Ellipse'};
shapes.ellipsoid = {'ellipsoid','Ellipsoid'};
shapes.line = {'line','Line'};
shapes.naca = {'naca','NACA'};
shapes.none = {'','empty','Empty'};
shapes.parallelogram = {'parallelogram','Parallelogram'};
shapes.pipe = {'pipe','Pipe'};
shapes.rectangle = {'rectangle','Rectangle'};
shapes.revolution = {'bodyOfRevolution'};
shapes.rhombus = {'rhombus','Rhombus'};
shapes.segment = {'segment','Segment'};
shapes.rounded = {'rounded','Rounded'};
shapes.wave = {'waveAiryInfinite','WaveAiryInfinite'};


%% GEOMETRY
if ~ischar(category)
    category = char(category);
end
if ~ischar(shape)
    shape = char(shape);
end
getPossibleShapes(category,categories,shapes);

switch category
    case categories.arc
         switch shape
             case shapes.circle
                [pts,nodes,cen,vec] = geom_2D_arc_circle;
             case shapes.ellipse
                 [pts,nodes,cen,vec] = geom_2D_arc_ellipse;
             otherwise
                showErrow(categories.arc(1));
         end
    case categories.arch
         switch shape
             case shapes.circle
                [pts,nodes,cen,vec] = geom_3D_arch_circle;
             case shapes.ellipse
                 [pts,nodes,cen,vec] = geom_3D_arch_ellipse;
             otherwise
                showErrow(categories.arch(1));
         end
    case categories.contour
         switch shape
            case shapes.circle
                [pts,nodes,cen,vec] = geom_2D_contour_circle;
            case shapes.circleAffined
                [pts,nodes,cen,vec] = geom_2D_contour_circleAffined;
            case shapes.ellipse
                [pts,nodes,cen,vec] = geom_2D_contour_ellipse;
            case shapes.line
                [pts,nodes,cen,vec] = geom_2D_contour_line;
            case shapes.naca
                [pts,nodes,cen,vec] = geom_2D_contour_naca;
            case shapes.parallelogram
                [pts,nodes,cen,vec] = geom_2D_contour_parallelogram;
            case shapes.rectangle
                [pts,nodes,cen,vec] = geom_2D_contour_rectangle;
            case shapes.rhombus
                [pts,nodes,cen,vec] = geom_2D_contour_rhombus;
            otherwise
                showErrow(categories.plate(1));
        end
    case categories.cuboid
        switch shape
            case shapes.none
                [pts,nodes,cen,vec] = geom_3D_cuboid;
            case shapes.rounded
                [pts,nodes,cen,vec] = geom_3D_cuboid_rounded;
            otherwise
                showErrow(categories.cuboid(1));
        end
    case categories.custom
        switch shape
            case shapes.revolution
                [pts,nodes,cen,vec] = ...
                    geom_3D_custom_bodyOfRevolution(0:.1:1,1:.1:2);
            case shapes.coneTruncated
                [pts,nodes,cen,vec] = geom_3D_custom_coneTruncated;
            case shapes.ellipsoid
                [pts,nodes,cen,vec] = geom_3D_custom_ellipsoid;
            case shapes.pipe
                [pts,nodes,cen,vec] = geom_3D_custom_pipe;
            case shapes.wave
                [pts,nodes,cen,vec] = geom_3D_custom_waveAiryInfinite;
            otherwise
                showErrow(categories.custom(1));
        end
    case categories.cylinder
        switch shape
            case shapes.none
                [pts,nodes,cen,vec] = geom_3D_cylinder;
            case shapes.ellipse
                [pts,nodes,cen,vec] = geom_3D_cylinder_ellipse;
            case shapes.naca
                [pts,nodes,cen,vec] = geom_3D_cylinder_naca;
            otherwise
                showErrow(categories.cylinder(1));
        end
    case categories.grid
        switch shape
            case shapes.circle
                [X,Y,Z] = geom_2D_grid_circle([2 1],[24 12]);
            case shapes.ellipse
                [X,Y,Z] = geom_2D_grid_ellipse([4 3],[2 1],[24 12]);
            case shapes.parallelogram
                [X,Y,Z] = geom_2D_grid_parallelogram([6 5],[5 3],[pi/4 pi/4],[10 8],[6 4]);
            case shapes.rectangle
                [X,Y,Z] = geom_2D_grid_rectangle([4 3],[2 1],[10 8],[6 4]);
            otherwise
                showErrow(categories.grid(1));
        end
    case categories.plate
        switch shape
            case shapes.circle
                [pts,nodes,cen,vec] = geom_2D_plate_circle;
            case shapes.ellipse
                [pts,nodes,cen,vec] = geom_2D_plate_ellipse;
            case shapes.naca
                [pts,nodes,cen,vec] = geom_2D_plate_naca;
            case shapes.parallelogram
                [pts,nodes,cen,vec] = geom_2D_plate_parallelogram;
            case shapes.rectangle
                [pts,nodes,cen,vec] = geom_2D_plate_rectangle;
            case shapes.rhombus
                [pts,nodes,cen,vec] = geom_2D_plate_rhombus;
            otherwise
                showErrow(categories.plate(1));
        end
    case categories.sphere
        switch shape
            case shapes.digon
                [pts,nodes,cen,vec] = geom_3D_sphere_digon;
            case shapes.none
                [pts,nodes,cen,vec] = geom_3D_sphere;
            case shapes.segment
                [pts,nodes,cen,vec] = geom_3D_sphere_segment;
            otherwise
                showErrow(categories.sphere(1));
        end
    otherwise
end


%% VISUALIZATION
if (ismember(category,categories.grid))
    plot3(X,Y,Z,'bx');
else
    geom_show(pts,nodes,cen,vec,1,options);
end


%% SUPPORT FUNCTIONS
function getPossibleShapes(target,src,base)
    switch target
        case src.arc
            s = [base.circle base.ellipse];
        case src.arch
            s = [base.circle base.ellipse];
        case src.contour
            s = [base.circle base.circleAffined base.ellipse base.line ...
                base.naca base.parallelogram base.rectangle base.rhombus];
        case src.cuboid
            s = [base.none base.rounded];
        case src.custom
            s = [base.revolution base.coneTruncated base.ellipsoid ...
                base.pipe base.wave];
        case src.cylinder
            s = [base.none base.ellipse base.naca];
        case src.grid
            s = [base.circle base.ellipse base.parallelogram ...
                base.rectangle];
        case src.plate
            s = [base.circle base.ellipse base.naca base.parallelogram ...
                base.rectangle base.rhombus];
        case src.sphere
            s = [base.digon base.none base.segment];
        otherwise
            error('Unknown choice as category');
    end
    disp(['Following choices are valid for the geometry: ' target]);
    s'
end

function showErrow(category)
    error(['Unknown shape for ' char(category) ' geometry']);
end