clearvars
close all
clc


%% Add nessessary MATLAB files 
workDir = 'F:\MatLab\#Workspace\';
addpath(genpath([workDir 'Geometry\']));
addpath(genpath([workDir 'Integration\']));
addpath([workDir 'AddedInertia\'], ...
        [workDir 'BEM\'], ...
        [workDir 'Input\'], ...
        [workDir 'Mathematics\'], ...
        [workDir 'Output\'] ...
        );


%% GEOMETRY
% Setup
radius  = 1.0;
divC = 35;
divH = 35;

% Creation
[pts,nodes,cen,vec,sizes] = geom_3D_sphere(radius,divC,divH);

% Modifications
% none

% Controling
options.showVecQ = true;
geom_show(pts,nodes,cen,vec,1,options);


%% ADDED MASSES BY THE BOUNDARY ELEMENT METHOD
clear options;
options.type = 'tri';
options.nInt = 5;
options.dimG = 3;
options.dimI = 3;
options.prec = 'single';
options.save = '';
options.prop = 'explicit';

[addedMass,pot,BC] = bem_v_addedMass(pts,nodes,cen,vec,sizes,options);


%% COMPARISON TO THEORY

simulation = 1000* diag(addedMass);
theory     = 1000* 2/3 * pi * radius^3;
% theory according to A. Korotkin: Added Mass of Ship Structures, pp. 86, 
% where the fluid density is chosen with 1000 kg/m�

disp('Result comparison:');
simulation(1:3)'
theory * ones(1,3) 


%% VISUALIZATION
clear options;
options.barName = 'Potential for a_11';
options.showCenQ = true;
options.showGeoQ = true;
options.showPtsQ = false;
options.showVecQ = true;
geom_show(pts,nodes,cen,vec,pot(:,1)',options);