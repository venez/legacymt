% [elemSize] = geom_op_calcElemSize(elemPts,elemNodes,true)
% [elemSize] = geom_op_calcElemSize(elemVec,elemNodes,false)
% 
% Calculates the individual element sizes 'elemSize' for their given points 
% 'elemPts' and nodes 'elemNodes'. The functions distinguish automatically
% between the element shapes 'lines', 'triangles' and 'quads'.
% 
%   Example:
%       sizes = geom_op_calcElemSize(pts,nodes,true);
%       sizes = geom_op_calcElemSize(vec,nodes,false);
% 
function [elemSize] = geom_op_calcElemSize(elemPts,elemNodes,ptsQ)

usePtsQ = true;
if (nargin > 2)
    usePtsQ = ptsQ;
end

[m,~] = size(elemNodes);
if usePtsQ
    switch m
        case 2 % lines
            elemSize = v_mag(...
                    elemPts(:,elemNodes(2,:)) - elemPts(:,elemNodes(1,:)));
        case 3 % triangles
            elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
            elemSize = v_mag(elemVec) / 2.0;
        case 4  % quads
            elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
            elemSize = v_mag(elemVec);
        otherwise
            error('Unknown element shape')       
    end
else
    elemVec = elemPts;
    switch m
        case 2 % lines
            error('Unknown element shape for given input arguments');
        case 3 % triangles
            elemSize = v_mag(elemVec) / 2.0;
        case 4  % quads
            elemSize = v_mag(elemVec);
        otherwise
            error('Unknown element shape')       
    end
end
end