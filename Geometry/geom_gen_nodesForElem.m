% nodes = geom_gen_nodesForElem(type,n,firstNode,secondNode,offset,opts)
% 
%   Detailed explanation goes here (TODO)
% 
function nodes = geom_gen_nodesForElem(type,n,firstNode,secondNode,offset,opts)

default.clockQ = true;
default.closedQ = true;
switch nargin
    case 5
        options = default;
    case 6
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end

switch type
    case {2,'Line','LinE','LiNE','LINE','LINe','LIne','LiNe','LInE','lINE','line'}
        tmp1 = [firstNode  : n - offset;...
                secondNode : n - offset + 1];
        
        if options.closedQ
            tmp1(2,end) = firstNode;
        end
        tmp2 = [];
    case {3,'Triangle','triangle','TRI','Tri','tRi','trI','tri'}
        thirdNode = secondNode + 1;
    
        tmp1 = [firstNode   : secondNode - offset;...
                thirdNode   : thirdNode + n - 1;...
                secondNode  : thirdNode + n - 2];

        tmp2 = [firstNode      : secondNode - offset;...
                firstNode + 1  : thirdNode - offset;...
                secondNode + 1 : thirdNode + n - 1];

        if options.closedQ
            tmp1(2,end) = secondNode;
            tmp2(2,end) = firstNode;
            tmp2(3,end) = secondNode;
        end
    case {4,'Quad','quad','QUAD','QuAD','QUaD','QUAd','qUAD','QuaD','QuAd','QUad','qUAd'}
        thirdNode = secondNode + 1;
        fourthNode = firstNode + 1;

        tmp1 = [firstNode   : secondNode - offset;...
                secondNode  : thirdNode + n - 2
                thirdNode   : thirdNode + n - 1;...
                fourthNode  : fourthNode + n - 1
                ];
        if options.closedQ
            tmp1(3,end) = secondNode;
            tmp1(4,end) = firstNode;
        end

        tmp2 = [];
    otherwise
        error('');
end

nodes = [tmp1 tmp2];
if ~options.clockQ
    nodes = flip(nodes);
end

end