% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_op_mergeTwoBodies(pts1,nodes1,pts2,nodes2)
% 
%   Detailed explanation goes here (TODO)
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = geom_op_mergeTwoBodies(pts1,nodes1,pts2,nodes2)

    elemPts = [pts1 , pts2];
    elemNodes = [nodes1 , nodes2 + length(pts1)];
    
    if (nargout > 2)
        elemCen = geom_op_calcElemCen(elemPts,elemNodes);
    end
    if (nargout > 3)
        elemVec = geom_op_calcElemVec(elemPts,elemNodes,true);
    end
    if (nargout > 4)
        elemSize = geom_op_calcElemSize(elemPts,elemNodes,true);
    end
end