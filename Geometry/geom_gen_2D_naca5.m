% [XY,XYU,XYL,XYC] = geom_gen_2D_naca4(design,divisions,spacing,opts)
%
% Returns matrices of coordinates that make up a NACA 4 digit airfoil
% 
% Note: the vertices of each element in 'elemNodes' are defined in a
%       clockwise direction.
% 
%   Example:
%       opts.edge = false;
%       [XY] = geom_2D_naca4('2412',100,'uni',opts);
% 
%   Output variants:
%    *  XY              = returns a '2 x n' matrix with 
%    * [XY,XYU]         = returns 2 '2 x n' matrices with additional
%                         x & y coordinates of upper airfoil surface
%    * [XY,XYU,XYL]     = returns 3 '2 x n' matrices with additional
%                         x & y coordinates of lower airfoil surface
%    * [XY,XYU,XYL,XYC] = returns 4 '2 x n' matrices with additional
%                         x & y coordinates of camber airfoil surface
% 
%   Arguments permitted for 'spacing' (default = 'cos'):
%    * 'cos', 'cosine', 'uni', 'uniform'
% 
%   Fields permitted for 'opts':
%       edge    - default = true (zero-thickness trailing edge)
% 
function [XY,XYU,XYL,XYC] = ...
            geom_gen_2D_naca5(design,divisions,spacing,opts)

% input handling
digit = '0015';
n = 100;
s = 'cosine';
default.edge = true;

switch nargin
    case 0
    case 1
        digit = design;
    case 2
        digit = design;
        n = divisions;
    case {3,4}
        digit = design;
        n = divisions;
        s = spacing;
    otherwise
        error('Unknown number of input arguments');
end
if (nargin ~= 4)
    options = default;
else
    options = completeInput(opts,default);
end

% input interpretation
l = str2num(digit(1))/100;	 % theoretical optimum lift coefficient
p = str2num(digit(2))/100;   % location of maximum camber (xcMax = 0.05*p)
q = str2num(digit(3))/100;   % simple or reflex indication for camber
xx = str2num(digit(4:5))/100;% ratio of thickness <-> chord length

    
% define x coordinates
switch (s)
    case {'cos','cosine'} % half cosine based spacing
        x = (1 - cos(linspace(0,pi,n + 1)'))/2;
    case {'uni','uniform'}
        x = linspace(0,1,n + 1)';
    otherwise
        error('Unknown input argument')
end
xc1 = x(x <= p);
xc2 = x(x > p);
xc = [xc1 ; xc2];


error('Development still in progress');


% calculate y coordinates
if (options.edge)
    a4 = -0.1036;
else
    a4 = -0.1015;
end
yt = 5*t* (0.2969*sqrt(x) -0.126 * x - 0.3516*x.^2 + 0.2843*x.^3 + a4*x.^4);

% identify coordinates of upper and lower boundary
if (p == 0) % identical with symmetrical airfoil
    xu = x;
    yu = yt;

    xl = x;
    yl = -yt;
    
    yc = zeros(size(xc));
else
    % camber line points
    if (q == 0) % normal camber line
        yc1 = xc1^3 - 3*r*xc1^2 + r^2*(3 - r)*xc1;
        yc2 = r^3*(1 - xc2);
        
        dyc1_dx = 3*xc1^2 - 6*r*xc1 + r^2*(3 - r);
        dyc2_dx = -r^3;
    else % q == 1 as reflex camber line
        yc1 = (xc2 - r)^3 - k2/k1 * (1 - r)^3*xc2 - r^3*xc2 + r^3;
        yc2 = k2/k1 * (xc2 - r)^3 - k2/k1 * (1 - r)^3*xc2 - r^3*xc2 + r^3;
        
        dyc1_dx = (xc2 - r)^3 - k2/k1 * (1 - r)^3*xc2 - r^3*xc2 + r^3;
        dyc2_dx = 3*k2/k1 * (xc2 - r)^2 - k2/k1 * (1 - r)^3 - r^3;
    end
    
    yc = k1/6 * [yc1 ; yc2];
    dyc_dx = k1/6 * [dyc1_dx ; dyc2_dx];
    theta = atan(dyc_dx);

    % upper coordinates
    xu = x - yt.*sin(theta);
    yu = yc + yt.*cos(theta);

    % lower coordinates
    xl = x + yt.*sin(theta);
    yl = yc - yt.*cos(theta);
end

% collect information for output
XY = [[xu ; flip(xl(2:end-1))]' ; 
      [yu ; flip(yl(2:end-1))]'];
XYU = [xu' ; yu'];
XYL = [xl' ; yl'];
XYC = [xc' ; yc'];

end