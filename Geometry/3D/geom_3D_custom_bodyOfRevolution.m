% [elemPts,elemNodes,elemCen,elemVec,elemSize] = 
%       geom_3D_custom_bodyOfRevolution(x,y,divC,opts)
%
% Returns a list of segments of the elements that make up a body of 
% revolution in 'elemPts'. The list of elements that make up the boundary
% are defined by the indices of the two vertices in 'elemNodes'.
% 
% TODO
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_custom_bodyOfRevolution(x,y,divC,opts)

% input handling
nC = 36;
default.triQ = true;
default.clockQ = true;
switch nargin
    case {0,1}
        error('Need information about contour in x/y coordinates');
    case 2
    case {3,4}
        nC = divC;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 4
    options = default;
else
    options = completeInput(opts,default);
end
if options.triQ
    type = 'tri';
else
    type = 'quad';
end


% coordinates
div = 2*pi / nC;
angles = 0 : div : (2*pi - div); % circumferential angles (about X axis)
X = repmat(x,[nC 1]);            %grid of x points
Y = cos(angles)'*y;
Z = sin(angles)'*y;
n = numel(X);
elemPts = [reshape(X,[1 n]) ; reshape(Y,[1 n]) ; reshape(Z,[1 n])];


% elements
if (nargout > 1)
    options.closedQ = true;
    elemNodes = [];
    for ii = 1:length(x)-1
        firstNode = 1 + (ii-1)*nC;
        secondNode = firstNode + nC;
        elemNodes = [elemNodes ...
                     geom_gen_nodesForElem(type,nC,firstNode,secondNode,1,options)];
    end
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end

end