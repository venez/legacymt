% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%   geom_3D_custom_pipe(radius,height,divC,divH,opts)
%
% Returns a list of edges of the panels that make up the pipe in  
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the three nodes in 'elemNodes'. 
% 
% NOTE: the coordinate sets of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
%   Fields permitted for 'opts':
%       clockQ  - default = true
%       triQ    - default = true
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_custom_pipe(radius,height,divC,divH,opts)

% input handling
h = 1.0;
nC = 36;
nH = 4;
r = 1.0;
default.clockQ = true;
default.triQ = true;

switch nargin
    case 0
    case 1
        r = radius;
    case 2
        h = height;
        r = radius;
    case 3
        h = height;
        nC = divC;
        r = radius;
    case {4,5}
        h = height;
        nC = divC;
        nH = divH;
        r = radius;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 5
    options = default;
else
    options = completeInput(opts,default);
end
options.botQ = false;
options.topQ = false;

% coordinates
if (nargout == 1)
    elemPts = geom_3D_cylinder(r,h,nC,nH,0,options);
end

% elements
if (nargout > 1)
    [elemPts,elemNodes] = geom_3D_cylinder(r,h,nC,nH,0,options);
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end


end