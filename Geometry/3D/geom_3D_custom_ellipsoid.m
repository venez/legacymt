% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_3D_custom_ellipsoid(aX,bY,cZ,divC,divMajor)
%
% Returns a list of edges of the panels that make up the ellipsoid in  
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the three nodes in 'elemNodes'.
% 
% NOTE: the coordinate sets of each element in 'elemNodes' are defined in a
%       clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_custom_ellipsoid(aX,bY,cZ,divC,divMajor,opts)

% Input handling
a = 1.0;
b = 0.5;
c = b;
nH = 10;
nC = 36;
default.clockQ = true;
default.triQ = true;

switch nargin
    case 0
    case 1
        a = aX;    
    case 2
        a = aX;
        b = bY;
    case 3
        a = aX;
        b = bY;
        nC = divC;
    case 4
        a = aX;
        b = bY;
        c = cZ;
        nC = divC;
    case {5,6}
        a = aX;
        b = bY;
        c = cZ;
        nH = divMajor;
        nC = divC;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 6
    options = default;
else
    options = completeInput(opts,default);
end


% coordinates
if options.triQ
    firstPts = [0 ; 0 ; -a];
    tmp = 1;
    type = 'tri';
else
    firstPts = geom_2D_contour_ellipse(b/100,c/100,nC,-a);
    tmp = nC;
    type = 'quad';
end

elemPts = firstPts;
h = 2*a/nH;
for j = 1:nH-1
    r = sqrt(c^2 - (a - j*h)^2 * c^2/a^2);
    r2 = sqrt(b^2 - (a - j*h)^2 * b^2/a^2);
    elemPts = [elemPts , geom_2D_contour_ellipse(r,r2,nC,-a+j*h)];
end
elemPts = [elemPts , firstPts + repmat([0 ; 0 ; 2*a],[1 tmp]) ];
elemPts = op_rotate(elemPts,'Y','+',pi/2);


% elements
if (nargout > 1)
	% first node <-> first ring
    if options.triQ
        elemNodes = [ones(1,nC) ; 3 : nC+2 ; 2 : nC+1];
        elemNodes(2,end) = 2;
	else
        firstNode = 1;
        secondNode = firstNode + nC;
        elemNodes = geom_gen_nodesForElem('quad',nC,firstNode,secondNode,1,options);
    end
    if (options.triQ) && (~options.clockQ)
        elemNodes = flip(elemNodes);
    end
    
    % sections
	for j = 1:nH-2
        firstNode = 1 + options.triQ + (j-options.triQ) * nC;
        secondNode = firstNode + nC;
        elemNodes = [elemNodes , ...
                     geom_gen_nodesForElem(type,nC,firstNode,secondNode,1,options)];
	end
    
    % last ring <-> last node
    n = length(elemPts);
	if options.triQ
        tmp = [n - nC : n - 1 ; n - nC + 1 : n ; n * ones(1,nC)];
        tmp(2,end) = n - nC;
        if (options.clockQ)
            elemNodes = [elemNodes , tmp];
        else
            elemNodes = [elemNodes , flip(tmp)];
        end
	else
        firstNode = n - 2*nC + 1;
        secondNode = firstNode + nC;
        elemNodes = [elemNodes ,...
                geom_gen_nodesForElem('quad',nC,firstNode,secondNode,1,options)];
	end
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% Output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end

end