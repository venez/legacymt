% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%   geom_3D_cylinder_naca(design,height,divC,divL,divR,spacing,opts)
%
% Returns a list of edges of the panels that make up the NACA foil in  
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the three nodes in 'elemNodes'. Optionally top and / or
% bottom plate can be excluded from result.
% 
% NOTE: the coordinate sets of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
%   Fields permitted for 'opts':
%       botQ    - default = true
%       clockQ  - default = true
%       edge    - default = true
%       topQ    - default = true
%       triQ    - default = true
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_cylinder_naca(design,height,divC,divH,divR,spacing,opts)

% input handling
digit = '0015';
h = 1.0;
nC = 12;
nH = 3;
nR = 1;
s = 'cosine';
default.clockQ = true;
default.edge = true;
default.botQ = true;
default.topQ = true;
default.triQ = true;

switch nargin
    case 0
    case 1
        digit = design;
    case 2
        digit = design;
        h = height;
    case 3
        digit = design;
        h = height;
        nC = divC;
    case 4
        digit = design;
        h = height;
        nC = divC;
        nH = divH;
    case 5
        digit = design;
        h = height;
        nC = divC;
        nH = divH;
        nR = divR;
    case {6,7}
        digit = design;
        h = height;
        nC = divC;
        nH = divH;
        s = spacing;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 7
    options = default;
else
    options = completeInput(opts,default);
end
if options.triQ
    type = 'tri';
else
    type = 'quad';
end


% coordinates
tmpPtsB = [];
tmpPtsT = [];
tmpNodesB = [];
tmpNodesT = [];

if options.botQ
    [tmpPtsB,tmpNodesB] = geom_2D_plate_naca(digit,nC,nR,s,-h/2,options);
end
if options.topQ
    [tmpPtsT,tmpNodesT] = geom_2D_plate_naca(digit,nC,nR,s,h/2,options);
end

ptsIn = geom_2D_contour_naca(digit,nC,s,-h/2,options);
[tmpPts,tmpNodes] = geom_op_extrudeCrossSection(ptsIn,h,nH,'z',options);

elemPts = [tmpPtsB , tmpPts , tmpPtsT];


% elements (here: triangles)
nc = 2*nC;
if (nargout > 1)
    % secure outward vector direction
    elemNodes = tmpNodesB;

    % ground plate <-> first ring
    if options.botQ
        firstNode = length(tmpPtsB) + 1 - nc;
        secondNode = firstNode + nc;
        offset = 1;
        
        elemNodes = [elemNodes, ...
            geom_gen_nodesForElem(type,nc,firstNode,secondNode,offset,options)];
    end
    
    % sections
    n = length(tmpPtsB);
    elemNodes = [elemNodes tmpNodes + n];
    
    % top plate <-> last ring
    if options.topQ
        firstNode = length(tmpPtsB) + length(tmpPts) + 1 - nc;
        secondNode = firstNode + length(tmpPtsT);
        offset = (nR - 1) * nc + nC;
        
        elemNodes = [elemNodes, ...
            geom_gen_nodesForElem(type,nc,firstNode,secondNode,offset,options)];
    end
    
    % top plate
    n = length(tmpPtsB) + length(tmpPts);
    elemNodes = [elemNodes , flip(tmpNodesT) + n];
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end


end