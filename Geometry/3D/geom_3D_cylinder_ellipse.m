% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%   geom_3D_cylinder_ellipse(radius,height,divC,divH,divR,opts)
%
% Returns a list of edges of the panels that make up the cylinder in  
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the three nodes in 'elemNodes'. Optionally top and / or
% bottom plate can be excluded from result.
% 
% NOTE: the coordinate sets of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
%   Fields permitted for 'opts':
%       topQ    - default = true
%       botQ    - default = true
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_cylinder_ellipse(majorAxis,minorAxis,height,divC,divH,divR,opts)

% input handling
a = 1.0;
b = 0.5;
h = 1.0;
nC = 36;
nH = 4;
nR = 3;
default.botQ = true;
default.clockQ = true;
default.topQ = true;
default.triQ = true;

switch nargin
    case 0
    case 1
        a = majorAxis;
    case 2
        a = majorAxis;
        b = minorAxis;
    case 3
        a = majorAxis;
        b = minorAxis;
        h = height;
    case 4
        a = majorAxis;
        b = minorAxis;
        h = height;
        nC = divC;
    case 5
        a = majorAxis;
        b = minorAxis;
        h = height;
        nC = divC;
        nH = divH;
    case {6,7}
        a = majorAxis;
        b = minorAxis;
        h = height;
        nC = divC;
        nH = divH;
        nR = divR;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 7
    options = default;
else
    options = completeInput(opts,default);
end


% coordinates
tmpPtsB = [];
tmpPtsT = [];
tmpNodesB = [];
tmpNodesT = [];

if options.botQ
    [tmpPtsB,tmpNodesB] = geom_2D_plate_ellipse(a,b,nC,nR,-h/2,options);
end
if options.topQ
    [tmpPtsT,tmpNodesT] = geom_2D_plate_ellipse(a,b,nC,nR,h/2,options);
end

ptsIn = geom_2D_contour_ellipse(a,b,nC,-h/2);
[tmpPts,tmpNodes] = geom_op_extrudeCrossSection(ptsIn,h,nH,'z',options);

elemPts = [tmpPtsB , tmpPts , tmpPtsT];
 

% elements
if (nargout > 1)
    % secure outward vector direction
    elemNodes = tmpNodesB;

    % ground plate <-> first ring
    if options.botQ
        firstNode = length(tmpPtsB) + 1 - nC;
        secondNode = firstNode + nC;
        offset = 1;
                
        if (options.triQ)
            tmp = geom_gen_nodesForElem('tri',nC,firstNode,secondNode,offset,options);
        else
            tmp = geom_gen_nodesForElem('quad',nC,firstNode,secondNode,offset,options);
        end
        elemNodes = [elemNodes , tmp];
    end
    
    % sections
    n = length(tmpPtsB);
    elemNodes = [elemNodes tmpNodes + n];
    
    % top plate <-> last ring
    if options.topQ
        firstNode = length(tmpPtsB) + length(tmpPts) + 1 - nC;
        secondNode = firstNode + length(tmpPtsT);
        offset = (nR - 1) * nC + 2;
        
        if (options.triQ)
            tmp = geom_gen_nodesForElem('tri',nC,firstNode,secondNode,offset,options);
        else
            tmp = geom_gen_nodesForElem('quad',nC,firstNode,secondNode,offset,options);
        end
        elemNodes = [elemNodes , tmp];
    end
    
    % top plate
    n = length(tmpPtsB) + length(tmpPts);
    elemNodes = [elemNodes , flip(tmpNodesT) + n];
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end


end