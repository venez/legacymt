% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_3D_sphere_digon(radius,phiC,phiR,divC,divR,opts)
%
% Returns a list of edges of the panels that make up the sphere digon in  
% 'elemPts' and the list of elements that make up the boundary which are 
% defined by the indices of the three or -if 'quadsQ' is specified- four
% nodes in 'elemNodes'.
% 
% Inputs:   radius
%           phiC = opening angle in circumferential direction
%           phiR = opening angle in radial direction
%           divC = number of circumferential divisions
%           divR = number of radial divisions
% 
% NOTE: the coordinate sets of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_sphere_digon(radius,phiC,phiR,divC,divR,opts)

% Input handling
nC = 4;
nR = 4;
r = 1.0;
pC = [pi/2 pi];
pR = pi/2;
default.clockQ = true;
default.triQ = true;

switch nargin
    case 0
    case 1
        r = radius;
    case 2
        pC = phiC;
        r = radius;
	case 3
        pC = phiC;
		pR = phiR;
        r = radius;
    case 4
        nC = divC;
        pC = phiC;
		pR = phiR;
        r = radius;
    case {5,6}
        nC = divC;
        nR = divR;
        pC = phiC;
		pR = phiR;
        r = radius;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 6
    options = default;
else
    options = completeInput(opts,default);
end
% if (options.triQ)
    type = 'tri';
% else
%     type = 'quad';
% end


% error handling
[phiS,phiE] = checkAngle(pC);
if (length(pR) > 1) || (pR < 0) || (pR > pi)
    error('Unknown definition for argument phiR');
end
closedQ = false;
if (pR == pi)
    closedQ = true;
end


% coordinates
elemPts = geom_2D_arc_circle(r,pR,nR,0.0);
elemPts = op_rotate(elemPts,'X','-',pi/2);
firstPts = elemPts(:,1);
lastPts = [];
if closedQ
    lastPts = elemPts(:,end);
end

% if ~options.triQ
%     firstPts = geom_2D_arc_circle(r/100,pC,nC,0.0);
%     firstPts = op_rotate(testPts,'z','-',pi/2);
%     firstPts = op_rotate(testPts,'x','-',pi/2);
%     firstPts = op_displace(testPts,[0 radius 0],false);
% end

incAngle = (phiE-phiS)/nC;
angles = phiS:incAngle:phiE;
tmpPts = [];
for ii = 1:nC+1
    tmp = op_rotate(elemPts(:,2:end-closedQ),'Z','-',angles(ii));
    tmpPts = [tmpPts tmp];
end
elemPts = [firstPts tmpPts lastPts];

% elements (here: triangles only)
if (nargout > 1)
    incR = nR-closedQ;
    tmp = 2 : incR : (nC+1)*(incR-1);
    if options.clockQ
        elemNodes = [ones(1,nC) ; tmp + incR; tmp];
    else
        elemNodes = [ones(1,nC) ; tmp; tmp + incR];
    end
                        
    options.closedQ = false;
    options.clockQ = ~options.clockQ;
    for jj = 1:nC
        firstNode = 1 + (jj-1) * incR;
        secondNode = firstNode + incR;
        n = incR - 1;
        off = 2;
        elemNodes = [elemNodes , 1+ ...
                    geom_gen_nodesForElem(type,n,firstNode,secondNode,off,options)];
    end
    
    if (closedQ) && (options.clockQ)
        elemNodes = [elemNodes , ...
            [length(elemPts) * ones(1,nC) ; tmp+nR-2; tmp+nR-2 + incR]; ];
    elseif (closedQ) && (~options.clockQ)
        elemNodes = [elemNodes , ...
            [length(elemPts) * ones(1,nC) ; tmp+nR-2 + incR ; tmp+nR-2] ];
    end
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end


end

function [s,e] = checkAngle(p)
if (length(p) == 1)
	e = p;
	s = -p;
elseif (length(p) == 2)
	e = max(p);
	s = min(p);
else
    error('Unknown specification for 3rd argument')
end

end