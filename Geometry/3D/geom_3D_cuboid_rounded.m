% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%   geom_3D_cuboid_rounded(lengthX,lengthY,lengthZ,radiusR,divX,divY,divZ,divR,opts)
%
% Returns a list of edges of the panels that make up the rounded cuboid in
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the three nodes in 'elemNodes'. Optionally top and / or
% bottom plate can be excluded from result.
% 
% NOTE: the coordinate sets of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
%   Fields permitted for 'opts':
%       botQ    - default = true
%       clockQ  - default = true
%       topQ    - default = true
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_cuboid_rounded(lengthX,lengthY,lengthZ,radiusR,divX,divY,divZ,divR,opts)

% input handling
x = 2.0;
y = 1.5;
z = 0.5;
r = 0.1;
nX = 10;
nY = 5;
nZ = 4;
nR = 3;
default.botQ = true;
default.clockQ = true;
default.topQ = true;
% default.triQ = true;
    
switch nargin
    case 0
    case 1
        x = lengthX;
    case 2
        x = lengthX;
        y = lengthY;
    case 3
        x = lengthX;
        y = lengthY;
        z = lengthZ;
    case 4
        x = lengthX;
        y = lengthY;
        z = lengthZ;
        r = radiusR;
    case 5
        x = lengthX;
        y = lengthY;
        z = lengthZ;
        r = radiusR;
        nX = divX;
    case 6
        x = lengthX;
        y = lengthY;
        z = lengthZ;
        r = radiusR;
        nX = divX;
        nY = divY;
    case 7
        x = lengthX;
        y = lengthY;
        z = lengthZ;
        r = radiusR;
        nX = divX;
        nY = divY;
        nZ = divZ;
    case {8,9}
        x = lengthX;
        y = lengthY;
        z = lengthZ;
        r = radiusR;
        nX = divX;
        nY = divY;
        nZ = divZ;
        nR = divR;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 9
    options = default;
else
    options = completeInput(opts,default);
end

% coordinates & elements
dimX = x - 2*r;
dimY = y - 2*r;
dimZ = z - 2*r;

    % plane geometries
ptsB = [];      nodesB = [];
ptsT = [];      nodesT = [];
[ptsTB,nodesTB] = geom_2D_plate_rectangle(dimX,dimY,nX,nY,0.0,options);
if options.botQ
    ptsB = op_rotate(ptsTB,'X','-',pi);
    ptsB = op_displace(ptsB,[0 0 -z/2],false);
    nodesB = nodesTB;
end
if options.topQ
    ptsT = op_displace(ptsTB,[0 0 z/2],false);
    nodesT = nodesTB;
end
[ptsTB,nodesTB] = geom_op_mergeTwoBodies(ptsT,nodesT,ptsB,nodesB);

[ptsF,nodesF] = geom_2D_plate_rectangle(dimZ,dimY,nZ,nY,dimX/2+r,options);
ptsF = op_rotate(ptsF,'Y','-',pi/2);
ptsB = op_rotate(ptsF,'Z','-',pi);
nodesB = nodesF;
[ptsFB,nodesFB] = geom_op_mergeTwoBodies(ptsF,nodesF,ptsB,nodesB);

[ptsL,nodesL] = geom_2D_plate_rectangle(dimX,dimZ,nX,nZ,dimY/2+r,options);
ptsL = op_rotate(ptsL,'X','-',pi/2);
ptsR = op_rotate(ptsL,'Z','-',pi);
nodesR = nodesL;
[ptsLR,nodesLR] = geom_op_mergeTwoBodies(ptsL,nodesL,ptsR,nodesR);

[platePts,plateNodes] = geom_op_mergeTwoBodies(ptsFB,nodesFB,ptsLR,nodesLR);
[platePts,plateNodes] = geom_op_mergeTwoBodies(ptsTB,nodesTB,platePts,plateNodes);

    % edge geometries
[archPts,nodes1] = geom_3D_arch_circle(r,[0 pi/2],dimX,nR,nX,options);
pts1 = op_rotate(archPts,'Y','+',pi/2);
pts2 = op_rotate(archPts,'Y','-',pi/2);
pts1 = op_displace(pts1,[0 dimY/2 dimZ/2],false);
pts2 = op_displace(pts2,[0 dimY/2 -dimZ/2],false);
nodes2 = nodes1;
[archPtsX,archNodesX] = geom_op_mergeTwoBodies(pts1,nodes1,pts2,nodes2);

[archPts,nodes1] = geom_3D_arch_circle(r,[0 pi/2],dimY,nR,nY,options);
pts1 = op_rotate(archPts,'X','-',pi/2);
pts1 = op_displace(pts1,[dimX/2 0 dimZ/2],false);
pts2 = op_rotate(archPts,'X','-',pi/2);
pts2 = op_rotate(pts2,'Y','+',pi/2);
pts2 = op_displace(pts2,[-dimX/2 0 dimZ/2],false);
nodes2 = nodes1;
[archPtsY,archNodesY] = geom_op_mergeTwoBodies(pts1,nodes1,pts2,nodes2);

[archPts,nodes1] = geom_3D_arch_circle(r,[0 pi/2],dimZ,nR,nZ,options);
pts1 = op_displace(archPts,[dimX/2 dimY/2 0],false);
pts2 = op_rotate(archPts,'Z','-',pi/2);
pts2 = op_displace(pts2,[-dimX/2 dimY/2 0],false);
nodes2 = nodes1;
[archPtsZ,archNodesZ] = geom_op_mergeTwoBodies(pts1,nodes1,pts2,nodes2);

[pts1,nodes1] = geom_op_mergeTwoBodies(archPtsY,archNodesY,archPtsZ,archNodesZ);
[pts2,nodes2] = geom_op_mergeTwoBodies(pts1,nodes1,archPtsX,archNodesX);

pts1 = op_rotate(pts2,'X','-',pi);
nodes1 = nodes2;
[archPts,archNodes] = geom_op_mergeTwoBodies(pts1,nodes1,pts2,nodes2);

    % corner geometries
[ptsQ1,nodesQ1] = geom_3D_sphere_digon(r,[0 pi/2],pi/2,nR,nR,options);
ptsQ1 = op_displace(ptsQ1,[dimX/2 dimY/2 dimZ/2],false);
[ptsQ2,nodesQ2] = geom_3D_sphere_digon(r,[pi/2 pi],pi/2,nR,nR,options);
ptsQ2 = op_displace(ptsQ2,[-dimX/2 dimY/2 dimZ/2],false);
[ptsQ3,nodesQ3] = geom_3D_sphere_digon(r,[pi 3/2*pi],pi/2,nR,nR,options);
ptsQ3 = op_displace(ptsQ3,[-dimX/2 -dimY/2 dimZ/2],false);
[ptsQ4,nodesQ4] = geom_3D_sphere_digon(r,[3/2*pi 2*pi],pi/2,nR,nR,options);
ptsQ4 = op_displace(ptsQ4,[dimX/2 -dimY/2 dimZ/2],false);

[pts1,nodes1] = geom_op_mergeTwoBodies(ptsQ1,nodesQ1,ptsQ2,nodesQ2);
[pts2,nodes2] = geom_op_mergeTwoBodies(ptsQ3,nodesQ3,ptsQ4,nodesQ4);
[pts2,nodes2] = geom_op_mergeTwoBodies(pts1,nodes1,pts2,nodes2);

pts1 = op_rotate(pts2,'x','-',pi);
nodes1 = nodes2;
[digonPts,digonNodes] = geom_op_mergeTwoBodies(pts1,nodes1,pts2,nodes2);

    % merge geometries into final and complete its information
[elemPts,elemNodes] = geom_op_mergeTwoBodies(platePts,plateNodes,archPts,archNodes);
[elemPts,elemNodes] = geom_op_mergeTwoBodies(elemPts,elemNodes,digonPts,digonNodes);


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end


end