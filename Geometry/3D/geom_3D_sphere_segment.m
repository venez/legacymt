% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_3D_sphere_segment(radius,height,phi,divH,divV,opts)
%
% Returns a list of edges of the panels that make up the sphere ring in  
% 'elemPts' and the list of elements that make up the boundary which are 
% defined by the indices of the three or -if 'quadsQ' is specified- four
% nodes in 'elemNodes'.
% 
% Inputs:   radius
%			height
%           phi = 
%           divH = number of horizontnal divisions
%           divV = number of vertical divisions
% 
% NOTE: the coordinate sets of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_sphere_segment(radius,height,phi,divH,divV,opts)

% Input handling
nH = 36;
nV = 10;
r = 1.0;
s = 1.5;
p = [0 2*pi];
default.clockQ = true;
default.triQ = false;

switch nargin
    case 0
    case 1
        r = radius;
    case 2
        r = radius;
        s = height;
	case 3
		p = phi;
        r = radius;
        s = height;
    case 4
        nH = divH;
		p = phi;
        r = radius;
        s = height;
    case {5,6}
        nH = divH;
        nV = divV;
		p = phi;
        r = radius;
        s = height;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 6
    options = default;
else
    options = completeInput(opts,default);
end
if (options.triQ)
    type = 'tri';
else
    type = 'quad';
end


% error handling
if (length(p) ~= 1) && (length(p) ~= 2)
	error('Unknown specification for 3rd argument')
end
if (length(p) == 1)
	phiE = p;
	phiS = -p;
else
	phiE = max(p);
	phiS = min(p);
end


% coordinates
divA = (abs(phiE) + abs(phiS))/nH;
angles = phiS : divA : phiE - divA;
elemPts = [];
for jj = 1:nV + 1
     h = -s/2 + (jj - 1) * s/nV;
     a = sqrt(r^2 - h^2);
     elemPts = [elemPts ,...
 				[a*sin(angles); ...
 				 a*cos(angles);...
 				 repmat(h,1,nH)]];
end


% elements (here: triangles or quads)
elemNodes = [];
if (phiS == phiE - 2*pi) 
    options.closedQ = true;
else
    options.closedQ = false;
end
if (nargout > 1)
	for jj = 1:nV
        firstNode = 1 + (jj-1) * nH;
        secondNode = firstNode + nH;
        n = nH - ~options.closedQ;
        off = 1 + ~options.closedQ;
        
        elemNodes = [elemNodes , ...
                     geom_gen_nodesForElem(type,n,firstNode,secondNode,off,options)];
	end
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end


end