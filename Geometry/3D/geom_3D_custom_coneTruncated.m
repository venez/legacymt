% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_3D_custom_coneTruncated(radiusB,radiusT,height,divC,divH,divR,opts)
%
% Returns a list of edges of the panels that make up the cylinder in  
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the three nodes in 'elemNodes'. Optionally top and / or
% bottom plate can be excluded from result.
% 
% NOTE: the coordinate sets of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
%   Fields permitted for 'opts':
%       topQ    - default = true
%       botQ    - default = true
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_custom_coneTruncated(radiusB,radiusT,height,divC,divH,divR,opts)

% input handling
h = 1.0;
nC = 26;
nH = 4;
nR = 3;
R = 1.0;
r = 0.5;
default.botQ = true;
default.clockQ = true;
default.topQ = true;
default.triQ = true;

switch nargin
    case 0
    case 1
        R = radiusB;
    case 2
        R = radiusB;
        r = radiusT;
    case 3
        h = height;
        R = radiusB;
        r = radiusT;
    case 4
        h = height;
        nC = divC;
        R = radiusB;
        r = radiusT;
    case 5
        h = height;
        nC = divC;
        nH = divH;
        R = radiusB;
        r = radiusT;
    case {6,7}
        h = height;
        nC = divC;
        nH = divH;
        nR = divR;
        R = radiusB;
        r = radiusT;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 7
    options = default;
else
    options = completeInput(opts,default);
end
if options.triQ
    type = 'tri';
else
    type = 'quad';
end

% coordinates
tmpPtsB = [];
tmpPtsT = [];
tmpNodesB = [];
tmpNodesT = [];

if options.botQ
    [tmpPtsB,tmpNodesB] = geom_2D_plate_circle(R,nC,nR,-h/2,options);
end
if options.topQ
    [tmpPtsT,tmpNodesT] = geom_2D_plate_circle(r,nC,nR,h/2,options);
end

ptsIn = geom_2D_contour_circle(R,nC,-h/2);
k = h * r / (R - r);
f = (h + k - h/(nH - 1)) / (h + k);
options.incScale = [f f 1];
[tmpPts,tmpNodes] = geom_op_extrudeCrossSection(ptsIn,h,nH,'z',options);
elemPts = [tmpPtsB , tmpPts , tmpPtsT];


% elements
if (nargout > 1)
    % secure outward vector direction
    elemNodes = tmpNodesB;

    % ground plate <-> first ring
    if options.botQ
        firstNode = length(tmpPtsB) + 1 - nC;
        secondNode = firstNode + nC;
        offset = 1;
        
        elemNodes = [elemNodes, ...
            geom_gen_nodesForElem(type,nC,firstNode,secondNode,offset,options)];
    end
    
    % sections
    n = length(tmpPtsB);
    elemNodes = [elemNodes tmpNodes + n];
    
    % top plate <-> last ring
    if options.topQ
        firstNode = length(tmpPtsB) + length(tmpPts) + 1 - nC;
        secondNode = firstNode + length(tmpPtsT);
        offset = (nR - 1) * nC + 2;
        
        elemNodes = [elemNodes, ...
            geom_gen_nodesForElem(type,nC,firstNode,secondNode,offset,options)];
    end
    
    % top plate
    n = length(tmpPtsB) + length(tmpPts);
    elemNodes = [elemNodes , flip(tmpNodesT) + n];
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end


end