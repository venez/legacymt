% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%   geom_3D_cuboid(lengthX,lengthY,lengthZ,divX,divY,divZ,opts)
%
% Returns a list of edges of the panels that make up the cuboid in  
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the three nodes in 'elemNodes'. Optionally top and / or
% bottom plate can be excluded from result.
% 
% NOTE: the coordinate sets of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
%   Fields permitted for 'opts':
%       botQ    - default = true
%       clockQ  - default = true
%       topQ    - default = true
%       triQ    - default = true
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_cuboid(lengthX,lengthY,lengthZ,divX,divY,divZ,opts)

% input handling
x = 2.0;
y = 1.5;
z = 0.5;
nX = 10;
nY = 5;
nZ = 4;
default.botQ = true;
default.clockQ = true;
default.topQ = true;
default.triQ = true;
    
switch nargin
    case 0
    case 1
        x = lengthX;
    case 2
        x = lengthX;
        y = lengthY;
    case 3
        x = lengthX;
        y = lengthY;
        z = lengthZ;
    case 4
        x = lengthX;
        y = lengthY;
        z = lengthZ;
        nX = divX;
    case 5
        x = lengthX;
        y = lengthY;
        z = lengthZ;
        nX = divX;
        nY = divY;
    case {6,7}
        x = lengthX;
        y = lengthY;
        z = lengthZ;
        nX = divX;
        nY = divY;
        nZ = divZ;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 7
    options = default;
else
    options = completeInput(opts,default);
end

% coordinates & elements
ptsB = [];      nodesB = [];
ptsT = [];      nodesT = [];
[ptsTB,nodesTB] = geom_2D_plate_rectangle(x,y,nX,nY,0.0,options);
if options.botQ
    ptsB = op_rotate(ptsTB,'X','-',pi);
    ptsB = op_displace(ptsB,[0 0 -z/2],false);
    nodesB = nodesTB;
end
if options.topQ
    ptsT = op_displace(ptsTB,[0 0 z/2],false);
    nodesT = nodesTB;
end
[ptsTB,nodesTB] = geom_op_mergeTwoBodies(ptsT,nodesT,ptsB,nodesB);

[ptsF,nodesF] = geom_2D_plate_rectangle(z,y,nZ,nY,x/2,options);
ptsF = op_rotate(ptsF,'Y','-',pi/2);
ptsB = op_rotate(ptsF,'Z','-',pi);
nodesB = nodesF;
[ptsFB,nodesFB] = geom_op_mergeTwoBodies(ptsF,nodesF,ptsB,nodesB);

[ptsL,nodesL] = geom_2D_plate_rectangle(x,z,nX,nZ,y/2,options);
ptsL = op_rotate(ptsL,'X','-',pi/2);
ptsR = op_rotate(ptsL,'Z','-',pi);
nodesR = nodesL;
[ptsLR,nodesLR] = geom_op_mergeTwoBodies(ptsL,nodesL,ptsR,nodesR);

[elemPts,elemNodes] = geom_op_mergeTwoBodies(ptsFB,nodesFB,ptsLR,nodesLR);
[elemPts,elemNodes] = geom_op_mergeTwoBodies(ptsTB,nodesTB,elemPts,elemNodes);


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end


end