% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_3D_custom_waveAiryInfinite(major,minor,divMajor,divMinor,zCoord)
% 
% Returns a list of edges of the panels that make up the plane in
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the three vertices in 'elemNodes'.
% 
% NOTE: the vertices of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_custom_waveAiryInfinite(lengthX,lengthY,divX,divY,ampl,omega,opts)

% Input handling
g = 9.81;
o = 2.0;
x = 5.0;
y = 0.5;
z = 1.0;
nX = 10;
nY = 5;
default.clockQ = true;
default.triQ = true;

switch nargin
    case 0
    case 1
        x = lengthX;
    case 2
        x = lengthX;
        y = lengthY;
    case 3
        x = lengthX;
        y = lengthY;
        nX = divX;
    case 4
        x = lengthX;
        y = lengthY;
        nX = divX;
        nY = divY;
    case 5
        x = lengthX;
        y = lengthY;
        nX = divX;
        nY = divY;
        z = ampl;
    case {6,7}
        x = lengthX;
        y = lengthY;
        nX = divX;
        nY = divY;
        z = ampl;
        o = omega;
    otherwise
        error('Unknown number of input argument');
end
if nargin ~= 6
    options = default;
else
    options = completeInput(opts,default);
end
if options.triQ
    type = 'tri';
else
    type = 'quad';
end

% coordinates
k = o^2/g;
tmpX = -x/2 : x/nX : +x/2;
tmpY = -y/2 : y/nY : +y/2;
tmpZ = z*sin(k.*tmpX);
elemPts = [];
for i = 1:nX+1
   elemPts = [elemPts , [repmat(tmpX(i),1,nY+1) ; tmpY ; ...
                         repmat(tmpZ(i),1,nY+1)]];
end


% elements
if (nargout > 1)
    elemNodes = [];
    for j = 1:nX
        firstNode = 1 + (j-1) * (nY + 1);
        secondNode = firstNode + nY + 1;
        options.closedQ = false;
        
        elemNodes = [elemNodes , ...
                     geom_gen_nodesForElem(type,nY,firstNode,secondNode,2,options)];
    end
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% Output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output argument');
end

end
