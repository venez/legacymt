% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%   geom_3D_arch_circle(radius,height,divC,divH,opts)
%
% Returns a list of edges of the panels that make up the cylinder in  
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the three nodes in 'elemNodes'. 
% 
% NOTE: the coordinate sets of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
%   Fields permitted for 'opts':
%       clockQ  - default = true
%       triQ    - default = true
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_arch_circle(radius,phi,height,divC,divH,opts)

% input handling
h = 1.0;
nC = 5;
nH = 4;
p = [0 pi];
r = 1.0;
default.clockQ = true;
default.triQ = true;

switch nargin
    case 0
    case 1
        r = radius;
    case 2
        p = phi;
        r = radius;
    case 3
        h = height;
        p = phi;
        r = radius;
    case 4
        h = height;
        nC = divC;
        p = phi;
        r = radius;
    case {5,6}
        h = height;
        nC = divC;
        nH = divH;
        p = phi;
        r = radius;
    otherwise
        error('Unknown number of input arguments');
end


% complete input
if nargin ~= 6
    options = default;
else
    options = completeInput(opts,default);
end
if (length(p) == 1)
	phiE = p;
	phiS = 0;
else
	phiE = max(p);
	phiS = min(p);
end
if (phiS == phiE - 2*pi)
    options.closedQ = true;
else
    options.closedQ = false;
end

% error handling
if (length(p) ~= 1) && (length(p) ~= 2)
	error('Unknown specification for 3rd argument')
end
if (phiS == phiE) || (phiE-phiS > 2*pi)
    error('Illegal specification regarding given angle range')
end


% coordinates
ptsIn = geom_2D_arc_circle(r,[phiS phiE],nC,-h/2);
options.botQ = false;
options.topQ = false;
[elemPts,tmpNodes] = geom_op_extrudeCrossSection(ptsIn,h,nH,'z',options);


% elements
if (nargout > 1)
    elemNodes = tmpNodes;
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end


end