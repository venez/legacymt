% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_3D_sphere(radius,divH,divV,opts)
%
% Returns a list of edges of the panels that make up the sphere in  
% 'elemPts' and the list of elements that make up the boundary which are 
% defined by the indices of the three nodes in 'elemNodes'.
% 
% Inputs:   radius
%           divC = number of circumferential divisions
%           divH = number of vertical divisions
% 
% NOTE: the coordinate sets of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_3D_sphere(radius,divC,divH,opts)

% Input handling
nC = 36;
nH = 10;
r = 1.0;
default.clockQ = true;
default.triQ = true;

switch nargin
    case 0
    case 1
        r = radius;
    case 2
        nC = divC;
        r = radius;
    case {3,4}
        nC = divC;
        nH = divH;
        r = radius;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 4
    options = default;
else
    options = completeInput(opts,default);
end


% coordinates
if options.triQ
    firstPts = [0 ; 0 ; -r];
    tmp = 1;
    type = 'tri';
else
    firstPts = geom_2D_contour_circle(r/100,nC,-r);
    tmp = nC;
    type = 'quad';
end

elemPts = firstPts;
phi = -pi/nH;
for j = 1:nH-1
    a =     r * sin(j*phi);
    h = r - r * cos(j*phi);
    elemPts = [elemPts , geom_2D_contour_circle(a,nC,-r+h)];
end
elemPts = [elemPts , firstPts + repmat([0 ; 0 ; 2*r],[1 tmp]) ];


% elements
if (nargout > 1)
	% first node <-> first ring
    if options.triQ
        elemNodes = [ones(1,nC) ; 3 : nC+2 ; 2 : nC+1];
        elemNodes(2,end) = 2;
    else
        firstNode = 1;
        secondNode = firstNode + nC;
        elemNodes = geom_gen_nodesForElem('quad',nC,firstNode,secondNode,1,options);
    end
    if (options.triQ) && (~options.clockQ)
        elemNodes = flip(elemNodes);
    end
    
    % sections
	for j = 1:nH-2
        firstNode = 1 + options.triQ + (j-options.triQ) * nC;
        secondNode = firstNode + nC;
        elemNodes = [elemNodes , ...
                geom_gen_nodesForElem(type,nC,firstNode,secondNode,1,options)];
    end
    
    % last ring <-> last node
    n = length(elemPts);
	if options.triQ
        tmpPts = [n - nC : n - 1 ; n - nC + 1 : n ; n * ones(1,nC)];
        tmpPts(2,end) = n - nC;
        if (options.clockQ)
            elemNodes = [elemNodes , tmpPts];
        else
            elemNodes = [elemNodes , flip(tmpPts)];
        end
	else
        firstNode = n - 2*nC + 1;
        secondNode = firstNode + nC;
        elemNodes = [elemNodes ,...
                geom_gen_nodesForElem('quad',nC,firstNode,secondNode,1,options)];
	end
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end


end