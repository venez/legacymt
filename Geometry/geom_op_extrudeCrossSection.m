% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%           geom_op_extrudeCrossSection(ptsIn,height,divH,dir,opts)
% 
%   Detailed explanation goes here (TODO)
% 
%       botQ      - default: true
%       clockQ    - default: true
%       closedQ   - default: false
%       incRot    - default: []
%       incScale  - default: []
%       topQ      - default: false
%       triQ      - default: true
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
    geom_op_extrudeCrossSection(ptsIn,height,divH,dir,opts)

% input handling
d = 'z';
L = 1.0;
nH = 3;
default.botQ = true;
default.clockQ = true;
default.closedQ = true;
default.incRot = [];
default.incScale = [];
default.topQ = false;
default.triQ = true;

switch nargin
    case 1
    case 2
        L = height;
    case 3
        L = height;
        nH = divH;
    case {4,5}
        L = height;
        nH = divH;
        d = dir;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 5
    options = default;
else
    options = completeInput(opts,default);
end


% error handling
[m,n] = size(ptsIn);
if (m > n) || (m > 3)
    error('Unknown shape for first input argument');
end


% preparations
nC = length(ptsIn);
switch d
    case {'X','x',1}
        idx = 1;
        choice = [2 3];
    case {'Y','y',2}
        idx = 2;
        choice = [1 3];
    case {'Z','z',3}
        idx = 3;
        choice = [1 2];
%     case {'C','c',4} TODO
    otherwise
        error('Unknown direction');
end
if (m < 3)
    offset = 0;
else
    offset = ptsIn(idx,1);
end
tmpPts = offset * ones(m,n);
tmpPts(choice,:) = ptsIn(choice,:);


% coordinates
if options.botQ
    offset = offset + L/nH;
end

elemPts = [];
for ii = 1:nH + 1 - (options.botQ) - (options.topQ)
    tmpPts(idx,:) = repmat(offset + L/nH * (ii-1) ,[1 n]);
    
    if ~isempty(options.incScale)
        tmpPts = op_scale(tmpPts,options.incScale);
    end
    if ~isempty(options.incRot)
        tmpPts = op_rotate(tmpPts,idx,'-',options.incRot);
    end
    
    elemPts = [elemPts tmpPts];
end


% elements
elemNodes = [];
for ii = 1:nH - (options.botQ) - (options.topQ)
    firstNode = 1 + (ii - 1) * n;
    secondNode = firstNode + n;
    offset = 1;
    if (options.triQ)
        tmp = geom_gen_nodesForElem('tri',nC,firstNode,secondNode,offset,options);
    else
        tmp = geom_gen_nodesForElem('quad',nC,firstNode,secondNode,offset,options);
    end
    if (options.triQ) && (~options.closedQ)
        tmp = tmp(:,[1:nC-1 , nC+1:(2*nC-1)]);
    elseif (~options.triQ) && (~options.closedQ)
        tmp = tmp(:,1:nC-1);
    end
    elemNodes = [elemNodes , tmp];
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end

end