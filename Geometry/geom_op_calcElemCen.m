% [elemCen] = geom_op_calcElemCen(elemPts,elemNodes)
% 
% Calculates the individual element center points for their given points 
% 'elemPts' and nodes 'elemNodes'. The functions distinguish automatically
% between the element shapes 'lines', 'triangles' and 'quads'.
% 
%   Example:
%       sizes = geom_op_calcElemSize(pts,nodes);
% 
function [elemCen] = geom_op_calcElemCen(elemPts,elemNodes)

[m,n] = size(elemNodes);
if (m ~= 2) && (m ~= 3) && (m ~= 4)
    error('Unknown element shape');
end

[dimPts,~] = size(elemPts);
tmp = zeros(dimPts,n);
for ii = 1:m
    tmp = tmp + elemPts(:,elemNodes(ii,:));
end
elemCen = tmp/m;

end