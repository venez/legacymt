% [currentFig] = geom_show(elemPts,elemNodes,elemCen,elemVec,var,opts)
% 
%	Function shows a model specified by the variables 'elemPts', 
%	'elemNodes', 'elemCen' and 'elemVec' on screen as well as returning a 
%	pointer to the current figure by executing MATLAB function 'gcf'.
%   An optional declared variable 'var' will be illustrated at the models 
%   surface. The optional argument 'opts' contains the possibility to 
%   specify various figure properties.
% 
%   Fields permitted for 'opts':
%       alphaE      - default = 1.0;
%       alphaF      - default = 1.0;
%       barName     - default = '';
%       colCen      - default = 'r';
%       colGeo      - default = 'b';
%       colPts      - default = 'b';
%       colVec      - default = 'k';
%       colormap    - default = MATLAB default;
%       figHandle   - default = 1;
%       gridQ       - default = true;
%       markCen     - default = 'o';
%       markPts     - default = 'x';
%       opacity     - default = 1.0;
%       showBarQ    - default = true;
%       showCenQ    - default = false;
%       showEleQ    - default = false;
%       showGeoQ    - default = true;
%       showNodQ    - default = false;
%       showPtsQ    - default = false;
%       showVarQ    - default = false;
%       showVecQ    - default = false;
%       title       - default = '';
%       xlabel      - default = 'X';
%       xlimit      - default = [-10 10];
%       ylabel      - default = 'Y';
%       ylimit      - default = [-10 10];
%       zlabel      - default = 'Z';
%       zlimit      - default = [-10 10];
% 
function [currentFig] = geom_show(elemPts,elemNodes,elemCen,elemVec,var,opts)


% Input handling
default.alphaE = 1.0;
default.alphaF = 1.0;
default.barName = '';
default.colCen = 'r';
default.colGeo = 'b';
default.colPts = 'b';
default.colVec = 'k';
default.colormap = [];
default.figHandle = 1;
default.gridQ = true;
default.markCen = 'o';
default.markPts = 'x';
default.opacity = 1.0;
default.showBarQ = true;
default.showCenQ = false;
default.showEleQ = false;
default.showGeoQ = true;
default.showNodQ = false;
default.showPtsQ = false;
default.showVarQ = false;
default.showVecQ = false;
default.title = '';
default.xlabel = 'X';
default.xlimit = [-10 10];
default.ylabel = 'Y';
default.ylimit = [-10 10];
default.zlabel = 'Z';
default.zlimit = [-10 10];

switch nargin
    case 4
        options = default;
    case 5
        options = default;
        if isempty(var)
            options.showVarQ = false;
        else
            options.showVarQ = true;
        end
    case 6
        options = completeInput(opts,default);
        if isempty(var)
            options.showVarQ = false;
        else
            options.showVarQ = true;
        end
    otherwise
        error('Unknown number of input arguments');
end

% Assignments
[m,n1] = size(elemNodes);
[~,n2] = size(elemPts);

tmp = [elemNodes;elemNodes(1,:)];
tmp1 = elemPts(:,elemNodes(1,:)); % coordinates of left line point
tmp2 = elemPts(:,elemNodes(2,:)); % coordinates of right line point

xplot = [tmp1(1,:);tmp2(1,:)];
yplot = [tmp1(2,:);tmp2(2,:)];

switch m
    case 2
        % line geometry as default
        twoDimQ = true;
    case 3
        % Triangle
        % tmp1 = coordinates of left corner
        % tmp2 = coordinates of right corner
        tmp3 = elemPts(:,elemNodes(3,:)); % coordinates of top corner
        
        xplot = [xplot ; tmp3(1,:)];
        yplot = [yplot ; tmp3(2,:)];
        zplot = [tmp1(3,:) ; tmp2(3,:) ; tmp3(3,:)];
        twoDimQ = false;
    case 4
        % Rectangle
        % tmp1 = coordinates of lower left corner
        % tmp2 = coordinates of lower right corner
        tmp3 = elemPts(:,elemNodes(3,:)); % coordinates of upper right corner
        tmp4 = elemPts(:,elemNodes(4,:)); % coordinates of upper left corner
        
        xplot = [xplot ; tmp3(1,:) ; tmp4(1,:)];
        yplot = [yplot ; tmp3(2,:) ; tmp4(2,:)];
        zplot = [tmp1(3,:) ; tmp2(3,:) ; tmp3(3,:) ; tmp4(3,:)];
        twoDimQ = false;
    otherwise
        error('Unknown geometry input');
end


% Visualization
figure(options.figHandle);
hold on;
alpha(options.opacity);
if ~isempty(options.colormap)
    colormap(gcf,options.colormap);
end
% axis image;
axis equal;
% axis vis3d;
if (isempty(options.title) == false)
	title(options.title);
end

if (twoDimQ == true)
    view(2);
    xlabel(options.xlabel);
    ylabel(options.ylabel);
    if (options.gridQ == true)
        set(gca,'Xgrid','on');
        set(gca,'Ygrid','on');
    end
       
    if (options.showGeoQ == true) % Show geometry without a variable
        line(elemPts(1,tmp),elemPts(2,tmp),'Color',options.colGeo);
    end
    
    if (options.showPtsQ == true) % Point coordinates
        plot(elemPts(1,:),elemPts(2,:),[options.colPts options.markPts]);
    end
    
    if (options.showEleQ == true) % Element indices
        for ii = 1:n1
            text(elemCen(1,ii),elemCen(2,ii),num2str(ii),'color',options.colCen);
        end
    end
    
    if (options.showNodQ == true) % Node indices
        for ii = 1:n2
            text(elemPts(1,ii),elemPts(2,ii),num2str(ii));
        end
    end
        
    if (options.showCenQ == true) % Center points of elements
        plot(elemCen(1,:),elemCen(2,:),[options.colCen options.markCen]);
    end
    
    if (options.showVecQ == true) % Normal vector of elements
        quiver(elemCen(1,:),elemCen(2,:),elemVec(1,:),elemVec(2,:), ...
               'Color',options.colVec);
    end
else
    view(3);
    xlabel(options.xlabel);
    ylabel(options.ylabel);
    zlabel(options.zlabel);
    if (options.gridQ == true)
        set(gca,'Xgrid','on');
        set(gca,'Ygrid','on');
        set(gca,'Zgrid','on');
    end
    
	if (options.showGeoQ == true) && (options.showVarQ == false)
        line(elemPts(1,tmp),elemPts(2,tmp),elemPts(3,tmp),'Color', ...
            options.colGeo);
	elseif (options.showGeoQ == true) && (options.showVarQ == true)
        fill3(xplot,yplot,zplot,var,'FaceAlpha',options.alphaF,'EdgeAlpha',options.alphaE);
        if (isempty(options.barName) == true) && (options.showBarQ)
            colorbar
        elseif (options.showBarQ)
            c = colorbar;
            c.Label.String = options.barName;
        end
	end
    
    if (options.showPtsQ == true) % Point coordinates
        plot3(elemPts(1,:),elemPts(2,:),elemPts(3,:), ...
              [options.colPts options.markPts]);
    end
    
    if (options.showEleQ == true) % Element indices
        for ii = 1:n1
            text(elemCen(1,ii),elemCen(2,ii),elemCen(3,ii),num2str(ii),'color',options.colCen);
        end
    end
    
    if (options.showNodQ == true) % Node indices
        for ii = 1:n2
            text(elemPts(1,ii),elemPts(2,ii),elemPts(3,ii),num2str(ii));
        end
    end
        
    if (options.showCenQ == true) % Center points of elements
        plot3(elemCen(1,:),elemCen(2,:),elemCen(3,:), ...
              [options.colCen options.markCen]);
    end
    
    if (options.showVecQ == true) % Normal vector of elements
        quiver3(elemCen(1,:),elemCen(2,:),elemCen(3,:), ...
                elemVec(1,:),elemVec(2,:),elemVec(3,:), ...
                'Color',options.colVec)
    end
    
    
end
hold off;

currentFig = gcf;


end