% [elemVec] = geom_op_calcElemVec(elemPts,elemNodes,normQ)
% 
% Calculates the individual element unit vectos for given points 'elemPts' 
% and nodes 'elemNodes'. The functions distinguish automatically between 
% the element shapes 'lines', 'triangles' and 'quads'. The optional
% argument 'normQ' is by default true and results in a normalized vector.
% 
%   Example:
%       vecs = geom_op_calcElemVec(pts,nodes);
% 
function [elemVec] = geom_op_calcElemVec(elemPts,elemNodes,normQ)

normVecQ = true;
if (nargin > 2)
    normVecQ = normQ;
end

[n,~] = size(elemNodes);
switch n
    case 2  % lines
        elemVec = v_cross(...
                elemPts(:,elemNodes(2,:)) - elemPts(:,elemNodes(1,:)),...
                [0 ; 0 ; 1]);
    case 3  % triangles
        elemVec = v_cross(...
                elemPts(:,elemNodes(1,:)) - elemPts(:,elemNodes(2,:)),...
                elemPts(:,elemNodes(3,:)) - elemPts(:,elemNodes(2,:)));
    case 4  % quads
        elemVec = v_cross(...
                elemPts(:,elemNodes(1,:)) - elemPts(:,elemNodes(3,:)),...
                elemPts(:,elemNodes(2,:)) - elemPts(:,elemNodes(4,:)));
    otherwise
        error('Unknown element shape');
end

if (normVecQ)
    elemVec = v_norm(elemVec);
end
            
end