% [X,Y,Z] = geom_2D_grid_circle(radius,divRadius,plane,offset)
% 
% Generates a user-defined circular grid:
% 
%   This function can be used to generate one or multiple grids in shape of
%   a circle with various radii given in 'radius' and specified divisions 
%   'divRadius' in a defined 'plane'. 
%   Their center points are by default at the origin of the coordinate 
%   system, but can be changed by specifing an 'offset'.
% 
%   Example:
%       coordsOut = geom_grid2D_circle([4 3],[2 1],'ZX',[2 0 1]);
% 
%   Output variants:
%    * X        = returns a '3 x n' matrix
%    * [X,Y,Z]  = returns 3 '1 x n' matrices
% 
%   Arguments for plane:
%    * 'xy', 'XY', 'Xy', 'xY', 'yx', 'YX', 'yX', 'Yx'
%    * 'xz', 'XZ', 'Xz', 'xZ', 'zx', 'ZX', 'zX', 'Zx'
%    * 'yz', 'YZ', 'Yz', 'yZ', 'zy', 'ZY', 'zY', 'Zy'
% 
function [X,Y,Z] = geom_2D_grid_circle(radius,divRadius,plane,offset)

% Input handling
defaultQ = false;
mA = 1;
mB = mA; nA = mA; nB = nA;

r = 1.0;
d = 0.5;
switch nargin
    case 0
        defaultQ = true;
    case 1
        [mA,nA] = size(radius);
        r = radius;
        mB = mA;
        nB = nA;
        d = repmat(d,mB,nB);
    case {2,3,4}
        [mA,nA] = size(radius);
        [mB,nB] = size(divRadius);
        r = radius;
        d = divRadius;
    otherwise
        error('Unknown number of input arguments')
end

% Error handling
if (mA ~= mB)
    error('Input arguments must have the number of rows');
end
if (nA ~= nB)
    error('Input arguments must have the number of columns');
end


% Execution
if (defaultQ == true)
    pts = geom_2D_contour_circle();
else
    pts = [];
    for i = 1:mA
        for j = 1:nA
            pts = [pts, geom_2D_contour_circle(r(i,j),d(i,j))];
        end    
    end
    
end
if (nargin > 2) || (nargout > 2)
    [~,n] = size(pts);
    pts = [pts;zeros(1,n)];
end


% Transformation
if (nargin > 2)
    switch plane
        case {'xy','XY','Xy','xY','yx','YX','yX','Yx'}
            % default
        case {'xz','XZ','Xz','xZ','zx','ZX','zX','Zx'}
            pts = op_rotate(pts,'X','-',pi/2);
        case {'yz','YZ','Yz','yZ','zy','ZY','zY','Zy'}
            pts = op_rotate(pts,'Y','+',pi/2);
        otherwise
            error('It is not possible to interpret the declared plane');
    end
end

if (nargin > 3)
    pts = op_displace(pts,offset);
end


%Output
switch nargout
    case {0,1}
        X = pts;
    case 2
        X = pts(1,:);
        Y = pts(2,:);
    case 3
        X = pts(1,:);
        Y = pts(2,:);
        Z = pts(3,:);
    otherwise
        error('Unknown number of output arguments');
end

end