% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_2D_contour_naca(design,divisions,spacing,zCoord,opts)
%
% Returns a list of edges of the panels that make up a NACA airfoil (only 4
% digits are supported at the moment) in 'elemPts' and the list of elements
% that make up the boundary are defined by the indices of the two vertices
% in 'elemNodes'.
% 
% Note: the vertices of each element in 'elemNodes' are defined in a
%       clockwise direction.
% 
%   Example:
%       opts.edge = false;
%       elemPts = geom_2D_naca('2412',100,'uni',10.2,opts);
% 
%   Arguments permitted for 'spacing' (default = 'cos'):
%    * 'cos', 'cosine', 'uni', 'uniform'
% 
%   Fields permitted for 'opts':
%       edge    - default = true (zero-thickness trailing edge)
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_2D_contour_naca(design,divisions,spacing,zCoord,opts)

% input handling
digit = '0015';
n = 50;
s = 'cosine';
zCoordsQ = false;
default.edge = true;

switch nargin
    case 0
    case 1
        digit = design;
    case 2
        digit = design;
        n = divisions;
    case 3
        digit = design;
        n = divisions;
        s = spacing;
    case {4,5}
        digit = design;
        n = divisions;
        s = spacing;
        zCoordsQ = true;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 6
    options = default;
else
    options = completeInput(opts,default);
end


% interpretation
if (length(digit) == 4)
    elemPts = geom_gen_2D_naca4(digit,n,s,options);
elseif (length(digit) == 5)   
    elemPts = geom_gen_2D_naca5(digit,n,s,options);
% elseif (length(digit) == 6) %% TODO
% elseif (length(digit) == 7)
end
elemPts = [elemPts elemPts(:,1)];

if (zCoordsQ == true)
     elemPts = [elemPts; ...
                repmat(zCoord,1,2*n + 1)]; 
end


% lines
if (nargout > 1)
    elemNodes = [1:2*n; 2:(2*n + 1)];
    elemNodes(2,end) = 1;
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    tmp = elemPts;
    if (zCoordsQ == false)
        tmp = [tmp ; zeros(1,2*n + 1)];
    end
    elemVec = v_norm(v_cross(tmp(:,elemNodes(1,:)) - ...
                             tmp(:,elemNodes(2,:)),[0 ; 0 ; 1]));
end
    

% line lengths
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemPts,elemNodes,true);
end


% output handling
switch nargout
    case {0,1}
        elemPts = elemPts(:,1:2*n);
    case {2,3,4,5}
        % default
    otherwise
        error('Unknown number of output arguments');
end

end