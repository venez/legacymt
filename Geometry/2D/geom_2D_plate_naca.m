% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_2D_naca(design,divisions,spacing,zCoord,opts)
%
% Returns a list of edges of the panels that make up a NACA airfoil (only 4
% digits are supported at the moment) in 'elemPts' and the list of elements
% that make up the boundary are defined by the indices of the two vertices
% in 'elemNodes'.
% 
% Note: the vertices of each element in 'elemNodes' are defined in a
%       clockwise direction.
% 
%   Example:
%       opts.edge = false;
%       elemPts = geom_2D_naca('2412',100,'uni',10.2,opts);
% 
%   Arguments permitted for 'spacing' (default = 'cos'):
%    * 'cos', 'cosine', 'uni', 'uniform'
% 
%   Fields permitted for 'opts':
%       edge    - default = true (zero-thickness trailing edge)
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_2D_plate_naca(design,divC,divR,spacing,zCoord,opts)

% input handling
digit = '0015';
nC = 11;
nR = 2;
s = 'cosine';
z = 1.0;
default.clockQ = true;
default.edge = true;
default.warnQ = true;

switch nargin
    case 0
    case 1
        digit = design;
    case 2
        digit = design;
        nC = divC;
    case 3
        digit = design;
        nC = divC;
        nR = divR;
    case 4
        digit = design;
        nC = divC;
        nR = divR;
        s = spacing;
    case {5,6}
        digit = design;
        nC = divC;
        nR = divR;
        s = spacing;
        z = zCoord;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 6
    options = default;
else
    options = completeInput(opts,default);
end


% coordinates
n = 2*nC;
switch length(digit)
    case 4
        [tmpPts,~,~,camberPts] = geom_gen_2D_naca4(digit,nC,s,options);
    case 5
        [tmpPts,~,~,camberPts] = geom_gen_2D_naca5(digit,nC,s,options);
%     case 6    % TODO
%     case 7
end


for i = 1:nR
    fac = 1/nR * i;
    if (i == 1)
        elemPts = [[camberPts(:,2:end-1); repmat(z,1,nC-1)] ...
                   [tmpPts; repmat(z,1,n)]];
        elemPts = op_scale(elemPts,[fac fac 1]);
        elemPts = op_displace(elemPts,[(nR-1)/nR/2 0 0],options.warnQ);
    elseif (i < nR)
        tmpPts2 = op_scale([tmpPts; repmat(z,1,n)],[fac fac 1]);
        tmpPts2 = op_displace(tmpPts2,[(nR-i)/nR/2 0 0],options.warnQ);
        elemPts = [elemPts tmpPts2];
    else
        elemPts = [elemPts [tmpPts; repmat(z,1,n)]];
    end
end


% elements (here: triangles)
if (nargout > 1)
    nT = nC - 1;
    
    % upper side, inner section
    tmpNodesIn = [nC ; nC + 1 ; 1];
    tmpNodesIn = [tmpNodesIn , ...
                 [1 : nT - 1 ; nC + 1: nC+nT-1 ; 2 : nT] ];
	tmpNodesIn = [tmpNodesIn , ...
                 [nC + 1 : nC+nT-1 ; nC + 2 : nC+nT ; 2 : nT] ];
    tmpNodesIn = [tmpNodesIn , ...
                 [nC+nT; nC+nT+1 ; nT ] ];
    
    % lower side, inner section
    tmpNodesIn = [tmpNodesIn ...
                 [nC+nT+1 ; nC+nT+2 ; nT ] ];
    tmpNodesIn = [tmpNodesIn ...
                 [(nC+2 : n-1)+nT ; flip([1 : nT-1 ; 2 : nT],2)] ];
    tmpNodesIn = [tmpNodesIn ...
                 [(nC+2 : n-1)+nT ; (nC+3 : n)+nT ; flip(1 : nT-1)] ];
	tmpNodesIn = [tmpNodesIn ...
                 [n+nT ; nC ; 1] ];

    % outer sections
    tmpNodesOut = [];
    for i = 1:nR - 1
        firstNode = 1 + (i-1) * n + nT;
        secondNode = firstNode + n;
        offset = 1;
        
        tmpNodesOut = [tmpNodesOut , ...
                       geom_gen_nodesForElem('tri',n,firstNode,secondNode,offset,options)];
    end
    
    if options.clockQ
        elemNodes = [flip(tmpNodesIn) , tmpNodesOut];
    else
        elemNodes = [tmpNodesIn , tmpNodesOut];
    end 
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,true);
end

    
% element sizes
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemPts,elemNodes,true);
end


% output handling
switch nargout
    case {0,1}
        elemPts = elemPts(:,1:2*nC);
    case {2,3,4,5}
        % default
    otherwise
        error('Unknown number of output arguments');
end

end