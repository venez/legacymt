% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_2D_plateParallelogram(lengthX,lengthY,phi,divX,divY,zCoord)
% 
% Returns a list of edges of the panels that make up the parallelogram 
% shaped plane in 'elemPts' and the list of elements that make up the 
% boundary are defined by the indices of the three vertices in 'elemNodes'.
% 
% NOTE: the vertices of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_2D_plateParallelogram(lengthX,lengthY,phi,divX,divY,zCoord)

% input handling
x = 1.0;
y = 0.5;
p = pi/4;
z = 0;
nX = 10;
nY = 5;

switch nargin
    case 0
    case 1
        x = lengthX;
    case 2
        x = lengthX;
        y = lengthY;
    case 3
        x = lengthX;
        y = lengthY;
        p = phi;
	case 4
        x = lengthX;
        y = lengthY;
        p = phi;
        nX = divX;
    case 5
        x = lengthX;
        y = lengthY;
        p = phi;
        nX = divX;
        nY = divY;
    case 6
        x = lengthX;
        y = lengthY;
        p = phi;
        nX = divX;
        nY = divY;
        z = zCoord;
    otherwise
        error('Unknown number of input argument');
end

% coordinates
h = y*sin(p);
tmpX = -x/2 : x/nX : +x/2;
tmpY = -h/2 : h/nY : +h/2;
tmpZ = z*ones(1,nY+1);
dX = tmpY/tan(p);
elemPts = [];
for i = 1:nX+1
   elemPts = [elemPts , [repmat(tmpX(i),1,nY+1) + dX ; tmpY ; tmpZ]];
end

% Elements (here: triangles)
if (nargout > 1)
    elemNodes = [];
    for i = 1:nX
        startElem = 1 + (i-1)*(nY+1);
        bridgElem = startElem + nY + 1;
        endElem   = i - 1 + i * nY;
        elemNodes = [elemNodes, ...
                     [startElem + 1 :endElem + 1;...
                      bridgElem     :bridgElem + nY - 1;...
                      startElem     :endElem]];
                  
        elemNodes = [elemNodes,...
                     [startElem + 1 :endElem + 1;...
                      bridgElem + 1 :bridgElem + nY;...
                      bridgElem     :bridgElem + nY - 1]];
    end
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output argument');
end

end
