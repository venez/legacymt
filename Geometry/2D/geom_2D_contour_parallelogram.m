% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_2D_contour_parallelogram(major,minor,phi,divMajor,divMinor,zCoord)
% 
% Returns a list of edges of the panels that make up the parallelogram in
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the two vertices in 'elemNodes'.
% 
% NOTE: the vertices of each element in 'elemNodes' are defined in a
%       counter-clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_2D_contour_parallelogram(major,minor,phi,divMajor,divMinor,zCoord)

% input handling
a = 1.0;
b = 0.5;
p = pi/4;
na = 10;
nb = 5;
zCoordsQ = false;

switch nargin
    case 0
    case 1
        a = major;
    case 2
        a = major;
        b = minor;
    case 3
        a = major;
        b = minor;
        p = phi;
    case 4
        a = major;
        b = minor;
        p = phi;
        na = divMajor;
    case 5
        a = major;
        b = minor;
        p = phi;
        na = divMajor;
        nb = divMinor;
    case 6
        a = major;
        b = minor;
        p = phi;
        na = divMajor;
        nb = divMinor;
        zCoordsQ = true;
    otherwise
        error('Unknown number of input argument');
end


% coordinates
n = 2*(na+nb);
h = b*sin(p);
dX = h/tan(p);

tmp1 = linspace(a/2,a/2,nb+1);
% tmp1 = tmp1(1:nb);
tmp2 = linspace(a/2,-a/2,na+1);
% tmp2 = tmp2(1:na);

CB_X = tmp1 + linspace(h,0,nb+1)/tan(p);
BA_X = tmp2 ;
AD_X = -tmp1 + linspace(0,h,nb+1)/tan(p);
DC_X = -tmp2 + dX;

CB_Y = linspace(h/2,-h/2,nb+1);
BA_Y = repmat(-h/2,1,na+1);
AD_Y = linspace(-h/2,h/2,nb+1);
DC_Y = repmat(h/2,1,na+1);

if (zCoordsQ == false)
    elemPts = [flip([CB_X(1:end-1) BA_X(1:end-1) AD_X(1:end-1) DC_X(1:end-1)])
               flip([CB_Y(1:end-1) BA_Y(1:end-1) AD_Y(1:end-1) DC_Y(1:end-1)])];
	elemPts = [elemPts , [a/2 + dX;h/2]];
else
    elemPts = [flip([CB_X(1:end-1) BA_X(1:end-1) AD_X(1:end-1) DC_X(1:end-1)])
               flip([CB_Y(1:end-1) BA_Y(1:end-1) AD_Y(1:end-1) DC_Y(1:end-1)])
               repmat(zCoord,1,n)];
	elemPts = [elemPts , [a/2 + dX;h/2;zCoord]];
end

% lines
if (nargout > 1)
    elemNodes = [1:n; 2:(n+1)];
    elemNodes(2,n) = 1;
end

% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end

% normal unit vector to elements
if (nargout > 3)
    tmp = elemPts;
    if (zCoordsQ == false)
        tmp = [tmp ; zeros(1,n+1)];
    end
    elemVec = v_norm(v_cross(tmp(:,elemNodes(2,:)) - ...
                             tmp(:,elemNodes(1,:)),[0 ; 0 ; 1]));
end

% line lengths
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemPts,elemNodes,true);
end

% output handling
switch nargout
    case {0,1}
        elemPts = elemPts(:,1:n-1);
    case {2,3,4,5}
        elemPts = elemPts(:,1:n);
    otherwise
        error('Unknown number of output arguments');
end

end