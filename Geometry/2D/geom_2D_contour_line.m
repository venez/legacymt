% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_2D_contour_line(length,divisions,zCoord)
%
% Returns a list of segments of the element that make up a line in 
% 'elemPts'. The list of elements that make up the boundary are defined
% by the indices of the two vertices in 'elemNodes'.
% 
% Note: the vertices of each element in 'elemNodes' are defined in a
%       positive X-direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_2D_contour_line(length,divisions,zCoord)

% input handling
n = 10;
l = 1.0;
zCoordsQ = false;

switch nargin
    case 0
    case 1
        l = length;
    case 2
        n = divisions;
        l = length;
    case 3
        n = divisions;
        l = length;
        zCoordsQ = true;
    otherwise
        error('Unknown number of input arguments');
end

% coordinates
elemPts = [linspace(-l/2,l/2,n + 1); ...
           zeros(1,n+1)];
if (zCoordsQ == true)
     elemPts = [elemPts; ...
                repmat(zCoord,1,n + 1)]; 
end

% lines
if (nargout > 1)
    elemNodes = [1:n; 2:n+1];
end

% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end

% normal unit vector to elements
if (nargout > 3)
    elemVec = [zeros(1,n); ...
               ones(1,n)];
	if (zCoordsQ == true)
        elemVec = [elemVec; ...
                   repmat(zCoord,1,n)]; 
	end
end
    
% line lengths
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemPts,elemNodes,true);
end


end


% % [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
% %       geom_2D_contour_line(length,divisions,zCoord)
% %
% % Returns a list of segments of the element that make up a line in 
% % 'elemPts'. The list of elements that make up the boundary are defined
% % by the indices of the two vertices in 'elemNodes'.
% % 
% % 
% function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%             geom_2D_contour_line(length,divisions,zCoord,opts)
% 
% % input handling
% n = 10;
% l = 1.0;
% zCoordsQ = true;
% default.clockQ = false;
% zCoord= 1.1;
% switch nargin
%     case 0
%     case 1
%         l = length;
%     case 2
%         n = divisions;
%         l = length;
%     case 3
%         n = divisions;
%         l = length;
%         zCoordsQ = true;
%     otherwise
%         error('Unknown number of input arguments');
% end
% if nargin ~= 4
%     options = default;
% else
%     options = completeInput(opts,default);
% end
% 
% 
% % coordinates
% elemPts = [linspace(-l/2,l/2,n + 1) ; zeros(1,n+1)];
% if (zCoordsQ == true)
%      elemPts = [elemPts ; repmat(zCoord,1,n + 1)]; 
% end
% 
% 
% % lines
% if (nargout > 1)
%     options.closedQ = false;
%     elemNodes = geom_gen_nodesForElem('line',n,1,2,0,options);
% end
% 
% 
% % center points of elements
% if (nargout > 2)
%     elemCen = geom_op_calcElemCen(elemPts,elemNodes);
% end
% 
% 
% % normal unit vector to elements
% if (nargout > 3)
%     elemVec = [zeros(1,n); ...
%                ones(1,n)];
% 	if (zCoordsQ == true)
%         elemVec = [elemVec; ...
%                    repmat(zCoord,1,n)]; 
% 	end
% end
% 
%     
% % line lengths
% if (nargout > 4)
%     elemSize = geom_op_calcElemSize(elemPts,elemNodes,true);
% end
% 
% 
% end