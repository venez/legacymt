% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_2D_arc_circle(radius,phi,divisions,zCoord)
%
% Returns a list of edges of the panels that make up the circle arc in  
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the two vertices in 'elemNodes'.
% 
% Note: the vertices of each element in 'elemNodes' are defined in a
%       counter-clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_2D_arc_circle(radius,phi,divisions,zCoord)

% input handling
n = 36;
r = 1.0;
p = [0 pi];
zCoordsQ = false;

switch nargin
    case 0
    case 1
        r = radius;
    case 2
        p = phi;
        r = radius;
    case 3
        n = divisions;
        p = phi;
        r = radius;
    case 4
        n = divisions;
        p = phi;
        r = radius;
        zCoordsQ = true;
    otherwise
        error('Unknown number of input arguments');
end

% complete input
if (length(p) == 1)
	phiE = p;
	phiS = 0;
else
	phiE = max(p);
	phiS = min(p);
end
if (phiS == phiE - 2*pi)
    closedQ = true;
else
    closedQ = false;
end

% error handling
if (length(p) ~= 1) && (length(p) ~= 2)
	error('Unknown specification for 3rd argument')
end
if (phiS == phiE) || (phiE-phiS > 2*pi)
    error('Illegal specification regarding given angle range')
end

% coordinates
angles = phiS:(phiE-phiS)/n:phiE;
elemPts = [r*sin(angles); ...
           r*cos(angles)];
if (zCoordsQ == true)
     elemPts = [elemPts; ...
                repmat(zCoord,1,n+1)]; 
end

% lines
if (nargout > 1) && (closedQ)
    elemNodes = [1:n; 2:(n+1)];
    elemNodes(2,n) = 1;
elseif (nargout > 1) && (~closedQ)
    elemNodes = [1:n; 2:(n+1)];
end

% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end

% normal unit vector to elements
if (nargout > 3)
    angles = angles + pi/n;
    elemVec = [cos(angles); ...
               sin(angles)];
	if (zCoordsQ == true)
        elemVec = [elemVec; ...
                   repmat(zCoord,1,n+1)]; 
	end
	elemVec = elemVec(:,1:n);
end
    
% line lengths
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemPts,elemNodes,true);
end

% output handling
switch nargout
    case {0,1}
        elemPts = elemPts(:,1:end);
    case {2,3,4,5}
        % default
    otherwise
        error('Unknown number of output arguments');
end

end