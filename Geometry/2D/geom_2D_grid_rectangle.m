% [X,Y,Z] = geom_2D_grid_rectangle(major,minor,divMajor,divMinor,plane,offset)
% 
% Generates a user-defined rectangular grid:
% 
%   This function can be used to generate one or multiple grids in shape of
%   a rectangle of widths 'major' and heights 'minor' with specified
%   divisions 'divMajor' and 'divMinor' in a defined 'plane'. 
%   Their center points are by default at the origin of the coordinate 
%   system, but can be changed by specifing an 'offset'.
% 
%   Example:
%       coordsOut = geom_grid2D_rectangle([4 3],[2 1],[10 8],[6 4],'ZX',[2 0 1]);
% 
%   Output variants:
%    * X        = returns a '3 x n' matrix
%    * [X,Y,Z]  = returns 3 '1 x n' matrices
% 
%   Arguments for plane:
%    * 'xy', 'XY', 'Xy', 'xY', 'yx', 'YX', 'yX', 'Yx'
%    * 'xz', 'XZ', 'Xz', 'xZ', 'zx', 'ZX', 'zX', 'Zx'
%    * 'yz', 'YZ', 'Yz', 'yZ', 'zy', 'ZY', 'zY', 'Zy'
% 
function [X,Y,Z] = geom_2D_grid_rectangle(major,minor,divMajor,divMinor,plane,offset)

% Input handling
defaultQ = false;
mA = 1;
mB = mA; mDA = mA; mDB = mA;
nA = mA;
nB = nA; nDA = nA; nDB = nA;

A = 1.0;
B = 0.5;
DA = 10;
DB = 5;
switch nargin
    case 0
        defaultQ = true;
    case 1
        [mA,nA] = size(major);
        A = major;
        mB = mA; mDA = mA; mDB = mA;
        nB = nA; nDA = nA; nDB = nA;
        B = repmat(B,mB,nB);
        DA = repmat(DA,mDA,nDA);
        DB = repmat(DB,mDB,mDB);
    case 2
        [mA,nA] = size(major);
        [mB,nB] = size(minor);
        A = major;
        B = minor;
        mDA = mA; mDB = mA;
        nDA = nA; nDB = nA;
        DA = repmat(DA,mDA,nDA);
        DB = repmat(DB,mDB,mDB);
    case 3
        [mA,nA] = size(major);
        [mB,nB] = size(minor);
        [mDA,nDA] = size(divMajor);
        A = major;
        B = minor;
        DA = divMajor;
        mDB = mA;
        nDB = nA;
        DB = repmat(DB,mDB,mDB);
    case {4,5,6}
        [mA,nA] = size(major);
        [mB,nB] = size(minor);
        [mDA,nDA] = size(divMajor);
        [mDB,nDB] = size(divMinor);
        A = major;
        B = minor;
        DA = divMajor;
        DB = divMinor;
    otherwise
        error('Unknown number of input arguments');
end

% Error handling
if (mA ~= mB) || (mA ~= mDA) || (mA ~= mDB)
    error('Input arguments must have the same number of rows');
end
if (nA ~= nB) || (nA ~= nDA) || (nA ~= nDB)
    error('Input arguments must have the same number of columns');
end


% Execution
if (defaultQ == true)
    pts = geom_2D_rectangle();
else
    pts = [];
    for i = 1:mA
        for j = 1:nA
            pts = [pts, geom_2D_contour_rectangle(A(i),B(j),DA(i),DB(j))];
        end    
    end
end
if (nargin > 4) || (nargout > 2)
	[~,n] = size(pts);
    pts = [pts ; zeros(1,n)];
end


% Transformation    
if (nargin > 4)
    pts = [pts;zeros(1,n)];
    switch plane
        case {'xy','XY','Xy','xY','yx','YX','yX','Yx'}
            % default
        case {'xz','XZ','Xz','xZ','zx','ZX','zX','Zx'}
            pts = op_rotate(pts,'X','-',pi/2);
        case {'yz','YZ','Yz','yZ','zy','ZY','zY','Zy'}
            pts = op_rotate(pts,'Y','+',pi/2);
        otherwise
            error('It is not possible to interpret the declared plane');
    end
end

if (nargin > 5)
    pts = op_displace(pts,offset);
end


%Output
switch nargout
    case {0,1}
        X = pts;
    case 2
        X = pts(1,:);
        Y = pts(2,:);
    case 3
        X = pts(1,:);
        Y = pts(2,:);
        Z = pts(3,:);
    otherwise
        error('Unknown number of output arguments');
end

warning('Function delivers still unsatisfying results');
end