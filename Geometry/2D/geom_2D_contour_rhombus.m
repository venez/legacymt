% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%             geom_2D_contour_rhombus(major,phi,divisions,zCoord)
% 
% Returns a list of edges of the panels that make up the rectangle in
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the two vertices in 'elemNodes'.
% 
% NOTE: the vertices of each element in 'elemNodes' are defined in a
%       clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_2D_contour_rhombus(major,phi,divisions,zCoord)

% input handling
x = 2.0;
n = 10;
p = pi/4;
zCoordsQ = false;

switch nargin
    case 0
    case 1
        x = major;
    case 2
        x = major;
        p = phi;
    case 3
        x = major;
        p = phi;
        n = divisions;
    case 4
        x = major;
        p = phi;
        n = divisions;
        zCoordsQ = true;
    otherwise
        error('Unknown number of input argument');
end

% coordinates
a = sqrt(2*(x/2)^2);
switch nargout
    case 1
        [elemPts] = geom_2D_contour_parallelogram(a,a,p,n,n);
    case 2
        [elemPts,elemNodes] = geom_2D_contour_parallelogram(a,a,p,n,n);
    case 3
        [elemPts,elemNodes,elemCen] = geom_2D_contour_parallelogram(a,a,p,n,n);
    case 4
        [elemPts,elemNodes,elemCen,elemVec] = geom_2D_contour_parallelogram(a,a,p,n,n);
    case 5
        [elemPts,elemNodes,elemCen,elemVec,elemSize] = geom_2D_contour_parallelogram(a,a,p,n,n);
    otherwise
        error('Unknown number of output arguments');
end

if (zCoordsQ)
    elemPts = [elemPts(1:2,:)
               repmat(zCoord,1,length(elemPts))];
else
    elemPts = elemPts(1:2,:);
end

end