% [X,Y,Z] = geom_2D_grid_parallelogram(major,minor,height,divMajor,divMinor,plane,offset)
% 
% Generates a user-defined rectangular grid:
% 
%   This function can be used to generate one or multiple grids in shape of
%   a rectangle of widths 'major' and heights 'minor' with specified
%   divisions 'divMajor' and 'divMinor' in a defined 'plane'. 
%   Their center points are by default at the origin of the coordinate 
%   system, but can be changed by specifing an 'offset'.
% 
%   Example:
%       coordsOut = geom_grid2D_parallelogram([6 5],[5 3],[pi/4 pi/4],[10 8],[6 4],'ZX',[2 0 1]);
% 
%   Output variants:
%    * X        = returns a '3 x n' matrix
%    * [X,Y,Z]  = returns 3 '1 x n' matrices
% 
%   Arguments for plane:
%    * 'xy', 'XY', 'Xy', 'xY', 'yx', 'YX', 'yX', 'Yx'
%    * 'xz', 'XZ', 'Xz', 'xZ', 'zx', 'ZX', 'zX', 'Zx'
%    * 'yz', 'YZ', 'Yz', 'yZ', 'zy', 'ZY', 'zY', 'Zy'
% 
function [X,Y,Z] = geom_2D_grid_parallelogram(major,minor,phi,divMajor,divMinor,plane,offset)

% Input handling
defaultQ = false;
mA = 1;
mB = mA; mP = mA; mDA = mA; mDB = mA;
nA = mA;
nB = nA; nP = nA; nDA = nA; nDB = nA;

A = 1.0;
B = 0.5;
P = pi/4;
DA = 10;
DB = 5;
switch nargin
    case 0
        defaultQ = true;
    case 1
        [mA,nA] = size(major);
        A = major;
        mB = mA; mP = mA; mDA = mA; mDB = mA;
        nB = nA; nP = nA; nDA = nA; nDB = nA;
        B = repmat(B,mB,nB);
        DA = repmat(DA,mDA,nDA);
        DB = repmat(DB,mDB,mDB);
    case 2
        [mA,nA] = size(major);
        [mB,nB] = size(minor);
        A = major;
        B = minor;
        mP = mA; mDA = mA; mDB = mA;
        nP = nA; nDA = nA; nDB = nA;
        DA = repmat(DA,mDA,nDA);
        DB = repmat(DB,mDB,mDB);
    case 3
        [mA,nA] = size(major);
        [mB,nB] = size(minor);
        [mP,nP] = size(phi);
        A = major;
        B = minor;
        P = phi;
        mDA = mA; mDB = mA;
        nDA = nA; nDB = nA;
        DA = repmat(DA,mDA,nDA);
        DB = repmat(DB,mDB,mDB);
    case 4
        [mA,nA] = size(major);
        [mB,nB] = size(minor);
        [mP,nP] = size(phi);
        [mDA,nDA] = size(divMajor);
        A = major;
        B = minor;
        P = phi;
        DA = divMajor;
        mDB = mA;
        nDB = nA;
        DB = repmat(DB,mDB,mDB);
    case {5,6,7}
        [mA,nA] = size(major);
        [mB,nB] = size(minor);
        [mP,nP] = size(phi);
        [mDA,nDA] = size(divMajor);
        [mDB,nDB] = size(divMinor);
        A = major;
        B = minor;
        P = phi;
        DA = divMajor;
        DB = divMinor;
    otherwise
        error('Unknown number of input arguments');
end

% Error handling
if (mA ~= mB) || (mA ~= mP) || (mA ~= mDA) || (mA ~= mDB)
    error('Input arguments must have the number of rows');
end
if (nA ~= nB) || (nA ~= nP) || (nA ~= nDA) || (nA ~= nDB)
    error('Input arguments must have the number of columns');
end


% Execution
if (defaultQ == true)
    pts = geom_2D_rectangle();
else
    pts = [];
    for i = 1:mA
        for j = 1:nA
            pts = [pts, ...
                geom_2D_contour_parallelogram(A(i),B(j),P(j),DA(i),DB(j))];
        end    
    end
end
if (nargin > 5) || (nargout > 2)
	[~,n] = size(pts);
    pts = [pts ; zeros(1,n)];
end


% Transformation
if (nargin > 5)
    switch plane
        case {'xy','XY','Xy','xY','yx','YX','yX','Yx'}
            % default
        case {'xz','XZ','Xz','xZ','zx','ZX','zX','Zx'}
            pts = op_rotate(pts,'X','-',pi/2);
        case {'yz','YZ','Yz','yZ','zy','ZY','zY','Zy'}
            pts = op_rotate(pts,'Y','+',pi/2);
        otherwise
            error('It is not possible to interpret the declared plane');
    end
end

if (nargin > 6)
    pts = op_displace(pts,offset);
end


%Output
switch nargout
    case {0,1}
        X = pts;
    case 2
        X = pts(1,:);
        Y = pts(2,:);
    case 3
        X = pts(1,:);
        Y = pts(2,:);
        Z = pts(3,:);
    otherwise
        error('Unknown number of output arguments');
end

warning('Function delivers still unsatisfying results');
end