% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_2D_plateRhombus(major,minor,divMajor,divMinor,zCoord)
% 
% Returns a list of edges of the panels that make up the rhombus shaped 
% plane in 'elemPts' and the list of elements that make up the boundary are
% defined by the indices of the three vertices in 'elemNodes'.
% 
% NOTE: the vertices of each element in 'elemNodes' are defined in a
%       counter clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_2D_plate_rhombus(length,divisions,phi,zCoord)

% input handling
x = 1.0;
p = pi/4;
z = 0;
n = 10;

switch nargin
    case 0
    case 1
        x = length;
    case 2
        x = length;
        n = divisions;
    case 3
        x = length;
        p = phi;
    case 4
        x = length;
        p = phi;
        n = divisions;
        z = zCoord;
    otherwise
        error('Unknown number of input argument');
end


% coordinates
[elemPts,elemNodes] = geom_2D_plate_parallelogram(x,x,p,n,n,z);
elemPts = op_rotate(elemPts,'Z','+',p/2);


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output argument');
end

end
