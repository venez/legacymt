% [X,Y,Z] = geom_2D_grid_ellipse(major,minor,div,plane,offset)
% 
% Generates a user-defined elliptical grid:
% 
%   This function can be used to generate one or multiple grids in shape of
%   an ellipse of widths 'major' and heights 'minor' with specified
%   divisions 'div' around the circumference in a defined 'plane'. 
%   Their center points are by default at the origin of the coordinate 
%   system, but can be changed by specifing an 'offset'.
% 
%   Example:
%       coordsOut = geom_grid2D_ellipse([4 3],[2 1],[6 4],'ZX',[2 0 1]);
% 
%   Output variants:
%    * X        = returns a '3 x n' matrix
%    * [X,Y,Z]  = returns 3 '1 x n' matrices
% 
%   Arguments for plane:
%    * 'xy', 'XY', 'Xy', 'xY', 'yx', 'YX', 'yX', 'Yx'
%    * 'xz', 'XZ', 'Xz', 'xZ', 'zx', 'ZX', 'zX', 'Zx'
%    * 'yz', 'YZ', 'Yz', 'yZ', 'zy', 'ZY', 'zY', 'Zy'
% 
function [X,Y,Z] = geom_2D_grid_ellipse(major,minor,div,plane,offset)
   
% Input handling
defaultQ = false;
mA = 1;
mB = mA; mC = mA;
nA = mA;
nB = nA; nC = nA;

A = 1.0;
B = 0.5;
D = 10;
switch nargin
    case 0
        defaultQ = true;
    case 1
        [mA,nA] = size(major);
        A = major;
        mB = mA; mC = mA;
        nB = nA; nC = nA;
        B = repmat(B,mB,nB);
        D = repmat(D,mC,nC);
    case 2
        [mA,nA] = size(major);
        [mB,nB] = size(minor);
        A = major;
        B = minor;
        mC = mA;
        nC = nA;
        D = repmat(D,mC,nC);
    case {3,4,5}
        [mA,nA] = size(major);
        [mB,nB] = size(minor);
        [mC,nC] = size(div);
        A = major;
        B = minor;
        D = div;
    otherwise
        error('Unknown number of input arguments');
end

% Error handling
if (mA ~= mB) || (mA ~= mC)
    error('Input arguments must have the number of rows');
end
if (nA ~= nB) || (nA ~= nC)
    error('Input arguments must have the number of columns');
end


% Execution
if (defaultQ == true)
    pts = geom_2D_ellipse();
else
    pts = [];
    for i = 1:mA
        for j = 1:nA
            pts = [pts, geom_2D_contour_ellipse(A(i,j),B(i,j),D(i,j))];
        end    
    end
end
if (nargin > 3) || (nargout > 2)
    [~,n] = size(pts);
    pts = [pts;zeros(1,n)];
end


% Transformation
if (nargin > 3)
    switch plane
        case {'xy','XY','Xy','xY','yx','YX','yX','Yx'}
            % default
        case {'xz','XZ','Xz','xZ','zx','ZX','zX','Zx'}
            pts = op_rotate(pts,'X','-',pi/2);
        case {'yz','YZ','Yz','yZ','zy','ZY','zY','Zy'}
            pts = op_rotate(pts,'Y','+',pi/2);
        otherwise
            error('It is not possible to interpret the declared plane');
    end
end

if (nargin > 4)
    pts = op_displace(pts,offset);
end


%Output
switch nargout
    case {0,1}
        X = pts;
    case 2
        X = pts(1,:);
        Y = pts(2,:);
    case 3
        X = pts(1,:);
        Y = pts(2,:);
        Z = pts(3,:);
    otherwise
        error('Unknown number of output arguments');
end

end