% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_2D_arc_ellipse(majorAxis,minorAxis,phi,divisions,zCoord)
%
% Returns a list of edges of the panels that make up the ellipse arc in  
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the two vertices in 'elemNodes'.
% 
% Note: the vertices of each element in 'elemNodes' are defined in a
%       counter-clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_2D_arc_ellipse(majorAxis,minorAxis,phi,divisions,zCoord)

% input handling
n = 36;
a = 1.0;
b = 0.5;
p = [0 2*pi];
zCoordsQ = false;

switch nargin
    case 0
    case 1
        a = majorAxis;
    case 2
        a = majorAxis;
        b = minorAxis;
    case 3
        a = majorAxis;
        b = minorAxis;
        p = phi;
    case 4
        a = majorAxis;
        b = minorAxis;
        n = divisions;
        p = phi;
    case 5
        a = majorAxis;
        b = minorAxis;
        n = divisions;
        p = phi;
        zCoordsQ = true;
    otherwise
        error('Unknown number of input arguments');
end

% complete input
if (length(p) == 1)
	phiE = p;
	phiS = 0;
else
	phiE = max(p);
	phiS = min(p);
end
if (phiS == phiE - 2*pi) 
    closedQ = true;
else
    closedQ = false;
end

% error handling
if (length(p) ~= 1) && (length(p) ~= 2)
	error('Unknown specification for 3rd argument')
end
if (phiS == phiE) || (phiE-phiS > 2*pi)
    error('Illegal specification regarding given angle range')
end

% coordinates
angles = phiS:(phiE-phiS)/n:phiE;
elemPts = [a*sin(angles); ...
           b*cos(angles)];
if (zCoordsQ == true)
     elemPts = [elemPts; ...
                repmat(zCoord,1,n+1)]; 
end

% lines
if (nargout > 1) && (closedQ)
    elemNodes = [1:n; 2:(n+1)];
    elemNodes(2,n) = 1;
elseif (nargout > 1) && (~closedQ)
    elemNodes = [1:n; 2:(n+1)];
end

% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end

% normal unit vector to elements
if (nargout > 3)
    angles = angles + pi/n;
    elemVec = [cos(angles); ...
               sin(angles)];
	if (zCoordsQ == true)
        elemVec = [elemVec; ...
                   repmat(zCoord,1,n+1)]; 
	end
	elemVec = elemVec(:,1:n);
end
    
% line lengths
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemPts,elemNodes,true);
end

% output handling
switch nargout
    case {0,1}
        elemPts = elemPts(:,1:n);
    case {2,3,4,5}
        % default
    otherwise
        error('Unknown number of output arguments');
end

end