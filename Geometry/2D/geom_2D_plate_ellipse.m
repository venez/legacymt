% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_2D_plate_ellipse(majorAxis,minorAxis,divC,divR,zCoord,opts)
% 
% Returns a list of edges of the panels that make up the ellipse shaped 
% plane in 'elemPts' and the list of elements that make up the boundary are
% defined by the indices of the three vertices in 'elemNodes'.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_2D_plate_ellipse(majorAxis,minorAxis,divC,divR,zCoord,opts)

% input handling
a = 1.0;
b = 0.5;
nC = 36;
nR = 3;
z = 0;
default.clockQ = true;
default.triQ = true;

switch nargin
    case 0
    case 1
        a = majorAxis;
    case 2
        a = majorAxis;
        b = minorAxis;
    case 3
        a = majorAxis;
        b = minorAxis;
        nC = divC;
	case 4
        a = majorAxis;
        b = minorAxis;
        nC = divC;
        nR = divR;
    case {5,6}
        a = majorAxis;
        b = minorAxis;
        nC = divC;
        nR = divR;
        z = zCoord;
    otherwise
        error('Unknown number of input arguments');
end
if nargin ~= 6
    options = default;
else
    options = completeInput(opts,default);
end


% coordinates
A = (a/nR : a/nR : a);
B = (b/nR : b/nR : b);
if options.triQ
    elemPts = [0 ; 0 ; z];
    tmp = 1;
    type = 'tri';
else
    elemPts = geom_2D_contour_ellipse(A(1)/100,B(1)/100,nC,z);
    tmp = nC;
    type = 'quad';
end
for i = 1:nR
    tmpPts = geom_2D_contour_ellipse(A(i),B(i),nC,z);
    elemPts = [elemPts , tmpPts];
end


% elements
if (nargout > 1)
    if options.triQ
        elemNodes = 1 + [zeros(1,nC) ; 1:nC ; 2:nC + 1 ];
        elemNodes(3,end) = 2;
	else
        firstNode = 1;
        secondNode = firstNode + nC;
        elemNodes = geom_gen_nodesForElem('quad',nC,firstNode,secondNode,1,options);
    end
    
    if (options.triQ) && (options.clockQ)
        elemNodes = flip(elemNodes);
    end
    
    for i = 1:(nR-1)
        firstNode = 1 + tmp + (i-1) * nC;
        secondNode = firstNode + nC;
        offset = 1;
        
        elemNodes = [elemNodes , ...
                     geom_gen_nodesForElem(type,nC,firstNode,secondNode,offset,options)];
    end
end


% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end


% normal unit vector to elements
if (nargout > 3)
    elemVec = geom_op_calcElemVec(elemPts,elemNodes,false);
end


% element area (here: triangles)
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemVec,elemNodes,false);
end


% output handling
switch nargout
    case {0,1,2,3}
    case {4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output argument');
end

end
