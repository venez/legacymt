% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_2D_contour_rectangle(major,minor,divMajor,divMinor,zCoord)
% 
% Returns a list of edges of the panels that make up the rectangle in
% 'elemPts' and the list of elements that make up the boundary are defined
% by the indices of the two vertices in 'elemNodes'.
% 
% NOTE: the vertices of each element in 'elemNodes' are defined in a
%       counter-clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_2D_contour_rectangle(major,minor,divMajor,divMinor,zCoord)

% Default
a = 1.0;
b = 0.5;
na = 10;
nb = 5;
zCoordsQ = false;

switch nargin
    case 0
    case 1
        a = major;
    case 2
        a = major;
        b = minor;
    case 3
        a = major;
        b = minor;
        na = divMajor;
    case 4
        a = major;
        b = minor;
        na = divMajor;
        nb = divMinor;
    case 5
        a = major;
        b = minor;
        na = divMajor;
        nb = divMinor;
        zCoordsQ = true;
    otherwise
        error('Unknown number of input argument');
end

% coordinates
n = 2*(na+nb);

valAB = linspace(a/2,a/2,nb);
% valAB = valAB(1:nb);
valBA = linspace(b/2,b/2,na);
% valBA = valBA(1:na);

valAA = linspace(a/2,-a/2,na);
% valAA = valAA(1:na);
valBB = linspace(b/2,-b/2,nb);
% valBB = valBB(1:nb);

if (zCoordsQ == false)
    elemPts = [-valAB -valAA  valAB valAA
                valBB -valBA -valBB valBA];
	elemPts = [elemPts , [a/2;b/2]];
else
    elemPts = [-valAB -valAA  valAB valAA
                valBB -valBA -valBB valBA
               repmat(zCoord,1,n)];
	elemPts = [elemPts , [a/2;b/2;zCoord]];
end

% lines
if (nargout > 1)
    elemNodes = [1:n; 2:(n+1)];
    elemNodes(2,n) = 1;
end

% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end

% normal unit vector to elements
if (nargout > 3)
	tmp = elemPts;
    if (zCoordsQ == false)
        tmp = [tmp ; zeros(1,n+1)];
    end
    elemVec = v_norm(v_cross(tmp(:,elemNodes(2,:)) - ...
                             tmp(:,elemNodes(1,:)),[0 ; 0 ; 1]));
end
    
% line lengths
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemPts,elemNodes);
end

% output handling
switch nargout
    case {0,1}
        % default
    case {2,3,4,5}
        elemPts = elemPts(:,1:n);
	case {3,4,5}
        elemVec = v_norm(elemVec);
    otherwise
        error('Unknown number of output arguments');
end

end