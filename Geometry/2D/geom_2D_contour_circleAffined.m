% [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
%       geom_2D_contour_circleAffined(radius,stretch,deform,divisions,zCoord)
%         
% Returns a list of edges of the panels that make up the affine structure
% in 'elemPts' and the list of elements that make up the boundary are 
% defined by the indices of the two vertices in 'elemNodes'.
% 
% Note: the vertices of each element in 'elemNodes' are defined in a
%       counter-clockwise direction.
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = ...
            geom_2D_contour_circleAffined(radius,stretch,deform,divisions,zCoord)

% input handling
r = 1.0;
a = 0;
b = 0;
n = 36;
zCoordsQ = false;

switch nargin
    case 0
    case 1
        r = radius;
    case 2
        r = radius;
        a = strerch;
    case 3
        r = radius;
        a = stretch;
        b = deform;
    case 4
        r = radius;
        a = stretch;
        b = deform;
        n = divisions;
    case 5
        r = radius;
        a = stretch;
        b = deform;
        n = divisions;
        zCoordsQ = true;
    otherwise
        error('Unknown number of input arguments');
end
    
% coordinates
angles = 0:2*pi/n:2*pi;
elemPts = [r*cos(angles) + a*r*cos(angles) + b*r*cos(3*angles); ...
           r*sin(angles) - a*r*sin(angles) - b*r*sin(3*angles)];

if (zCoordsQ == true)
     elemPts = [elemPts; ...
                repmat(zCoord,1,n+1)]; 
end

% lines
if (nargout > 1)
    elemNodes = [1:n; 2:(n+1)];
    elemNodes(2,n) = 1;
end

% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end

% normal unit vector to elements
if (nargout > 3)
	tmp = elemPts;
    if (zCoordsQ == false)
        tmp = [tmp ; zeros(1,n+1)];
    end
    elemVec = v_norm(v_cross(tmp(:,elemNodes(2,:)) - ...
                             tmp(:,elemNodes(1,:)),[0 ; 0 ; 1]));
end
    
% line lengths
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemPts,elemNodes,true);
end

% output handling
switch nargout
    case {0,1}
        elemPts = elemPts(:,1:n);
    case {2,3,4,5}
        % default
    otherwise
        error('Unknown number of output arguments');
end

end