% export_mtx(src,filename,opts)
%
%    Writes the sparse or dense matrix 'src' to a Matrix Market (MM) 
%    formatted file - optional specified by 'filename'.
% 
%   Fields permitted for 'opts':
%       comm    - default = ''
%                 arbitrary comment (see below)
%       rep     - default = 'coordinate'
%                 alternative = 'array'
%       type    - default = ''
%                 if not specified the function will distinguish between
%                 types 'real', 'complex', 'integer' and 'pattern'
%       prec    - default = 16
%                 number of digits to display for real or complex values
% 
%   Example:
%       src = rand(4,4);
%       opts.prec = 5;
%       export_mtx(src,[],opts);
%
% 	Notes on opts.comm:
%   - matrix of comments to prepend to the MM file
%   - default setting results in a single line date stamp
%   - to build a comments matrix please use str2mat
%   
%     Example:
%     opts.comm = str2mat(' options.comm 1' ,...
%                         ' options.comm 2',...
%                         ' and so on.',...
%                         ' to attach a date:',...
%                         [' ',date]);
%
function export_mtx(filename,src,opts)

% Input handling
default.comm = '';
default.file = '';
default.msgQ = true;
default.prec = 16;
default.rep = 'coordinate';
default.type = '';
switch nargin
    case {1,2}
        options = default;
    case 3
        options = completeInput(opts,default);
        options.file = '';
    otherwise
        error('Unknown number of input arguments');
end


% Preparations
if isempty(filename)
    [FileName,PathName] = uiputfile('*.mtx',['Select file for Save as ' ...
                                            'Matrix Market']);
    filename = [PathName FileName];
end


% Identify matrix representation
if isequal(options.rep,'coordinate')
    coordFormatQ = true;
elseif isequal(options.rep,'array')
    coordFormatQ = false;
else
    error('Unknown representation specified in header.');
end


% Identify and check for matrix type
switch options.type
    case ''
        if (isreal(src))
            options.type = 'real';
        else
            if max(any(imag(src)))
                options.type = 'complex';
            else
                options.type = 'real';
            end
        end
    case {'real','complex','pattern'}
    otherwise
        error('Unknown specified arithmetic field')
end


% Identify symmetry
[nRows,nCols] = size(src);
if (nRows ~= nCols)
	options.symm = 'general';
    data = src;
else
    % storing entries only on and below the main diagonal
    data = tril(src);
    if (issymmetric(src,'skew'))
        options.symm = 'skew-symmetric';
    elseif (issymmetric(src))
        options.symm = 'symmetric';
    elseif (ishermitian(src,'skew'))
        options.symm = 'skew-hermitian';
    elseif (ishermitian(src))
        options.symm = 'hermitian';
    else
        options.symm = 'general';
        data = src;
    end
    
end


% Open file
mmfile = fopen(filename,'w');
if (mmfile == -1)
    error('Cannot open file for output');
end


% Print header
fprintf(mmfile,'%%%%MatrixMarket matrix %s %s %s\n', ...
        options.rep,options.type,options.symm);


% Print comment
[mC,~] = size(options.comm);
if (~mC)
    fprintf(mmfile,'%% Generated %s\n', date);
else
    for ii = 1:mC
    	fprintf(mmfile,'%%%s\n',options.comm(ii,:));
    end
end


% Print matrix
if(coordFormatQ) % part for sparse matrices
    nNonZeros = nnz(data);
    fprintf(mmfile,'%d %d %d\n',nRows,nCols,nNonZeros);
    
    [I,J,V] = find(data);
    switch options.type
    	case 'real'
            realformat = sprintf('%%d %%d %% .%dg\n',options.prec);
            for ii = 1 : nNonZeros
                fprintf(mmfile,realformat,I(ii),J(ii),V(ii));
            end
        case 'complex'
            cplxformat = sprintf('%%d %%d %% .%dg %% .%dg\n', ...
                                 options.prec,options.prec);
            for ii = 1 : nNonZeros
                fprintf(mmfile,cplxformat,I(ii),J(ii), ...
                        real(V(ii)),imag(V(ii)));
            end
        case 'pattern'
            for ii = 1 : nNonZeros
                fprintf(mmfile,'%d %d\n',I(ii),J(ii));
            end
    end

else % part for dense matrices
	fprintf(mmfile,'%d %d\n',nRows,nCols);
        
    if isequal(options.symm,'general')
        rowloop = '1';
    else
        rowloop = 'jj';
    end
    
    switch options.type
        case 'real'
            realformat = sprintf('%% .%dg\n', options.prec);
            for jj = 1 : nCols
                for ii = eval(rowloop) : nRows
                    fprintf(mmfile,realformat,data(ii,jj));
                end
            end
        case 'complex'
            cplxformat = sprintf('%% .%dg %% .%dg\n',options.prec, ...
                                                     options.prec);
            for jj = 1 : nCols
                for ii = eval(rowloop) : nRows
                    fprintf(mmfile,cplxformat, ...
                            real(data(ii,jj)),imag(data(ii,jj)));
                end
            end
        case 'pattern'
            error('Pattern type inconsistant with dense matrix')
    end
end


% Clean up
fclose(mmfile);

if options.msgQ
    disp('-- MTX export successfully completed --')
end


end