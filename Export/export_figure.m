% export_figure(gcf,filename,opts)
%
%    Writes the current figure src (called by gcf) to a formatted file - 
%   optional specified by 'filename'.
% 
%   Fields permitted for 'opts':
%       ext    -  default = 'png'
%                 arbitrary comment (see below)
%       res     - default resolution = [600 400]
%       unit    - default = 'pixels'
%                 see 'Figure Properties' in help documentation
% 
function [] = export_figure(gcf,filename,opts)

% Input handling
default.ext = '.png';
default.file = '';
default.res = [600 400];
default.unit = 'pixels';
switch nargin
    case {1,2}
        options = default;
    case 3
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end


% Preparations
if (isempty(filename))
    [FileName,PathName] = uiputfile( ...
            {'*.png;*.jpg;*.bmp', 'Bilddateien (*.bmp,*.jpg,*.png)';...
             '*.pdf','PDF-Dateien (*.pdf)';...
             '*.eps','EPS-Dateien (*.eps)';...
             '*.m;*.fig','MATLAB Dateien(*.m,*.fig)'}, ...
             'Save as');
    filename = [PathName FileName];
end
tmp = strsplit(filename,'.');
options.ext = char(tmp(end));


% Execution
if isequal(options.ext,'pdf')
    set(gcf,'PaperPosition', [0 0 options.res(1) options.res(2)], ...
            'PaperSize', [options.res(1) options.res(2)], ...
            'PaperUnits',options.unit);
    print(filename,'-dpdf','-fillpage');
else
    set(gcf,'units',options.unit,'innerposition', ...
                    [0 0 options.res(1) options.res(2)]);
    saveas(gcf,filename);
end
disp(['-- ' options.ext ' file saved --']);

end