% export_stl(filename,elemPts,elemNodes,elemVec,opts)
% 
%	Function exports a model of triangles defined by 'elemPts', 'elemNodes'
%	and 'elemVec' into a specified file 'filename' with user-defined 
%	options 'opts'. Latter contains optional the possibility to specify 
%	properties. 
%   For an empty 'filename' the function opens a dialog.
% 
%   Fields permitted for 'opts':
%       ext     - default = '.stl'
%       flip    - default = false
%       mode    - default = 'ascii'
%       name    - default = 'SOLID'
% 
%   Examples:
%       target = 'C:\Documents\';
%       opts.mode = 'binary';
%       export_stl(target,elemPts,elemNodes,elemVec,opts);
% 
function export_stl(filename,elemPts,elemNodes,elemVec,opts)


% Input handling
default.file = '';
default.ext = '.stl';
default.flip = false;
default.mode = 'ascii';
default.name = 'SOLID';
default.warnQ = true;

switch nargin
    case {3,4}
        options = default;
    case 5
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end


% Preparations
if (isempty(filename))
    [FileName,PathName] = uiputfile(['*' options.ext], ...
                                     'Select file for Save as STL');
    filename = [PathName FileName];
end

if (isequal(options.mode,'ascii'))
    permission = 'w';
elseif (isequal(options.mode,'binary'))
    permission = 'wb+';
else
    error('Unknown mode for STL export')
end

[pathstr,name,ext] = fileparts(filename);
if (isempty(pathstr))
    pathstr = pwd;
end
if ~isequal(ext,options.ext)
    ext = options.ext;
end
filename = [pathstr '\' name ext];

if (options.flip)
    fac = -1;
else
    fac = 1; 
end


% Execution
data = cat(2,reshape(single(elemVec*fac), 3, 1, []),...
             reshape(single(elemPts(:,elemNodes)), 3, 3, []));
fid = fopen(filename, permission);
if (fid == -1)
    error('Unable to write to %s', filename);
end
if (isequal(options.mode,'ascii'))
    fprintf(fid,'solid %s\r\n',options.name);	% Header
    fprintf(fid,[...                            % Data
        'facet normal %.7E %.7E %.7E\r\n' ...
        'outer loop\r\n' ...
        'vertex %.7E %.7E %.7E\r\n' ...
        'vertex %.7E %.7E %.7E\r\n' ...
        'vertex %.7E %.7E %.7E\r\n' ...
        'endloop\r\n' ...
        'endfacet\r\n'], data);
    fprintf(fid,'endsolid %s\r\n',options.name);
else
    [~,nElements] = size(elemNodes);
    
    fprintf(fid, '%-80s', options.name);        % Header
    fwrite(fid,nElements,'uint32');             % Number of elements
    % Write DATA
    % Add one uint16(0) to the end of each facet using a typecasting trick
    data = reshape(typecast(data(:),'uint16'),12*2,[]);
    fwrite(fid, data,'uint16');
    
% % BINARY
%     % Write HEADER
%     fprintf(fid, '%-80s', options.name);             % Title
%     fwrite(fid,nElements,'uint32');           % Number of elements
%     % Write DATA
%     % Add one uint16(0) to the end of each facet using a typecasting trick
%     data = reshape(typecast(data(:), 'uint16'), 12*2, []);
%     % Set the last bit to 0 (default) or supplied RGB
%     data(end+1,:) = options.facecolor;
%     fwrite(fid,data,'uint16');
    
end
fclose(fid);
disp('-- STL export successfully completed --')


end