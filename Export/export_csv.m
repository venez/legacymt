% export_csv(src,filename,opts)
% 
%	Writes the source data 'src' into a CSV formatted file, which is 
%   optional specified by 'filename' (otherwise function opens a dialog).
% 
%   Fields permitted for 'opts':
%       ext     - default = '.csv'
%       head    - default = []
%       mode    - default = 'ascii' (alternative: 'binary')
%       sep     - default = ','
%       msg     - default = true
% 
%   Examples:
%       target = 'C:\Documents\file.csv';
%       opts.mode = 'binary';
%       opts.head = {x,y,z};
%       export_csv(src,target,opts);
% 
function export_csv(filename,src,opts)


% Input handling
default.ext = '.csv';
default.head = [];
default.mode = 'ascii';
default.prec = 4;
default.sep = ',';
default.msgQ = true;
switch nargin
    case {1,2}
        options = default;
        [options.cols,options.rows] = size(src);
    case 3
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end

% Preparations
if (isempty(filename))
    [FileName,PathName] = uiputfile(['*' options.ext], ...
                                     'Select file for Save as CSV');
    filename = [PathName FileName];
end

if ~isempty(options.head)
    if ~iscell(options.head)
        options.head = {options.head};
    end
    nHead = length(options.head);
    head = cell(1,nHead);
    for ii = 1:nHead
        if ii < nHead
            head(ii) = strcat(options.head(ii),options.sep);
        else
            head(ii) = options.head(ii);
        end
    end
%     options.head = cell2mat(head); %head in text with separators
end

if (isequal(options.mode,'ascii'))
    permission = 'w';
elseif (isequal(options.mode,'binary'))
    permission = 'wb+';
else
    error('Unknown mode for CSV export')
end

[pathstr,name,ext] = fileparts(filename);
if (isempty(pathstr))
    pathstr = pwd;
end
if ~isequal(ext,options.ext)
    ext = options.ext;
end
filename = [pathstr '\' name ext];


% Execution
if ~isempty(options.head) && ~iscell(src)
    fid = fopen(filename, permission);
    if (fid == -1)
        error('Unable to write to %s', filename);
    end

    options.head = cell2mat(head); %head in text with separators
    fprintf(fid,'%s\n',options.head);
    fclose(fid);
    dlmwrite(filename,src,'-append',...
             'delimiter',options.sep,'precision',options.prec);
elseif ~isempty(options.head) && iscell(src)
    tmp = cell2table(src(2:end,:),'VariableNames',options.head);
    writetable(tmp,filename,'Delimiter',options.sep);
elseif isempty(options.head) && ~iscell(src)
    dlmwrite(filename,src,...
             'delimiter',options.sep,'precision',options.prec);
else
    tmp = cell2table(src(2:end,:),'VariableNames',src(1,:));
    writetable(tmp,filename,'Delimiter',options.sep);
end    

if options.msgQ
    disp('-- CSV export successfully completed --')
end


end