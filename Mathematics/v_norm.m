% [s] = v_norm(a)
% 
%   Functions returns as solution 's' the normalized vectors of the vector 
%   group 'a'.
% 
%   NOTE: If the optional 2nd input argument 'rowMajor' is specified all 
%   vectors will be interpreted as line-based instead of column-based.
% 
function [s] = v_norm(a,rowMajor)

s = a;
switch nargin
    case 1
        [~,n] = size(a);
        for i = 1:n
            s(:,i) = a(:,i)/norm(a(:,i));
        end
    case 2
        [m,~] = size(a);
        for i = 1:m
            s(i,:) = a(i,:)/norm(a(i,:));
        end
    otherwise
        error('Unknown definition for input arguments');
end


end