% [s] = v_unit(n,dir,dim)
% 
%   Functions returns as solution 's' a group of 'n' normalized unit 
%   vectors in the specified direction 'dir' and dimension 'dim'.
% 
%   Arguments permitted for 'dir':  {'X','x',1}, {'Y','y',2}, {'Z','z',3}
%   Arguments permitted for 'dim':  2, 3
% 
%   NOTE: If the optional 4th input argument 'rowMajor' is specified the
%   generated vectors will be interpreted as line-based instead of 
%   column-based.
% 
function [s] = v_unit(n,dir,dim,rowMajor)

var1 = linspace(1,1,n);
var0 = linspace(0,0,n);
msg = 'Unknown definition for dir argument';

if (dim == 2)
    switch dir
        case {'X','x',1}
            s = [var1 ; var0];
        case {'Y','y',2}
            s = [var0 ; var1];
        otherwise
            error(msg);
    end
elseif (dim == 3)
    switch dir
        case {'X','x',1}
            s = [var1 ; var0 ; var0];
        case {'Y','y',2}
            s = [var0 ; var1 ; var0];
        case {'Z','z',3}
            s = [var0 ; var0 ; var1];
        otherwise
            error(msg);
    end
else
    error('Unknown definition for dim argument');
end
   
if (nargin == 4)
    s = s';
end


end