% [s] = v_cos(a,b,magA,magB)
% 
%   Functions returns as solution 's' the cosine of the angle between
%   vector groups 'a' and 'b' based on the law of cosines. If one of the 
%   vector groups is not normalized their magnitudes 'magA' or 'magB' 
%   or both must be specified as additional input arguments.
% 
function [s] = v_cos(a,b,magA,magB)

s = v_dot(a,b);
switch nargin
    case 2
        % do nothing, because a and b are already unit vectors
    case 3
        s = s./magA;
    case 4
        s = s./magA./magB;
    otherwise
        error('Unknown number of input arguments');
end

end