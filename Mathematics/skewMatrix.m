% [s] = skewMatrix(x)
% [s] = skewMatrix(x,y,z)
% 
function [s] = skewMatrix(x,y,z)
switch nargin
    case 1
        s = [0 -x(3) x(2) ; x(3) 0 -x(1) ; -x(2) x(1) 0 ];
    case 3
        s = [0 -z y ; z 0 -x ; -y x 0 ];
    otherwise
        error('Unknown number of input arguments');
end
end