% [s] = v_cross(a,b)
% 
%   Functions returns as solution 's' the vectors that are perpendicular to 
%   the vector groups 'a' and 'b'. 
% 
%   NOTE: All vectors exist in a three dimensional space.
% 
function [s] = v_cross(a,b)

[m,n] = size(a);

if (isnumeric(a) && isnumeric(b))
    s = zeros(m,n);
end
    
if (m == 3)
    s(1,:) = (a(2,:).*b(3,:) - a(3,:).*b(2,:));
    s(2,:) = (a(3,:).*b(1,:) - a(1,:).*b(3,:));
    s(3,:) = (a(1,:).*b(2,:) - a(2,:).*b(1,:));
elseif (n == 3)
    s(:,1) = (a(:,2).*b(:,3) - a(:,3).*b(:,2));
    s(:,2) = (a(:,3).*b(:,1) - a(:,1).*b(:,3));
    s(:,3) = (a(:,1).*b(:,2) - a(:,2).*b(:,1));
else
    error('Unknown definition for input arguments');
end

end