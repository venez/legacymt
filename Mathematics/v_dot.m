% [s] = v_dot(a,b,rowMajor)
% 
%   Functions returns as solution 's' the scalar products of the vector 
%   groups 'a' and 'b'. 
% 
%   NOTE: If the optional 3rd input argument 'rowMajor' is specified all 
%   vectors will be interpreted as line-based instead of column-based.
% 
function [s] = v_dot(a,b,rowMajor)

[m,n] = size(a);
switch nargin
    case 2
        if (isnumeric(a) && isnumeric(b))
            s = zeros(1,n);
        end

        if (m == 2)
            s(1,:) = a(1,:).*b(1,:) + a(2,:).*b(2,:);
        elseif (m == 3)
            s(1,:) = a(1,:).*b(1,:) + a(2,:).*b(2,:) + a(3,:).*b(3,:);
        else
            for i = 1:m
                s(1,:) = s(1,:) + a(i,:).*b(i,:);
            end
        end
    case 3
        if (isnumeric(a) && isnumeric(b))
            s = zeros(m,1);
        end

        if (n == 2)
            s(:,1) = a(:,1).*b(:,1) + a(:,2).*b(:,2);
        elseif (n == 3)
            s(:,1) = a(:,1).*b(:,1) + a(:,2).*b(:,2) + a(:,3).*b(:,3);
        else
            for i = 1:n
                s(:,1) = s(:,1) + a(:,i).*b(:,i);
            end
        end
    otherwise
        error('Unknown definition for input arguments');
end


end