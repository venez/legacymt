% [s] = v_mag(a)
% 
%   Functions returns as solution 's' the magnitudes of the vector group
%   'a'.
% 
function [s] = v_mag(a)

s = sqrt(v_dot(a,a));

end