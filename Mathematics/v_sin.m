% [s] = v_sin(a,b,magA,magB)
% 
%   Functions returns as solution 's' the sine of the angle between
%   vector groups 'a' and 'b' based on the law of sines. If one of the 
%   vector groups is not normalized their magnitudes 'magA' or 'magB' 
%   or both must be specified as additional input arguments.
% 
function [s] = v_sin(a,b,magA,magB)

s = v_mag(v_cross(a,b));
switch nargin
    case 2
        % do nothing, because a and b are already unit vectors
    case 3
        s = s./magA;
    case 4
        s = s./magA./magB;
    otherwise
        disp('Unknown number of input arguments');
end

s = abs(s);
end