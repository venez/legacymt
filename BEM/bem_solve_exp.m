% [s] = bem_solve_exp(EQN,OBS,SRC,BC,opts)
% 
%   Detailed explanation goes here (TODO)
% 
%   Fields permitted for 'opts':
%       cond    - default = 'neumann'
%       prob    - default = 'exterior';
%       save    - default = '';
%       vers    - default = '-v7.3';
% 
%   Arguments permitted for 'cond':
%       'dirichlet','D','DBC','DBc','DbC','dBC','Dbc','dBc','dbC','dbc'
%       'neumann','N','NBC','NBc','NbC','nBC','Nbc','nBc','nbC','nbc'
% 
%   Arguments permitted for 'prob':
%       'exterior','EXTERIOR','EX','Ex','eX','ex'
%       'interior','INTERIOR','IN','In','iN','in'
% 
function [s] = bem_solve_exp(EQN,OBS,SRC,BC,opts)

warning('Algorithm is still highly experimental');


% Input handling
default.cond = 'neumann';
default.prob = 'exterior';
default.save = '';
default.vers = '-v7.3';

switch nargin
    case 3
        options = default;
    case 4
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end


% Problem defintions
switch EQN
    case {'Helmholtz','helmholtz','h'}
        [L,M] = bem_v_HelmholtzMatrices(SRC.elemPts,SRC.elemNodes, ...
                                        SRC.elemCen,SRC.elemVec, ...
                                        SRC.elemSize,options);
    case {'Laplace','laplace','l'}
        [L,M] = bem_v_LaplaceMatrices(SRC.elemPts,SRC.elemNodes, ...
                                      SRC.elemCen,SRC.elemVec, ...
                                      SRC.elemSize,options);
    otherwise
        error('Unknown equation to solve')
end

switch options.cond
    case {'dirichlet','D','DBC','DBc','DbC','dBC','Dbc','dBc','dbC','dbc'}
        dbc = BC;
        nbc = bem_solve_surface(L,M,BC,opts);
    case {'neumann','N','NBC','NBc','NbC','nBC','Nbc','nBc','nbC','nbc'}
        dbc = bem_solve_surface(L,M,BC,opts);
        nbc = BC;
%     case {'robin','R','RBC','RBc','RbC','rBC','Rbc','rBc','rbC','rbc'}
%         dbc = ...
%         nbc = ...;
    otherwise
        error('Unknown definition for boundary conditions');
end
s = bem_solve_domain(L,M,dbc,nbc,opts);




% Saving
if (isempty(options.save) == false)
    saveOptions.vers = options.vers;
    save_MatLabMatrix(s,[options.save datestr(now,'yyyy-mm-dd') ...
                        ' BEM_matrix_Res_' num2str(m)],saveOptions);
end


end