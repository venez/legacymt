% [L,M] = bem_v_HelmholtzMatrices(elemPts,elemNodes,elemCen,elemVec,elemSize,waveN,opts)
% 
%   Detailed explanation goes here (TODO)
% 
%   Fields permitted for 'opts':
%       dimG    - default = 3
%       nInt    - default = 5
%       note    - default = 100
%       prec    - default = 'double'
%       save    - default = ''
%       type    - default = 'tri'
%       vers    - default = '-v7.3'
% 
%   Arguments permitted for 'prec':
%       'double','single'
% 
%   Arguments permitted for 'type':
%       'Line','LinE','LiNE','LINE','LINe','LIne','LiNe','LInE','lINE','line'
%       'Triangle','triangle','TRI','Tri','tRi','trI','tri'
% 
function [L,M] = bem_v_HelmholtzMatrices(elemPts,elemNodes,elemCen,elemVec,elemSize,waveN,opts)


% Input handling
default.dimG = 3;
default.nInt = 5;
default.note = 100;
default.prec = 'double';
default.save = '';
default.type = 'tri';
default.vers = '-v7.3';

if (nargin < 7)
    options = default;
elseif (nargin == 7)
    options = completeInput(opts,default);
else
    error('Unknown number of input arguments');
end


% Preparations
switch options.type
    case {'Line','LinE','LiNE','LINE','LINe','LIne','LiNe','LInE','lINE','line'}
        tmp = [elemPts(:,elemNodes(1,:)); % coordinates of left point
               elemPts(:,elemNodes(2,:))];% coordinates of right point
        v_c = repmat(elemCen,2,1);
        
    case {'Triangle','triangle','TRI','Tri','tRi','trI','tri'}
        tmp = [elemPts(:,elemNodes(1,:)); % coordinates of left corner 
               elemPts(:,elemNodes(2,:)); % coordinates of top corner
               elemPts(:,elemNodes(3,:))];% coordinates of right corner
        v_c = repmat(elemCen,3,1);
    otherwise
        error('Unknown type for integration method');
end
[~,nElem] = size(elemCen);


% Control precision
if (isequal(options.prec,'double') && isequal(options.prec,'single'))
    error('Unknown chosen data type');
end
L = zeros(nElem,options.prec);
M = zeros(nElem,options.prec);
% if (nargout > 2)
% 	N = zeros(nElem,options.prec);
% end


% Execution
disp('--- Start calculation of Helmholtz integrator matrices ---');
for i = 1:nElem
    L(i,:) = v_int_operator0_Helmholtz(options.type,3,options.nInt, ...
                                       options.dimG,v_c(:,i) - tmp, ...
                                       elemSize,waveN);
    M(i,:) = v_int_operator1_Helmholtz(options.type,3,options.nInt, ...
                                       options.dimG,v_c(:,i) - tmp, ...
                                       elemVec(:,i),elemSize,waveN);
    L(i,i) = 0.0;
    M(i,i) = 0.0;
    
%     if (nargout > 2)
%         nO = repmat(elemCen(:,i),1,nElem) - elemCen(:,1:nElem);
%         nO = v_norm(nO);
%         N(i,:) = v_int_operator2_Helmholtz(options.type,3,options.nInt, ...
%                                          options.dimG,v_c(:,i) - tmp, ...
%                                          nO,elemVec(:,i),elemSize,waveN);
%     	N(i,i) = 0.0;
%     end
    
    if (mod(i,options.note) == 0)
        disp(['... element ' num2str(i) ' of ' num2str(nElem) ...
              ' calculated ...']);
    end
end
disp('--- Calculation finished ---');


% Output handling
% if (nargout > 2)
%     Mt = transpose(M);
% end


% Saving
if (isempty(options.save) == false)
    nElemStr = num2str(nElem);
    saveOptions.vers = options.vers;
    tmpFile = [options.save datestr(now,'yyyy-mm-dd') ' BEM_matrix_'];
    
    save_MatLabMatrix(L,[tmpFile 'L_' nElemStr],saveOptions);
    save_MatLabMatrix(M,[tmpFile 'M_' nElemStr],saveOptions);
%     if (nargout > 2)
%         save_MatLabMatrix(N,[tmpFile 'N_' nElemStr],saveOptions);
%     end
end

end