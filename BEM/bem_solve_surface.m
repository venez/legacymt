% [s] = bem_solve_surface(L,M,BC,opts)
% 
%   Functions calculates for given integral operators 'L' and 'M' with
%   known information about boundary conditions 'BC' the solution 's' for a
%   specified interior or exterior surface problem.
% 
%   Fields permitted for 'opts':
%       cond    - default = 'neumann'
%       prob    - default = 'exterior';
%       save    - default = '';
%       vers    - default = '-v7.3';
% 
%   Arguments permitted for 'cond':
%       'dirichlet','D','DBC','DBc','DbC','dBC','Dbc','dBc','dbC','dbc'
%       'neumann','N','NBC','NBc','NbC','nBC','Nbc','nBc','nbC','nbc'
% 
%   Arguments permitted for 'prob':
%       'exterior','EXTERIOR','EX','Ex','eX','ex'
%       'interior','INTERIOR','IN','In','iN','in'
% 
function [s] = bem_solve_surface(L,M,BC,opts)

warning('Algorithm still not finally validated');


% Input handling
default.cond = 'neumann';
default.prob = 'exterior';
default.save = '';
default.vers = '-v7.3';

switch nargin
    case 3
        options = default;
    case 4
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end
[m,n] = size(BC);
[mM,nM] = size(M);
[mL,nL] = size(L);


% Error handling
if (mM ~= mL) ||(nM ~= nL)
    error('Dimensions of integral operators differ from each other');
end
if (m ~= mM)
    error('Boundary condtions and integral operator do not match');
end
if (class(M) ~= class(L))
    error('Classes of integral operators differ from each other');    
end


% Problem defintion
switch options.prob
    case {'exterior','EXTERIOR','EX','Ex','eX','ex'}
        fac = - 0.5;
    case {'interior','INTERIOR','IN','In','iN','in'}
        fac = + 0.5;
    otherwise
        error('Unknown problem definition');
end


% Execution by solving linear system
s = zeros(m,n,class(BC));
M = M + fac*eye(m,class(M));
disp('--- Start solving BEM surface problem ---');
switch options.cond
    case {'dirichlet','D','DBC','DBc','DbC','dBC','Dbc','dBc','dbC','dbc'}
        for i = 1:n
            s(:,i) = linsolve(L, M * BC(:,i));
        end
    case {'neumann','N','NBC','NBc','NbC','nBC','Nbc','nBc','nbC','nbc'}
        for i = 1:n
            s(:,i) = linsolve(M, L * BC(:,i));
        end   
%     case {'robin','R','RBC','RBc','RbC','rBC','Rbc','rBc','rbC','rbc'}
%         ...
    otherwise
        error('Unknown definition for boundary conditions');
end
disp('--- BEM surface problem finished ---');


% Saving
if (isempty(options.save) == false)
	saveOptions.vers = options.vers;
    save_MatLabMatrix(s,[options.save datestr(now,'yyyy-mm-dd') ...
                        ' BEM_matrix_SurfaceRes_' num2str(m)],saveOptions);
end


end