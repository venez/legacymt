% [mh,pot,BC] = bem_v_addedMass_ext(n,elemPts,elemNodes,elemCen,elemVec,elemSize,opts)
% 
%   Detailed explanation goes here (TODO)
% 
%   first n elements refer to first body while the rest elements describe a
%   second object which is a wall or is moving as well
% 
%   Fields permitted for 'opts':
%       dimG    - default = 3
%       dimI    - default = 3
%       nInt    - default = 5
%       note    - default = 100
%       prop    - default = 'full' (see 'ai_calc_Symmetry' function)
%       save    - default = ''
%       type    - default = 'tri'
%       vers    - default = '-v7.3'
% 
function [mh,pot,BC] = bem_v_addedMass_ext(n,elemPts,elemNodes,elemCen,elemVec,elemSize,opts)


% Input handling
default.dimG = 3;
default.dimI = 3;
default.nInt = 5;
default.note = 100;
default.prop = 'full';
default.save = '';
default.type = 'tri';
default.vers = '-v7.3';
default.bc = 'move';

if (nargin < 6)
    options = default;
elseif (nargin == 6)
    options = completeInput(opts,default);
else
    error('Unknown number of input arguments');
end


% Execution
m = length(elemCen) - n;
[L,M] = bem_v_LaplaceMatrices(elemPts,elemNodes,elemCen,elemVec,elemSize,options);
bc1	  = ai_v_bc_addedMass(elemCen(:,1:n),elemVec(:,1:n),options);
switch options.bc
    case {1,'wall','WALL'}
        bc2 = zeros(m,6);
    case {2,'move','MOVE'}
        bc2 = ai_v_bc_addedMass(elemCen(:,m:end),elemVec(:,m:end),options);
    otherwise
        error('Unknown ');
end
BC  = [bc1 ; bc2];
pot = bem_solve(L,M,BC,options);
mh  = ai_calc_fluidMatrix(pot,BC,elemSize,options);


% Saving
[~,n] = size(elemCen);
if (isempty(options.save) == false)
    nElemStr = num2Str(n);
    saveOptions.vers = options.vers;
    tmpFile = [options.save datestr(now,'yyyy-mm-dd') ' BEM_matrix_'];
    
    save_MatLabMatrix(mh,[tmpFile 'AddedMass_' nElemStr],saveOptions);
end
if ((nargout > 1) && (isempty(options.save) == false))
    save_MatLabMatrix(pot,[tmpFile 'Pot_' nElemStr],saveOptions);
end

end