% [s] = bem_solve(L,M,BC,opts)
% 
%   Detailed explanation goes here
% 
%   Fields permitted for 'opts':
%       prob    - default = 'exterior';
%       save    - default = '';
%       vers    - default = '-v7.3';
% 
%   Arguments permitted for 'prob':
%       'exterior','EXTERIOR','EX','Ex','eX','ex'
%       'interior','INTERIOR','IN','In','iN','in'
% 
function [s] = bem_solve(L,M,BC,opts)


% Input handling
default.prob = 'exterior';
default.save = '';
default.vers = '-v7.3';

switch nargin
    case 3
        options = default;
    case 4
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end


% Problem defintion
switch options.prob
    case {'exterior','EXTERIOR','EX','Ex','eX','ex'}
        fac = - 0.5;
    case {'interior','INTERIOR','IN','In','iN','in'}
        fac = + 0.5;
    otherwise
        error('Unknown problem definition');
end
% 
%   experimental statements ... need for references
%switch options.prob
%    case {'interior','INTERIOR','IN','in'}
%        fac = + 2;
%    case {'inboundary','INBOUNDARY','INB','inb'}
%        fac = + 1.5;
%    case {'exboundary','EXBOUNDARY','EXB','exb'}
%        fac = + 0.5;
%    case {'exterior','EXTERIOR','EX','ex'}
%        fac = + 0.0;
%    otherwise
%        error('Unknown problem definition');
%end
%
%M     = M + (fac - 1)*eye(m,class(M));

% Execution by solving linear system                  
[m,n] = size(BC);
s     = zeros(m,n,class(BC));
M     = M + fac*eye(m,class(M));
disp('--- Start solving BEM problem ---');
for i = 1:n
    s(:,i) = linsolve(M, L * BC(:,i));
end
disp('--- BEM problem finished ---');


% Saving
if (isempty(options.save) == false)
	saveOptions.vers = options.vers;
    save_MatLabMatrix(s,[options.save datestr(now,'yyyy-mm-dd') ...
                        ' BEM_matrix_Res_' num2str(m)],saveOptions);
end


end