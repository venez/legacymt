% [s] = bem_solve_domain(L,M,DBC,NBC,opts)
% 
%   Functions calculation for given integral operators 'L' and 'M' with
%   known information about Dirichlet boundary conditions 'DBC' as well as 
%   Neumann boundary conditions 'NBC' the solution 's' for a specified
%   interior or exterior domain problem.
% 
%   Fields permitted for 'opts':
%       prob    - default = 'exterior';
%       save    - default = '';
%       vers    - default = '-v7.3';
% 
%   Arguments permitted for 'prob':
%       'exterior','EXTERIOR','EX','Ex','eX','ex'
%       'interior','INTERIOR','IN','In','iN','in'
% 
function [s] = bem_solve_domain(L,M,DBC,NBC,opts)

warning('Algorithm still not finally validated');


% Input handling
default.prob = 'exterior';
default.save = '';
default.vers = '-v7.3';

switch nargin
    case 3
        options = default;
    case 4
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end
[mD,nD] = size(DBC);
[mM,nM] = size(M);
[mN,nN] = size(NBC);
[mL,nL] = size(L);


% Error handling
if (mM ~= mL) ||(nM ~= nL)
    error('Dimensions of integral operators differ from each other');
end
if (class(M) ~= class(L))
    error('Classes of integral operators differ from each other');    
end
if (mD ~= mN) ||(nD ~= nN)
    error('Dimensions of boundary conditions differ from each other');
end
if (class(DBC) ~= class(NBC))
    error('Classes of boundary conditions differ from each other');    
end


% Problem defintion
switch options.prob
    case {'exterior','EXTERIOR','EX','Ex','eX','ex'}
        fac = - 1.0;
    case {'interior','INTERIOR','IN','In','iN','in'}
        fac = + 1.0;
    otherwise
        error('Unknown problem definition');
end


% Execution by solving linear system
s = zeros(mD,nD,class(DBC));
disp('--- Start BEM domain problem ---');
for i = 1:nD
    s(:,i) = -fac * M * NBC(:,i) + fac * L * DBC(:,i);
end
disp('--- BEM domain problem finished ---');


% Saving
if (isempty(options.save) == false)
    saveOptions.vers = options.vers;
    save_MatLabMatrix(s,[options.save datestr(now,'yyyy-mm-dd') ...
                        ' BEM_matrix_DomainRes_' num2str(mD)],saveOptions);
end


end