% [s] = bem_solve_surface_ext(L,M,BC,delta,opts)
% 
%   Functions calculates for given integral operators 'L' and 'M' with
%   known information about a weighted combination of boundary conditions
%   'BC' the solution 's' for a specified interior or exterior surface
%   problem. 
%   Therefore consider known input 'delta' with
%       delta(1,i) = 0 ... as known dirichlet boundary condition
%       delta(2,i) = 1 ... as known neumann boundary condition
%     
%   Fields permitted for 'opts':
%       prob    - default = 'exterior', alternative = 'interior'
%       save    - default = ''
%       vers    - default = '-v7.3'
% 
%   Arguments permitted for 'prob':
%       'exterior','EXTERIOR','EX','Ex','eX','ex'
%       'interior','INTERIOR','IN','In','iN','in'
% 
function [s] = bem_solve_surface_ext(L,M,BC,delta,opts)

warning('Algorithm still not finally validated');


% Input handling
default.prob = 'exterior';
default.save = '';
default.vers = '-v7.3';

switch nargin
    case 4
        options = default;
    case 5
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end
[m,n]   = size(BC);
[mD,nD] = size(delta);


% Error handling
if (mD ~= m) || (nD ~= n)
    error('Distribution does not match given boundary conditions');
end
if (any(any(delta > 1)) == true || any(any(round(delta) ~= delta)) == true)
    error(['Mixed boundary conditions are not allowed ' ...
                    'Delta must have values equal to 0 or 1' ]);
end


% Problem defintion
switch options.prob
    case {'exterior','EXTERIOR','EX','Ex','eX','ex'}
        fac = - 0.5;
    case {'interior','INTERIOR','IN','In','iN','in'}
        fac = + 0.5;
    otherwise
        error('Unknown problem definition');
end


% Execution by solving linear system
A = zeros(m);
B = zeros(m);
M = M + fac*eye(m);
s = zeros(m,n);
disp('--- Start solving extended BEM problem ---');
for i = 1:n
    % Transform equation: M phi = L v     to     A x = B
    % and consider known mixed boundary conditions for phi AND v
    %   delta = 0, BC is known velocity v
    %   delta = 1, BC is known potential phi
    A((delta(:,i) == 0),:) = M((delta(:,i) == 0),:);
    B((delta(:,i) == 0),:) = L((delta(:,i) == 0),:);
    A((delta(:,i) == 1),:) = L((delta(:,i) == 1),:);
    B((delta(:,i) == 1),:) = M((delta(:,i) == 1),:);

    s(:,i) = linsolve(A,B*BC(:,i));
end
disp('--- Calculation finished ---');


% Saving
if (isempty(options.save) == false)
	saveOptions.vers = options.vers;
    save_MatLabMatrix(s,[options.save datestr(now,'yyyy-mm-dd') ...
                        ' BEM_matrix_Res_' num2str(n)],saveOptions);
end