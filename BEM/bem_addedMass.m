% [mh,pot,BC] = bem_addedMass(elemPts,elemNodes,elemCen,elemVec,elemSize,opts)
% 
%   Detailed explanation goes here (TODO)
% 
%   Fields permitted for 'opts':
%       dimG    - default = 3
%       dimI    - default = 3
%       nInt    - default = 5
%       note    - default = 100
%       prop    - default = 'full' (see 'ai_calc_Symmetry' function)
%       save    - default = ''
%       type    - default = 'tri'
%       vers    - default = '-v7.3'
% 
function [mh,pot,BC] = bem_addedMass(elemPts,elemNodes,elemCen,elemVec,elemSize,opts)


% Input handling
default.dimG = 3;
default.dimI = 3;
default.nInt = 5;
default.note = 100;
default.prop = 'full';
default.save = '';
default.type = 'tri';
default.vers = '-v7.3';

if (nargin < 6)
    options = default;
elseif (nargin == 6)
    options = completeInput(opts,default);
else
    error('Unknown number of input arguments');
end


% Preparations
[L,M] = bem_LaplaceMatrices(elemPts,elemNodes,elemCen,elemVec, ...
                            elemSize,options);
BC	  = bem_v_bc_addedMass(elemCen,elemVec,options);
pot   = bem_solve(L,M,BC,options);


% Execution
mh = zeros(6,6);
switch options.prop
    case {'explicit'}
        for i = 1:6
            for j = 1:6
                mh(i,j) = -sum(pot(:,i).*BC(:,j).*elemSize');        
            end
        end
    case {'f','full'}
        for i = 1:6
            for j = i:6
                mh(i,j) = -sum(pot(:,i).*BC(:,j).*elemSize');        
            end
        end
    case {'s','symmetric'}
        for i = 1:6
            for j = i:2:6
                mh(i,j) = -sum(pot(:,i).*BC(:,j).*elemSize');
            end
        end
	case {'ds1','double symmetric1'}
        for i = 1:6
            mh(i,i) = -sum(pot(:,i).*BC(:,i).*elemSize');
        end
        tmp = [2 3 5 6];
        for i = 1:2
            j = tmp(5 - i);
            mh(i,j) = -sum(pot(:,i).*BC(:,j).*elemSize');
        end
	case {'ds2','double symmetric2'}
        for i = 1:6
            mh(i,i) = -sum(pot(:,i).*BC(:,i).*elemSize');
        end
        tmp = [1 2 4 5];
        for i = 1:2
            j = tmp(5 - i);
            mh(i,j) = -sum(pot(:,i).*BC(:,j).*elemSize');
        end
    otherwise
        error('Unknown given matrix property');
end
if (contains(options.prop,'explicit') == false)
    mh = mh + triu(mh,+1)';
end


% Saving
[~,n] = size(elemCen);
if (isempty(options.save) == false)
    nElemStr = num2Str(n);
    saveOptions.vers = options.vers;
    tmpFile = [options.save datestr(now,'yyyy-mm-dd') ' BEM_matrix_'];
                    
    save_MatLabMatrix(mh,[tmpFile 'AddedMass_' nElemStr],saveOptions);
end
if ((nargout > 1) && (isempty(options.save) == false))
    save_MatLabMatrix(pot,[tmpFile 'Pot_' nElemStr],saveOptions);
end


end