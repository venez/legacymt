% [L,M] = bem_HelmholtzMatrices(elemPts,elemNodes,elemCen,elemVec,elemSize,waveN,opts)
% 
%   Detailed explanation goes here (TODO)
% 
%   Fields permitted for 'opts':
%       dimG    - default = 3
%       dimI    - default = 3
%       nInt	- default = 5
%       save    - default = ''
%       type    - default = 'tri'
%       vers    - default = '-v7.3'
% 
function [L,M] = bem_HelmholtzMatrices(elemPts,elemNodes,elemCen,elemVec,elemSize,waveN,opts)


% Input handling
default.dimG = 3;
default.dimI = 3;
default.nInt = 5;
default.note = 100;
default.save = '';
default.type = 'tri';
default.vers = '-v7.3';

if (nargin < 7)
    type = default.type;
	nInt = default.nInt;
    dim = default.dimI;
    Green = default.dimG;
elseif (nargin == 7)
    options = completeInput(opts,default);
    type = options.type;
	nInt = options.nInt;
    dim = options.dimI;
    Green = options.dimG;
else
    error('Unknown number of input arguments');
end


% Preparations
switch type
    case {'Line','LinE','LiNE','LINE','LINe','LIne','LiNe','LInE','lINE','line'}
        tmp = [elemPts(:,elemNodes(1,:)); % coordinates of left point
               elemPts(:,elemNodes(2,:))];% coordinates of right point
        c	= repmat(elemCen,2,1);
        
    case {'Triangle','triangle','TRI','Tri','tRi','trI','tri'}
        tmp = [elemPts(:,elemNodes(1,:)); % coordinates of left corner 
               elemPts(:,elemNodes(2,:)); % coordinates of top corner
               elemPts(:,elemNodes(3,:))];% coordinates of right corner
        c	= repmat(elemCen,3,1);
    otherwise
        error('Unknown type for integration method');
end
[m,nElem] = size(elemCen);
L = zeros(nElem);
M = zeros(nElem);
% if (nargout > 2)
%     N = zeros(nElem);
% end


% Execution
disp('--- Start calculation of Helmholtz integrator matrices ---');
for i = 1:nElem
    for j = 1:nElem
        if (i == j)
            continue; % secure handling of singularities 
        end
        L(i,j) = int_operator0_Helmholtz(type,dim,nInt,Green, ...
                                       reshape(c(:,i) - tmp(:,j),m,3), ...
                                       elemSize(i),waveN);
 
        M(i,j) = int_operator1_Helmholtz(type,dim,nInt,Green, ...
                                       reshape(c(:,i) - tmp(:,j),m,3), ...
                                       elemVec(:,i),elemSize(i),waveN);
        
%         if (nargout > 2)  % TODO
%             nO = elemCen(:,i) - elemCen(:,j);
%             nO = nO/norm(nO);
%             N(i,j) = int_operator2_Helmholtz(type,dim,nInt,Green, ...
%                                         reshape(c(:,i) - tmp(:,j),m,3), ...
%                                         nO,elemVec(:,i),elemSize(i));
%         end        
    end
	if (mod(i,options.note) == 0)
    	disp(['... element ' num2str(i) ' of ' num2str(nElem) ...
              ' calculated ...']);
	end
end
disp('--- Calculation finished ---');


% Output handling
% if (nargout > 2)
%     Mt = transpose(M);
% end


% Saving
if (isempty(options.save) == false)
    nElemStr = num2str(nElem);
    saveOptions.vers = options.vers;
    tmpFile = [options.save datestr(now,'yyyy-mm-dd') ' BEM_matrix_'];
    
    save_MatLabMatrix(L,[tmpFile 'L_' nElemStr],saveOptions);
    save_MatLabMatrix(M,[tmpFile 'M_' nElemStr],saveOptions);
%     if (nargout > 3)
%         save_MatLabMatrix(N,[tmpFile 'N_' nElemStr],saveOptions);
%     end
end


end