% [bc] = ai_v_bc_addedMass(elemCen,elemVec,opts)
% 
%   Function defines the boundary conditions 'bc' on a surface described by
%   'elemCen' points and in the direction of 'elemVec' to solve the added 
%   mass problem of an object according to "Kornev: ship theory, p.45, 
%   eq. 3.18, 2009".
% 
%   Fields permitted for 'opts':
%       prec    - default = 'double'
%       save	- default = ''
%       vers    - default = '-v7.3'
% 
function [bc] = ai_v_bc_addedMass(elemCen,elemVec,opts)


% Input handling
default.prec = 'double';
default.save = '';
default.vers = '-v7.3';

if (nargin < 3)
    options = default;
elseif (nargin == 3)
    options = completeInput(opts,default);
else
    error('Unknown number of input arguments');
end


% Preparations
if (isequal(options.prec,'double') && isequal(options.prec,'single'))
    error('Unknown chosen data type');
end
[~,n] = size(elemCen);
bc = zeros(6,n,options.prec);


% Execution
for i = 1:6
    if (i <= 3)
        bc(i,:) = v_cos(elemVec,v_unit(n,i,3));
    elseif (i == 4)                       
        bc(i,:) = elemCen(2,:) .* v_cos(elemVec,v_unit(n,'Z',3)) - ...
                  elemCen(3,:) .* v_cos(elemVec,v_unit(n,'Y',3));
    elseif (i == 5)
        bc(i,:) = elemCen(3,:) .* v_cos(elemVec,v_unit(n,'X',3)) - ...
                  elemCen(1,:) .* v_cos(elemVec,v_unit(n,'Z',3));
    elseif (i == 6)
        bc(i,:) = elemCen(1,:) .* v_cos(elemVec,v_unit(n,'Y',3)) - ...
                  elemCen(2,:) .* v_cos(elemVec,v_unit(n,'X',3));
    end
end
bc = bc';


% Saving
if (isempty(options.save) == false)
	saveOptions.vers = options.vers;
    save_MatLabMatrix(bc,[options.save datestr(now,'yyyy-mm-dd') ...
                         ' BEM_matrix_BC_' num2str(n)],saveOptions);
end

end