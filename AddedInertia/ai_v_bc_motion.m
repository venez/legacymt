% [bc] = ai_v_bc_motion(elemCen,elemVec,vel,opts)
% 
%   Function defines the boundary conditions 'bc' on a surface described by
%   'elemCen' points and in the direction of 'elemVec' corresponding to the
%   impermeability of an object within a flow according to "Kornev: ship 
%   theory, p.44, eq. 3.15, 2009".
% 
%   Fields permitted for 'opts':
%       prec    - default = 'double'
%       save	- default = ''
%       vers    - default = '-v7.3'
% 
function [bc] = ai_v_bc_motion(elemCen,elemVec,vel,opts)


% Input handling
default.prec = 'double';
default.save = '';
default.vers = '-v7.3';

if (nargin < 4)
    options = default;
elseif (nargin == 4)
    options = completeInput(opts,default);
else
    error('Unknown number of input arguments');
end


% Preparations
if (isequal(options.prec,'double') && isequal(options.prec,'single'))
    error('Unknown chosen data type');
end
nV = length(vel);
if ~((nV == 1) || (nV == 3) || (nV == 6))
    error('3rd argument must have 1, 3 or 6 components');
end
[~,n] = size(elemCen);
bc = zeros(3,n,options.prec);


% Execution
[mV,nV] = size(vel);
if (mV == 1)
    v = [vel zeros(1,6-nV,1)];
    v = v';
else
    v = [vel ; zeros(6-nV,1)];
end

bc(:,:) = (v(1:3) + v_cross(repmat(v(4:6),[1 n]),elemCen) ) .* elemVec;
if (nV == 6)
    bc = [bc ; repmat(v(4:6),[1 n])];
end

% Return 
bc = bc(1:nV,:)';


% Saving
if (isempty(options.save) == false)
    saveOptions.vers = options.vers;
	save_MatLabMatrix(bc,[options.save datestr(now,'yyyy-mm-dd') ...
                         ' BEM_matrix_BC_' num2str(n)],saveOptions);
end

end