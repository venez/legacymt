% [mh] = ai_calc_Symmetry(problem,src)
% 
%   By specifing the basic 'problem' for the task the resulting solution 
%   for the fluid mass matrix considers the relations between the matrix
%   entries for an infinite expanded fluid.
% 
%   Entries permitted for 'problem':
%       e, explicit
%       f, full
%       p, point (symmetry planes: XY, YZ as well as ZX)
%       symXY, symYX
%       symXZ, symZX
%       symYZ, symZY
%       symXY-XZ, symXY-ZX, symYX-XZ, symYX-ZX
%       symXZ-YZ, symXZ-ZY, symZX-YZ, symZX-ZY
%       symYZ-XY, symYZ-YX, symZY-XY, symZY-YX
% 
function [mh] = ai_calc_Symmetry(problem,src)


% Execution
postQ = true;
switch problem
    case {'e','explicit','f','full'}
        postQ = false;
        tmp = ones(6,6);
	case {'symXY','symYX'}
        tmp = entriesSymXY;
    case {'symXZ','symZX'}
        tmp = entriesSymXZ;
	case {'symYZ','symZY'}
        tmp = entriesSymYZ;
    case {'symXY-XZ','symXY-ZX','symYX-XZ','symYX-ZX'}
        xy = entriesSymXY;
        xz = entriesSymXZ;
        tmp = overlapSym(xy,xz);
    case {'symXZ-YZ','symXZ-ZY','symZX-YZ','symZX-ZY'}
        xz = entriesSymXZ;
        yz = entriesSymYZ;
        tmp = overlapSym(xz,yz);
    case {'symYZ-XY','symYZ-YX','symZY-XY','symZY-YX'}
        yz = entriesSymYZ;
        xy = entriesSymXY;
        tmp = overlapSym(yz,xy);
    case {'p','point'}
        xz = entriesSymXZ;
        xy = entriesSymXY;
        yz = entriesSymYZ;
        tmpA = overlapSym(xz,xy);
        tmp = overlapSym(tmpA,yz);
    otherwise
        error('Unknown given matrix property');
end
mh = src .* tmp;


% Ensure relation for infinite expanded fluid:   a_jk = a_kj
if (postQ)
    tmp = (tril(mh,-1) + triu(mh,+1)')/2;
    mh = diag(diag(mh)) + tmp + tmp';
end

end


%% SUPPORT FUNCTION
function [s] = entriesSymXY()
s = [1 1 0 0 0 1
     1 1 0 0 0 1
     0 0 1 1 1 0
     0 0 1 1 1 0
     0 0 1 1 1 0
     1 1 0 0 0 1];
end

function [s] = entriesSymXZ()
s = [1 0 1 0 1 0
     0 1 0 1 0 1
     1 0 1 0 1 0
     0 1 0 1 0 1
     1 0 1 0 1 0
     0 1 0 1 0 1];
end

function [s] = entriesSymYZ()
s = [1 0 0 0 1 1
     0 1 1 1 0 0
     0 1 1 1 0 0
     0 1 1 1 0 0
     1 0 0 0 1 1
     1 0 0 0 1 1];
end

function [s] = overlapSym(a,b)
tmp = a + b;
s = reshape(tmp,[numel(tmp) 1]) == 2;
s = double(reshape(s,[6 6]));
end