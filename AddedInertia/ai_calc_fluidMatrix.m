% [mh] = ai_calc_fluidMatrix(pot,BC,elemSize,opts)
% 
%   Calculates the 6x6 added mass matrix for a given potential distribution
%   'pot' and known boundary conditions 'BC' for elemens, where it sizes
%   are stored in 'elemSize'. By specifing the optional 'opts.prob'/problem 
%   for the task the resulting solution already considers the relations 
%   between the matrix entries for an infinite expanded fluid.
% 
%   Fields permitted for 'opts':
%       prob    - default = 'full'  (see 'ai_calc_Symmetry' for details)
%       save    - default = ''
%       vers    - default = '-v7.3'
% 
function [mh] = ai_calc_fluidMatrix(pot,BC,elemSize,opts)


% Input handling
default.prob = 'full';
default.save = '';
default.vers = '-v7.3';

switch nargin
    case 3
        options = default;
    case 4
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end


% Execution
mh = zeros(6,6);
for ii = 1:6
    for jj = 1:6
        mh(ii,jj) = -sum(pot(:,ii).*BC(:,jj) .* elemSize');
    end
end


% Apply symmetry condition
mh = ai_calc_Symmetry(options.prob,mh);


% Saving
if (isempty(options.save) == false)
    [m,~] = size(BC);
	saveOptions.vers = options.vers;
    save_MatLabMatrix(s,[options.save datestr(now,'yyyy-mm-dd') ...
                        ' AddedMass_' num2str(m)],saveOptions);
end


end