% [s] = phys_density_WaterPure(temperature)
% 
%   Functions returns as solution 's' in [kg/m�] The density of the fluid 
%   'water pure' for a given 'temperature' in [�C] (validity: 0< T< 180�C).
% 
%   Source -> "Sharqawy, M.H, et. al: Thermophysical properties of seawater
%   - a review of existing correlations and data, In: Desalination and
%   Water Treatment 16, 2010, pp. 354�380"
% 
function [s] = phys_density_WaterPure(temperature)
    T = temperature;
       
    % coefficents, table 11
    a1 =  9.999 * 10^2;
    a2 =  2.034 * 10^-2;
    a3 = -6.162 * 10^-3;
    a4 =  2.261 * 10^-5;
    a5 = -4.657 * 10^-8;
    
    % result, formula (50)
    s = a1 + a2 * T + a3 * T.^2 + a4 * T.^3 + a5 * T.^4;
end