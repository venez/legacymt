% [s] = phys_viscosityDyn_Air(temperature,inKelvinQ)
% 
%   Functions returns as solution 's' the dynamic viscosity of the fluid
%   'air' for a given 'temperature' - in [�C] as default or in [K] by
%   specifing the 2nd input argument.
% 
%   Source -> "Egler, D., et. al: Engineering Fluid Mechanics, 2013"
% 
function [s] = phys_viscosityDyn_Air(temperature,inKelvinQ)

    if (nargin == 1)
        inKelvinQ = false;
    end
    if inKelvinQ ~= true
        temperature = temperature + 273.15;
    end

    % according to chapter 2, formula 2.10
    sutherlandConst = 110.56;       % [K]
    temperatureRef  = 15 + 273.15;  % [K]
    dynViscosityRef = 1.78*10^(-5); % [Pa s] at 15�C

    s = dynViscosityRef * (temperatureRef + sutherlandConst) / ...
      (temperature + sutherlandConst) * (temperature/temperatureRef)^(3/2);
end