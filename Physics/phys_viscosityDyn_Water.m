% [s] = phys_viscosityDyn_Water(salinity,temperature,inKelvinQ)
% 
%   Functions returns as solution 's' the dynamic viscosity of the fluid
%   'water' for a given 'salinity' in [kg/kg] and 'temperature' - in [�C] 
%   as default or in [K] by specifing the 3rd input argument.
% 
%   Source -> "Sharqawy, M.H, et. al: Thermophysical properties of seawater
%   - a review of existing correlations and data, In: Desalination and
%   Water Treatment 16, 2010, pp. 354�380"
% 
function [s] = phys_viscosityDyn_Water(salinity,temperature,inKelvinQ)

    if (nargin == 1)
        inKelvinQ = false;
    end
    if inKelvinQ ~= true
        temperature = temperature + 273.15;
    end

    % viscosity for pure water
    dynViscosityRef = 4.2844*10^-5 + (0.157 * (temperature + 64.993)^2 -...
                      91.296)^-1;
    
    % viscosity factors
    A = 1.541 + 1.998*10^-2 * t - 9.52*10^-5 * t^2;
    B = 7.974 - 7.561*10^-2 * t + 4.724*10^-4 * t^2;
	
    % formula #22
    s = dynViscosityRef * (1 + A*salinity + B * salinity^2);
end