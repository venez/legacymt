% [s] = phys_density_Air(pressure,humidity,temperature,inKelvinQ)
% 
%   Functions returns as solution 's' the density of the fluid 'air' for a
%   given 'pressure' in [Pa], 'humidity' in a range of [0.0 ; 1.0] and a 
%   'temperature' - in [�C] as default or in [K] by specifing the 4th input
%   argument.
% 
%   Source -> "Picard, A., et. al: Revised formula for the density of moist
%   air, In: Metrologia 45, 2008, pp. 149�155"
% 
%   Default values:
%    pressure = 101 325 Pa  ;   humidity = 0.5  ; temperature = 15 �C
% 
function [s] = phys_density_Air(pressure,humidity,temperature,inKelvinQ)
    % default
    h = 0.7;
    p = 101325;
    temp = 15;
    kelvinQ = false;

    % input
    switch nargin
        case 0
        case 1
            p = pressure;
        case 2
            h = humidity;
            p = pressure;
        case 3
            h = humidity;
            p = pressure;
            temp = temperature;
        case 4
            h = humidity;
            kelvinQ = inKelvinQ;
            p = pressure;
            temp = temperature;
        otherwise
    end
    if (nargin < 4 )
        
    end
    if kelvinQ ~= true
        T = temp + 273.15;
    else
        T = temp;
    end

    % material coefficients
    Ma = 28.96546*10^-3;    % kg/mol ... see section 2
    Mv = 18.01528*10^-3;    % kg/mol ... see section 2
    R = 8.314472;           % J/(mol K) ... see section 4
    
    % saturation pressure coefficients, appendix A.1
    A = 1.2378847*10^-5;    % 1/K�
    B = -1.9121316*10^-2;   % 1/K
    C = 33.93711047;        % -
    D = -6.3431645*10^3;    % K
    
    % increase factor coefficients, appendix A.1
    alpha = 1.00062;        % -
    beta  = 3.14*10^-8;     % 1/K
    gamma = 5.6*10^-7;      % 1/K�
    
    % compressibility factor coefficients, appendix A.2
    a0 =  1.58123*10^-6;    % K Pa^-1
    a1 = -2.9331*10^-8;     % Pa^-1
    a2 =  1.1043*10^-10;    % K^-1 Pa^-1
    b0 =  5.707 *10^-6;     % K Pa^-1
    b1 = -2.051 *10^-8;     % Pa^-1
    c0 =  1.9898*10^-4;     % K Pa^-1
    c1 = -2.376 *10^-6;     % Pa^-1
    d  =  1.83  *10^-11;    % K^2 Pa^-1
    e  = -0.765 *10^-8;     % K^2 Pa^-2
        
    % preparation, formulas (A1.1) and (A1.2) as well as (A1.3)
    ps = 1*exp(A*T^2 + B*T + C + D/T);  % Pa ... saturation pressure
    f = alpha + beta*p + gamma*T^2;     % - ... increase factor
    x = h * f * ps / p;                 % - ... substance amount fraction
    
    % compressibility factor, formula (A1.4)
    Z = 1 - p/T * (a0 + a1*T + a2*T^2 + (b0 + b1*T)*x + ...
        (c0 + c1*T)*x^2) + p^2 / T^2 * (d + e * x^2);
    
    % result
    s = p*Ma / (Z*R*T) * (1 - x*(1-Mv/Ma));
end