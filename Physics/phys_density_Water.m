% [s] = phys_density_Water(temperature,salinity,pressure)
% 
%   Functions returns as solution 's' in [kg/m�] The density of the fluid 
%   'water' for a given 'temperature' in [�C], 'salinity' in [g/kg] and an 
%   optional 'pressure' in [bar].
% 
%   Source -> "Sharqawy, M.H, et. al: Thermophysical properties of seawater
%   - a review of existing correlations and data, In: Desalination and
%   Water Treatment 16, 2010, pp. 354�380"
% 
function [s] = phys_density_Water(temperature,salinity,pressure)
    T = temperature;    
    
    if (nargin < 3)
        % Validity: 0 < T < 180 �C; 0 < S < 0.16 kg/kg
        S = salinity/1000;  % in [kg/kg]
        
        % temperature factors, table 2
        a1 =  9.999E+02;
        a2 =  2.034E-02;
        a3 = -6.162E-03;
        a4 =  2.261E-05;
        a5 = -4.657E-08;

        % salinity factors, table 2
        b1 =  8.020E+02;
        b2 = -2.001E+00;
        b3 =  1.677E-02;
        b4 = -3.060E-05;
        b5 = -1.613E-05;

        % result, formula (8)
        s = (a1 + a2 * T + a3 * T^2 + a4 * T^3 + a5 * T^4) + ...
            (b1 * S + b2 * S * T + b3 * S * T^2 + b4 * S * T^3 + ...
            b5 * S^2 * T^2);
    else
        % Validity: 0 < T < 180 �C; 0 < S < 80 g/kg, 0.1 < p < 100 MPa
        p = pressure / 10;  % in [MPa]
        S = salinity;

        % coefficents, table 2
        a1 =  9.992E+02;
        a2 =  9.539E-02;
        a3 = -7.619E-03;
        a4 =  3.131E-05;
        a5 = -6.174E-08;
        a6 =  4.337E-01;
        a7 =  2.549E-05;
        a8 = -2.899E-07;
        a9 =  9.578E-10;
        
        a10 =  1.763E-03;
        a11 = -1.231E-04;
        a12 =  1.366E-06;
        a13 =  4.045E-09;
        a14 = -1.467E-05;
        a15 =  8.839E-07;
        a16 = -1.102E-09;
        a17 =  4.247E-11;
        a18 = -3.959E-14;
        
        b1 = -7.999E-01;
        b2 =  2.409E-03;
        b3 = -2.581E-05;
        b4 =  6.856E-08;
        b5 =  6.298E-04;
        b6 = -9.363E-07;
        
        % result, formula (7)  
        s = (a1 + a2*T + a3*T^2 + a4*T^3 + a5*T^4 + a6*p + a7*p*T^2 + ...
            a8*p*T^3 + a9*p*T^4 + a10*p^2 + a11*p^2*T + a12*p^2*T^2 + ...
            a13*p^2*T^3 + a14*p^3 + a15*p^3*T + a16*p^3*T^2 + ...
            a17*p^3*T^3 + a18*p^3*T^4) - ...
            (b1*S + b2*S*T + b3*S*T^2 + b4*S*T^3 + b5*S*p + b6*S*p^2);
    end
end