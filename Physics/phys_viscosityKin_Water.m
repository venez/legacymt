% [s] = phys_viscosityKin_Water(temperature,inKelvinQ)
% 
%   Functions returns as solution 's' the kinematic viscosity of the fluid
%   'water' for a given 'pressure' in [Pa], 'humidity' in a range of [0.0 ; 
%   1.0] and 'temperature' - in [�C] as default or in [K] by specifing the 
%   4th input argument.
% 
function [s] = phys_viscosityKin_Water(pressure,humidity,temperature,inKelvinQ)

    s = phys_viscosityDyn_Water(temperature,inKelvinQ) / ...
        phys_density_Water(pressure,humidity,temperature,inKelvinQ);
end