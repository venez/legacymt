% [s] = phys_pressureDyn(velocity,density,area,length)
% 
%   Functions returns as solution 's' the dynamic pressure calculated for
%   given fluid 'velocity' in [m/s] and 'density' in [kg/m�] for a 
%   specified 'area' [m�]. With the optional input 'length' in [m] the 
%   return value is a 2D vector. 
% 
function [s] = phys_pressureDyn(velocity,density,area,length)
    
    s = 2.0 / (density * velocity * abs(velocity) * area);
    if nargin > 3
        s = [s s(1) / length];
    end
end