% [pot,stream] = potFlow_2D_waveAiry(pos,ampl,omega,time,depth)
% 
%   Function returns for a two-dimensional problem the values for the
%   potential and stream function at a specified group of position vectors
%   'pos' for a wave according to Airy with a known amplitude 'ampl' 
%   (default = 1.0) and angular frequency 'omega' (default = 1.0) at a 
%   user-defined point in 'time' (default = 0.0) and 'depth' (default = 
%   100.0).
% 
%   [according to 'Clauss: Meerestechnische Konstruktionen, pp. 109-111, 
%   eq. 3.36+3.39, vol. 2, 2008']
% 
function [pot,stream,vel,acc] = potFlow_2D_waveAiry(pos,ampl,omega,time,depth)

a = 1.0;
d = 100.0;
t = 0.0;
w = 1.0;
switch nargin
    case 1
    case 2
        a = ampl;
    case 3
        a = ampl;
        w = omega;
    case 4
        a = ampl;
        t = time;
        w = omega;
    case 5
        a = ampl;
        d = depth;
        t = time;
        w = omega;
    otherwise
        error('Unknown number of input arguments');
end

x = pos(1,:);
z = -pos(3,:);

g = 9.81;
k = w.^2/g;
theta = w.*t - k*x;

% acc. Clauss.1988, p. 109, eq. 3.36
tmp1 = cosh(k .* (z + d));
tmp2 = sinh(k .* (z + d));
tmp3 = cosh(k .* d);
tmp13 = tmp1 ./ tmp3;
tmp23 = tmp2 ./ tmp3;

pot = - a * g ./ w .* tmp13 .* cos(theta);
stream = a * g ./ w .* tmp23 .* sin(theta);

% acc. Clauss.1988, p. 110, table 3.1
tmp4 = sinh(k .* d);
tmp14 = tmp1 ./ tmp4;
tmp24 = tmp2 ./ tmp4;

vel(1,:) =  a .* w .* tmp14 .* sin(theta);
vel(3,:) = -a .* w .* tmp24 .* cos(theta);

acc(1,:) = -a .* w.^2 .* tmp14 .* cos(theta);
acc(3,:) = -a .* w.^2 .* tmp24 .* sin(theta);
end