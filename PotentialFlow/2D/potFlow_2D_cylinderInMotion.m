% [pot,stream] = potFlow_2D_cylinderInMotion(pos,radius,vInf,origin)
% 
%   Function returns for a two-dimensional problem the values of potential
%   and stream function at a specified group of position vectors 'pos' for
%   a circular cylinder with a 'radius' (default = 1.0) and the motion
%   velocity of 'vInf' (default = 1.0) placed at 'origin' (default = 
%   [0 ; 0]).
% 
%   [according to 'Kennard  (1967): Irrotional Flow of Frictionless Fluids,
%   p. 148, eq. 67b']
% 
function [pot,stream] = potFlow_2D_cylinderInMotion(pos,radius,vInf,origin)

a = 1.0;
o = [0 ; 0];
v = 1.0;
switch nargin
    case 1
    case 2
        a = radius;
    case 3
        a = radius;
        v = vInf;
    case 3
        a = radius;
        o = origin;
        v = vInf;
    otherwise
        error('Unknown number of input arguments');
end

x = pos(1,:) - o(1);
y = pos(2,:) - o(1);
r2 = x.^2 + y.^2;

pot = -v*(a^2 ./ r2) .* x;
stream = v*(a^2 ./ r2) .* y;
end