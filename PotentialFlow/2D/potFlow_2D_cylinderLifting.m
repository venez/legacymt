% [pot,stream] = potFlow_2D_cylinderLifting(pos,radius,gamma,vInf,origin)
% 
%   Function returns for a two-dimensional problem the values of potential, 
%   and stream function at a specified group of position vectors 'pos' for 
%   a cylinder with a 'radius' (default = 1.0) and a circulation of 'gamma'
%   (default = 1.0) in an uniform flow of 'vInf' (default = 1.0) placed at
%   'origin' (default = [0 ; 0]).
% 
%   [according to 'Kennard (1967): Irrotional Flow of Frictionless Fluids,
%   p. 152, eq. 69']
% 
function [pot,stream] = potFlow_2D_cylinderLifting(pos,radius,gamma,vInf,origin)

a = 1.0;
g = 1.0;
o = [0 ; 0];
v = 1.0;
switch nargin
    case 1
    case 2
        a = radius;
    case 3
        a = radius;
        v = vInf;
    case 4
        a = radius;
        g = gamma;
        v = vInf;
    case 4
        a = radius;
        g = gamma;
        o = origin;
        v = vInf;
    otherwise
        error('Unknown number of input arguments');
end

x = pos(1,:) - o(1);
y = pos(2,:) - o(1);
r2 = x.^2 + y.^2;
r = sqrt(r2);
theta = atan2(y,x);

pot = v * (1 + a^2 ./ r2) .* x - g/2/pi .* theta;
stream = v * (1 - a^2 ./ r2) .* y - g/2/pi .* log(r/a);
end