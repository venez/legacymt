% [pot,stream] = potFlow_2D_uniform(pos,vInf)
% 
%   Function returns for a two-dimensional problem the values for potential 
%   and stream function at a specified group of position vectors 'pos' for 
%   a uniform flow with an optional value for 'vInf' (default = [1.0 0]).
% 
%   [according to 'Schlichting & Truckenbrodt, p. 54, vol. 1, Aerodynamik
%   des Flugzeuges']
% 
function [pot,stream] = potFlow_2D_uniform(pos,vInf)

switch nargin
    case 1
        v = [1.0 0];
    case 2
        v = vInf;
    otherwise
        error('Unknown number of input arguments');
end

nV = length(v);
if (nV ~= 2)
    v = [v 0];
elseif (nV > 2)
    error('vInf must be a vector with two components at most');
end

pot = v(1) * pos(1,:) + v(2) * pos(2,:);
stream = v(1) * pos(2,:) - v(2) * pos(1,:);
end