% [pot,stream] = potFlow_2D_doublet(pos,strength,axis,origin)
% 
%   Function returns for a two-dimensional problem the values for potential 
%   and stream function at a specified group of position vectors 'pos' for 
%   a doublet with an optional 'strength' (default = 1.0) and for an
%   optional 'axis' (default = 'X', allowed input: 'X','x',1,'Y','y',2)
%   placed at origin (default = [0 ; 0]).
% 
%   [according to 'Truckenbrodt: Fluidmechanik, p. 142, vol. 2, 2008']
% 
function [pot,stream] = potFlow_2D_doublet(pos,strength,axis,origin)

a = 'X';
o = [0 ; 0];
s = 1.0;
switch nargin
    case 1
    case 2
        s = strength;
    case 3
        a = axis;
        s = strength;
    case 4
        a = axis;
        o = origin;
        s = strength;
    otherwise
        error('Unknown number of input arguments');
end

if (s == 0)
    error('source strength must be unequal to 0');
end

x = pos(1,:) - o(1);
y = pos(2,:) - o(1);
z = x + 1i*y;
switch a
    case {'X','x',1}
        w = s/2/pi./z;
    case {'Y','y',2}
        w = 1i * s/2/pi./z;
    otherwise
        error('Unknown specification for 3rd input argument');
end

pot = real(w);
stream = imag(w);
end