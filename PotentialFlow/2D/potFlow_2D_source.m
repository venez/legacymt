% [pot,stream] = potFlow_2D_source(pos,strength,origin)
% 
%   Function returns for a two-dimensional problem the values for potential 
%   and stream function at a specified group of position vectors 'pos' for 
%   a source with an optional 'strength' (default = 1.0) placed at 'origin'
%   (default = [0 ; 0]).
% 
%   [according to 'Truckenbrodt: Fluidmechanik, p. 142, vol. 2, 2008']
% 
function [pot,stream] = potFlow_2D_source(pos,strength,origin)

o = [0 ; 0];
s = 1.0;
switch nargin
    case 1
    case 2
        s = strength;
    case 3
        o = origin;
        s = strength;
    otherwise
        error('Unknown number of input arguments');
end

if (s <= 0)
    error('source strength must be greater than 0');
end

x = pos(1,:) - o(1);
y = pos(2,:) - o(2);
z = x + 1i*y;
w = s/2/pi * log(z);

pot = real(w);
stream = imag(w);
end