% [pot,stream] = potFlow_2D_waveAiryInfinite(pos,ampl,omega,time,depth)
% 
%   Function returns for a two-dimensional problem the values for the
%   potential and stream function at a specified group of position vectors
%   'pos' for a wave according to Airy with a known amplitude 'ampl' 
%   (default = 1.0) and angular frequency 'omega' (default = 1.0) at a 
%   user-defined point in 'time' (default = 0.0) and 'depth' (default = 
%   100.0).
% 
%   [according to 'Clauss: Meerestechnische Konstruktionen, pp. 109-111, 
%   eq. 3.36+3.39, vol. 2, 2008']
% 
function [pot,stream,vel,acc] = potFlow_2D_waveAiryInfinite(pos,ampl,omega,time,depth)

a = 1.0;
d = 100.0;
t = 0.0;
w = 1.0;
switch nargin
    case 1
    case 2
        a = ampl;
    case 3
        a = ampl;
        w = omega;
    case 4
        a = ampl;
        t = time;
        w = omega;
    case 5
        a = ampl;
        d = depth;
        t = time;
        w = omega;
    otherwise
        error('Unknown number of input arguments');
end

x = pos(1,:);
z = -pos(3,:);

g = 9.81;
k = w.^2/g;
theta = w.*t - k*x;

% acc. Clauss.1988, p. 111
tmp = exp(k .* z);
tmp1 = 1/2 * tmp .* exp(k .* d);    % ~ cosh(k(z+d)) = sinh(k(z+d))
tmp2 = 1/2 * exp(k .* d);           % ~ cosh(k d) = sinh(k d))
tmp12 = tmp1 ./ tmp2;

pot = - a * g ./ w .* tmp12 .* cos(theta);
stream = a * g ./ w .* tmp12 .* sin(theta);

% acc. Clauss.1988, p. 110, table 3.1
vel(1,:) =  a .* w .* tmp .* sin(theta);
vel(3,:) = -a .* w .* tmp .* cos(theta);

acc(1,:) = -a .* w.^2 .* tmp .* cos(theta);
acc(3,:) = -a .* w.^2 .* tmp .* sin(theta);
end