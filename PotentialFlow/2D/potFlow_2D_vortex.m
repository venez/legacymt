% [pot,stream] = potFlow_2D_vortex(pos,strength,origin)
% 
%	Function returns for a three-dimensional problem the values for  
%   potential and stream function at a specified group of position vectors
%   'pos' for a vortex with an optional 'strength' (default = 1.0) placed 
%   at 'origin' (default = [0 ; 0]).
% 
%   [according to 'Truckenbrodt: Fluidmechanik, p. 142, vol. 2, 2008']
% 
function [pot,stream] = potFlow_2D_vortex(pos,strength,origin)

o = [0 ; 0];
s = 1.0;
switch nargin
    case 1
    case 2
        s = strength;
    case 3
        s = strength;
        o = origin;
    otherwise
        error('Unknown number of input arguments');
end

x = pos(1,:) - o(1);
y = pos(2,:) - o(2);
z = x + 1i * y;
w = -1i * s/2/pi * log(z);

pot = real(w);
stream = imag(w);
end