% [pot,stream] = potFlow_3D_sphereInMotion(pos,radius,vInf,origin)
% 
%   Streaming flow of a moving sphere through the fluid which is at rest at 
%   infinity
% 
%   Function returns for a three-dimensional problem the values of the
%   potential and stream function at a specified group of position vectors 
%   'pos' for a sphere with a 'radius' (default = 1.0), which is optional 
%   placed at 'origin' (default = [0 ; 0 ; 0]), with a user-defined 
%   velocity 'vInf' (default = 1.0).
% 
%   [according to 'Kennard  (1967): Irrotional Flow of Frictionless Fluids,
%   p. 319, eq. 127']
% 
function [pot,stream] = potFlow_3D_sphereInMotion(pos,radius,vInf,origin)

a = 1.0;
v = 1.0;
o = [0.0 ; 0.0 ; 0.0];
switch nargin
    case 1
    case 2
        a = radius;
    case 3
        a = radius;
        v = vInf;
    case 4
        a = radius;
        o = origin;
        v = vInf;
    otherwise
        error('Unknown number of input arguments');
end

x = pos(1,:) - o(1,:);
y = pos(2,:) - o(2,:);
z = pos(3,:) - o(3,:);

w2 = y.^2 + z.^2;
r = sqrt(x.^2 + w2);

pot = a^3 * v / 2 .* x ./ r.^3;
stream = -a^3 * v / 2 .* w2 ./ r.^3;
end