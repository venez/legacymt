% [pot,stream] = potFlow_3D_doublet(pos,strength,origin)
% 
%   Function returns for a two-dimensional problem the values for potential 
%   and stream function at a specified group of position vectors 'pos' for 
%   a doublet with an optional 'strength' (default = 1.0) placed at the
%   user-defined 'origin' (default = [0 ; 0 ; 0]).
% 
%   [according to 'Truckenbrodt: Fluidmechanik, p. 160, eq. 5.88, vol. 2, 
%   2008']
% 
function [pot,stream] = potFlow_3D_doublet(pos,strength,origin)

a = 'X';
o = [0.0 ; 0.0 ; 0.0];
switch nargin
    case 1
        s = 1.0;
    case 2
        s = strength;
    case 3
        o = origin;
        s = strength;
    otherwise
        error('Unknown number of input arguments');
end

if (s == 0)
    error('source strength must be unequal to 0');
end

x = pos(1,:) - o(1,:);
y = pos(2,:) - o(2,:);
z = pos(3,:) - o(3,:);

r2 = y.^2 + z.^2;
r0 = sqrt(r2 + x.^2);

pot = s/4/pi * x/r0.^3;
stream = -s/4/pi * r2/r0.^3;
end