% [pot,stream] = potFlow_3D_rankine(vInf,pos,length)
% 
%   Function returns for a three-dimensional problem the values for 
%   potential and stream function at a specified group of position vectors 
%   'pos' for a rankine body of known 'length' (default = 1.0) in an
%   uniform flow with an optional value for 'vInf' (default = 1.0) and its
%   'diameter' (default = 1.0).
% 
%   [according to 'Kennard (1967): Irrotional Flow of Frictionless Fluids,
%   p. 308, eq. 122a & 122b']
% 
function [pot,stream] = potFlow_3D_rankine(pos,length,vInf,diameter)

v = 1.0;
l = 1.0;
d = 1.0;
switch nargin
    case 1
    case 2
        l = length;
    case 3
        l = length;
        v = vInf;
    case 4
        d = diameter;
        l = length;
        v = vInf;
    otherwise
        error('Unknown number of input arguments');
end

a = l/2;
b = sqrt(d^2/8 * v);
r = pos(2,:).^2 + pos(3,:).^2;

r1 = sqrt((pos(1,:) - a)^2 + r);
r2 = sqrt((pos(1,:) + a)^2 + r);
pot = v*(pos(1,:) + b.^2/2*(1/r1 - 1/r2));

cTheta1 = a/r1;
cTheta2 = a/r2;
stream = 1/2*v*(r + b.^2*(cTheta1 - cTheta2));

end