% [pot,stream] = potFlow_3D_sphere(pos,radius,vInf,origin)
% 
%   Streaming flow past a stationary sphere, when the fluid at infinity has
%   a uniform velocity
% 
%   Function returns for a three-dimensional problem the values for 
%   potential and stream function at a specified group of position vectors 
%   'pos' for a sphere with a 'radius' (default = 1.0), which is optional 
%   placed at 'origin' (default = [0 ; 0 ; 0]), with a user-defined 
%   velocity 'vInf'  (default = 1.0).
% 
%   [according to 'Truckenbrodt: Fluidmechanik, p. 161, eq. 5.91, vol. 2, 
%   2008' and 'Kennard (1967): Irrotional Flow of Frictionless Fluids,
%   p. 320, eq. 128']
% 
function [pot,stream] = potFlow_3D_sphere(pos,radius,vInf,origin)

o = [0.0 ; 0.0 ; 0.0];
R = 1.0;
v = 1.0;
switch nargin
    case 1
    case 2
        R = radius;
    case 3
        R = radius;
        v = vInf;
    case 4
        o = origin;
        R = radius;
        v = vInf;
    otherwise
        error('Unknown number of input arguments');
end

x = pos(1,:) - o(1,:);
y = pos(2,:) - o(2,:);
z = pos(3,:) - o(3,:);

w2 = y.^2 + z.^2;
r = sqrt(x.^2 + w2);

pot = v .* x .*(1 + 1/2 * (R./r).^3);
stream = 1/2 * v .* (1 - (R./r).^3) .* w2;
end