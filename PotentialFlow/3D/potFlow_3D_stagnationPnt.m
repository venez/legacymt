% [pot,stream] = potFlow_2D_stagnationPnt(pos,vInf)
% 
%   Function returns for a two-dimensional problem the values for potential 
%   and stream function at a specified group of position vectors 'pos' for 
%   a stagnation point with an optional value for 'vInf' (default = 1.0).
% 
%   [according to 'Truckenbrodt: Fluidmechanik, p. 159, eq. 5.85, vol. 2, 
%   2008']
% 
function [pot,stream] = potFlow_3D_stagnationPnt(pos,vInf)

switch nargin
    case 1
        v = 1.0;
    case 2
        v = vInf;
    otherwise
        error('Unknown number of input arguments');
end

if (v <= 0)
    error('vInf must be > 0');
end

r2 = pos(1,:).^2 + pos(2,:).^2;
z = pos(3,:);

pot = v/2 * (r2 - 2*z.^2);
stream = -v * r2 .* z;
end