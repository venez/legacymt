% [pot] = potFlow_3D_uniform(pos,vInf)
% 
%	Function returns for a three-dimensional problem the values for 
%   potential function (stream function TBA) at a specified group of 
%   position vectors 'pos' for a uniform flow with an optional value for 
%   'vInf' (default = [1.0 0 0]).
% 
%   [according to 'Kennard (1967): Irrotional Flow of Frictionless Fluids,
%   p. 299, eq. 119b' and '119c']
% 
function [pot,stream] = potFlow_3D_uniform(pos,vInf)

switch nargin
    case 1
        v = [1.0 0 0];
    case 2
        v = vInf;
    otherwise
        error('Unknown number of input arguments');
end

switch length(v)
    case 1
        v = [v 0 0];
    case 3
    otherwise
        error('vInf must be a vector with one or three components');
end

mag = norm(v);
cosX = v(1)/mag;
cosY = v(2)/mag;
cosZ = v(3)/mag;

% uniform flow towards positive x
pot = mag*(cosX*pos(1,:) + cosY*pos(2,:) + cosZ*pos(3,:));
stream = 1/2*mag*(pos(2,:).^2 + pos(3,:).^2);
end