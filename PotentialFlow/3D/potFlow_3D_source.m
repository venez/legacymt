% [pot,stream] = potFlow_3D_source(pos,strength,origin)
% 
%   Function returns for a three-dimensional problem the values for  
%   potential and stream function at a specified group of position vectors
%   'pos' for a source with an optional 'strength' (default = 1.0) placed
%   at 'origin' (default = [0 ; 0 ; 0]).
% 
%   [according to 'Kennard (1967): Irrotional Flow of Frictionless Fluids,
%   p. 305, eq. 121']
% 
function [pot,stream] = potFlow_3D_source(pos,strength,origin)

s = 1.0;
o = zeros(3,1);
switch nargin
    case 1
    case 2
        s = strength;
    case 3
        s = strength;
        o = origin;
    otherwise
        error('Unknown number of input arguments');
end

if (s <= 0)
    error('source strength must be greater than 0');
end

r = v_norm(pos - o);
x = pos(1,:) - o(1);

pot = s/4/pi * 1/r;
stream = s/4/pi * x/r;
end