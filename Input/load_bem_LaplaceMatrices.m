% [L,M,Mt,N] = load_bem_LaplaceMatrices()
% 
%	Function loads resulting integral operators of a BEM simulation by
%	opening a dialog. After that the user chooses only one file to load all
%	for specified output.
% 
%   Example:
%       [l,m] = load_bem_LaplaceMatrices();
% 
function [L,M,Mt,N] = load_bem_LaplaceMatrices()


% Preparations
[FileName,PathName] = uigetfile('*.mat','Select the MATLAB matrices');
nameParts = strsplit(FileName,'_');


% Execution
L = load(strjoin([PathName nameParts(1) '_' ...
                           nameParts(2) '_L_' ...
                           nameParts(4)],''));
M = load(strjoin([PathName nameParts(1) '_' ...
                           nameParts(2) '_M_' ...
                           nameParts(4)],''));

if (nargout > 2)
    Mt = transpose(M);
    N = load(strjoin([PathName nameParts(1) '_' ...
                           nameParts(2) '_N_' ...
                           nameParts(4)],''));
end


end

