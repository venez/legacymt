% [structOut] = completeInput(structIn,default)
% 
%	Function completes a user-defined 'structIn' with fields and associated
%   values of an 'default' struct. In doing so there will be no overwriting
%   of already existing values.
% 
function [structOut] = completeInput(structIn, default)

% Seperate intended fields and their values
fieldValues = struct2cell(default);
fieldNames = fieldnames(default);


% Compare defined fields with default ones and return an logical array with
% stored information about their (non-)existing
compare = isfield(structIn, fieldNames);


% The resulting struct has at least the same fields as the defined struct
structOut = structIn;

if (min(compare) == 0)
    nFields = length(fieldNames);
    
    % Identify the missing fields and their associated values
    ID = int32(0);
    tmpSave = zeros(1,nFields,'int32');
    for i = 1:nFields
        if compare(i) == 0
            ID = ID + 1;
            tmpSave(ID) = i;
        end
    end
    tmpSave = tmpSave(1:ID);
    
    % Add information
    addFieldValues(1:2:2*ID) = fieldNames(tmpSave);
    addFieldValues(2:2:2*ID) = fieldValues(tmpSave);
    
    for i = 1:2:2*ID
        structOut.(addFieldValues{i}) = addFieldValues{i+1};
    end
end


end