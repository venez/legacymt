% [elemPts,elemNodes,elemCen,elemVec,elemSize] = load_bem_Model(src)
% 
%	Function loads variables needed for a BEM model from a specified file 
%   'src'. For an empty or missing input argument the function opens a 
%   dialog.
% 
%   Example:
%       [a,b,c,d,e] = load_bem_Model('C:\Documents\bem_model.mat');
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = load_bem_Model(src)


% Input handling
switch nargin
    case 0
        [FileName,PathName] = uigetfile('*.mat','Select the BEM model');
        location = [PathName FileName];
    case 1
        location = src;
    otherwise
        error('Unknown number of input arguments');
end


% Execution
tmp = load(location);
elemPts     = tmp.elemPts;
elemNodes	= tmp.elemNodes;
elemCen     = tmp.elemCen;
elemVec     = tmp.elemVec;
elemSize	= tmp.elemSize;


end