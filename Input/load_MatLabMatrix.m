% [s] = load_MatLabMatrix(filename,opts)
% 
%	Function loads variables from a specified 'filename' with user-defined 
%	options 'opts'. Latter contains the possibility to specify expected
%	variables in 'vars', start the search in 'dir' and to choose a multiple
%   selection by 'mult' where loaded data is stored in a struct.
% 
%   For an empty 'src' the function opens a dialog.
% 
%   Fields permitted for 'opts':
%       dir     - default = pwd
%       mult    - default = false
%       vars    - default = ''
% 
%   Examples:
%       s = load_MatLabMatrix();
% 
%       src = 'C:\Documents\';
%       opts.vars = 'res';
%       s = load_MatLabMatrix(src,opts);
% 
function [s] = load_MatLabMatrix(filename,opts)


% Input handling
default.dir = pwd;
default.vars = '';
default.mult = false;
dialogQ = false;
switch nargin
    case 0
        dialogQ = true;
        options = default;
    case 1
        options = default;
    case 2
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end


% Preparations
if (options.mult)
    multiSel = 'on';
else
    multiSel = 'off';
end
if (dialogQ || isempty(filename))
    [FileName,PathName] = uigetfile('*.mat','Select the MATLAB matrix', ...
                                    options.dir,'MultiSelect',multiSel);
    if iscell(FileName)
        nFiles = length(FileName);
        location = [];
        for ii = 1 : nFiles
            location = [location ; [PathName cell2mat(FileName(ii))]];
        end
    else
        nFiles = 1;
        location = [PathName FileName];
    end
    
else
    location = filename;
    nFiles = 1;
end


% Execution
for ii = 1 : nFiles
    tmp = load(location(ii,:));
    if (~isempty(options.vars))
        tmp = tmp.(options.vars);
    end
    
    if (nFiles == 1)
        s = tmp;
    else
        field = char(ii + 64);
        s.(field) = tmp;
    end
end

end