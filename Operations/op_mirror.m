% [coordsOut] = op_mirror(coordsIn,plane)
% 
%   Function executes a reflection for a group of specified points 
%   'coordsIn' at a user-defined 'plane'.
% 
%   Arguments permitted for 'plane':
%       'xy', 'XY', 'Xy', 'xY', 'yx', 'YX', 'yX', 'Yx'
%       'xz', 'XZ', 'Xz', 'xZ', 'zx', 'ZX', 'zX', 'Zx'
%       'yz', 'YZ', 'Yz', 'yZ', 'zy', 'ZY', 'zY', 'Zy'
% 
%   Example:
%       coordsOut = op_mirror(coordsIn,'xz');
% 
function [coordsOut] = op_mirror(coordsIn,plane)

% Execution
[m,n] = size(coordsIn);

msg = 'Unknown format for 1st input argument';
coordsOut = coordsIn;
switch plane
    % mirror Z coordinates
    case {'xy','XY','Xy','xY','yx','YX','yX','Yx'}
        if (m == 3)
            coordsOut(3,:) = -1.0*coordsIn(3,:);
        elseif (n == 3)
            coordsOut(:,3) = -1.0*coordsIn(:,3);
        elseif (m == 1)
            coordsOut(1,3 : 3 : n) = -1*coordsIn(1,3 : 3 : n);
        elseif (n == 1)
            coordsOut(3 : 3 : n,1) = -1*coordsIn(3 : 3 : n,1);
        else 
            error(msg);
        end
    % mirror Y coordinates
    case {'xz','XZ','Xz','xZ','zx','ZX','zX','Zx'}
        if (m == 3)
            coordsOut(2,:) = -1.0*coordsIn(2,:);
        elseif (n == 3)
            coordsOut(:,2) = -1.0*coordsIn(:,2);
        elseif (m == 1)
            coordsOut(1,2 : 3 : n-1) = -1*coordsIn(1,2 : 3 : n-1);
        elseif (n == 1)
            coordsOut(2 : 3 : n-1,1) = -1*coordsIn(2 : 3 : n-1,1);
        else 
            error(msg);
        end
    % mirror X coordinates
    case {'yz','YZ','Yz','yZ','zy','ZY','zY','Zy'}
        if (m == 3)
            coordsOut(1,:) = -1.0*coordsIn(1,:);
        elseif (n == 3)
            coordsOut(:,1) = -1.0*coordsIn(:,1);
        elseif (m == 1)
            coordsOut(1,2 : 3 : n-2) = -1*coordsIn(1,1 : 3 : n-2);
        elseif (n == 1)
            coordsOut(1 : 3 : n-2,1) = -1*coordsIn(1 : 3 : n-2,1);
        else 
            error(msg);
        end
    otherwise
        error('It is not possible to interpret the declared plane');
end

end