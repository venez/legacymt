% [coordsOut] = op_scale(coordsIn,factorVec)
% 
%	Function executes a scaling for a group of specified points 'coordsIn'
%   by a positive user-defined one or three dimensional vector 'factor'.
%   The former forces an uniform scaling.
% 
%   Example:
%       coordsOut1 = op_scale(coordsIn,1.5);
%       coordsOut2 = op_scale(coordsIn,[1.5 1 1]);
%       coordsOut3 = op_scale(coordsIn,[1.5 ; 1 ; 1]);
% 
function coordsOut = op_scale(coordsIn,factor)


% Error handling
if (length(factor) == 1) 
    f = repmat(factor,[3 1]);
elseif (length(factor) ~= 3)
    error('factorVec needs to be a vector of 3 elements');
else
    f = factor;
end

if (f(1) < 0) || (f(2) < 0) || (f(3) < 0)
    error('Scaling factor must be positive');
end
if (f(1)==0) && (f(2)==0) && (f(3)==0)
    error('Scaling to zero specified');
end


% Execution
[m,n] = size(coordsIn);
if (m == 3)
    coordsOut(1,:) = coordsIn(1,:) * f(1);
    coordsOut(2,:) = coordsIn(2,:) * f(2);
    coordsOut(3,:) = coordsIn(3,:) * f(3);
elseif (n == 3)
    coordsOut(:,1) = coordsIn(:,1) * f(1);
    coordsOut(:,2) = coordsIn(:,2) * f(2);
    coordsOut(:,3) = coordsIn(:,3) * f(3);
elseif (m == 1)
    coordsOut(1,1 : 3 : n-2) = coordsIn(1,1 : 3 : n) * f(1);
    coordsOut(1,2 : 3 : n-1) = coordsIn(1,2 : 3 : n) * f(2);
    coordsOut(1,3 : 3 : n  ) = coordsIn(1,3 : 3 : n) * f(3);
elseif (n == 1)
    coordsOut(1 : 3 : n-2,1) = coordsIn(1 : 3 : n,1) * f(1);
    coordsOut(2 : 3 : n-1,1) = coordsIn(2 : 3 : n,1) * f(2);
    coordsOut(3 : 3 : n  ,1) = coordsIn(3 : 3 : n,1) * f(3);
else
    error('Unknown format for 1st input argument');
end


end