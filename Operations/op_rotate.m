% [coordsOut,RM] = op_rotate(coordsIn,axis,direction,angle)
% [coordsOut,RM] = op_rotate(coordsIn,axis,direction,angle,vector3D)
% 
%   Functions executes a rotation for a group of specified points
%   'coordsIn' around user-defined 'axis' (for 'custom' argument an
%   additional 'vector3D' must be specified) in 'direction' by an assigned
%   'angle' [rad] and returns the used rotation matrix 'RM' if desired.
% 
%   Arguments permitted for 'axis':
%       x axis			{'x', 'X', 1}, 
%		Y axis			{'y', 'Y', 2,},
%		Z axis			{'z', 'Z', 3},
%		custom axis		{'c', 'C', 'custom'}
%   Arguments permitted for 'direction':
%       clockwise           {'+', 'r', 'R', 'right', 'RIGHT', 0}
%       counterclockwise	{'-', 'l', 'L', 'left', 'LEFT', 1}
% 
%   Example clockwise:
%       coordsOut = op_rotate(coordsIn,'z','+',pi);
% 
%   Example counterclockwise:
%       coordsOut = op_rotate(coordsIn,'y','left',pi/2);
% 
% 
function [coordsOut,RM] = op_rotate(coordsIn,axis,direction,angle,vector3D)

% Error handling
if (nargin > 4) && ~(isequal(axis,'c') || isequal(axis,'C') || isequal(axis,'custom'))
    error('Contradictory argument combination');
end

% Defintion of transformation matrices
msgDir = 'It is not possible to interpret the declared direction';
msgAxis = 'It is not possible to interpret the declared axis';
cAngle = cos(angle);
sAngle = sin(angle);
switch axis
    case {'x','X',1}
        switch direction
            case {'+','r','R','right','RIGHT',0}
                RM = [ 1    0       0;
                       0    cAngle  sAngle;
                       0   -sAngle	cAngle;];
            case {'-','l','L','left','LEFT',1}
                RM = [ 1    0       0;
                       0    cAngle -sAngle;
                       0    sAngle  cAngle;];
            otherwise
                error(msgDir);
        end
	case {'y','Y',2}
        switch direction
            case {'+','r','R','right','RIGHT',0}
                RM = [ cAngle   0  -sAngle;
                       0        1   0;
                       sAngle   0   cAngle;];
        	case {'-','l','L','left','LEFT',1}
                RM = [ cAngle   0   sAngle;
                       0        1   0;
                      -sAngle   0   cAngle;];
            otherwise
                error(msgDir);
        end
	case {'z','Z',3}
        switch direction
            case {'+','r','R','right','RIGHT',0}
                RM = [ cAngle	sAngle	0;
                	  -sAngle   cAngle  0;
                       0        0       1];
        	case {'-','l','L','left','LEFT',1}
                RM = [ cAngle  -sAngle	0;
                       sAngle   cAngle  0;
                       0        0       1];
            otherwise
                error(msgDir);
        end
	case {'c','C','custom'}
        if (nargin ~= 5)
            error('Unknown number of arguments for chosen specification');
        end
        if (length(vector3D) ~= 3)
			error(msgAxis);
        end
        
        [mV,~] = size(vector3D);
        if (mV == 3)
            tmpAxis = v_norm(vector3D);
        else
            tmpAxis = v_norm(vector3D,true);
        end
		n1 = tmpAxis(1);
		n2 = tmpAxis(2);
		n3 = tmpAxis(3);
		tmpAngle = 1 - cAngle;
	
        switch direction
            case {'+','r','R','right','RIGHT',0}
                RM = [ n1^2*tmpAngle + cAngle ...
					   n1*n2*tmpAngle + n3*sAngle ...
					   n1*n3*tmpAngle - n2*sAngle ...
					   ; ...
                	   n2*n1*tmpAngle - n3*sAngle ...
					   n2^2*tmpAngle + cAngle ...
					   n2*n3*tmpAngle + n1*sAngle ...
					   ; ...
					   n3*n1*tmpAngle + n2*sAngle ...
					   n3*n2*tmpAngle - n1*sAngle ...
					   n3^2*tmpAngle + cAngle ];
        	case {'-','l','L','left','LEFT',1}
                RM = [ n1^2*tmpAngle + cAngle ...
					   n1*n2*tmpAngle - n3*sAngle ...
					   n1*n3*tmpAngle + n2*sAngle ...
					   ; ...
                	   n2*n1*tmpAngle + n3*sAngle ...
					   n2^2*tmpAngle + cAngle ...
					   n2*n3*tmpAngle - n1*sAngle ...
					   ; ...
					   n3*n1*tmpAngle - n2*sAngle ...
					   n3*n2*tmpAngle + n1*sAngle ...
					   n3^2*tmpAngle + cAngle ];
            otherwise
                error(msgDir);
        end
    otherwise
        error(msgAxis);
end

% Execution
[m,n] = size(coordsIn);
coordsOut = zeros(m,n,'like',angle);
if (m == 3)
    for i = 1:n
        coordsOut(:,i) = RM * coordsIn(:,i);
    end
elseif (n == 3)
    for i = 1:m
        coordsOut(i,:) = RM * transpose(coordsIn(i,:));
    end
elseif (m == 1)
	for i = 3:3:n
        coordsOut(1,i-2 : i) = RM * [coordsIn(1,i-2);...
                                     coordsIn(1,i-1);...
                                     coordsIn(1,i  )];
	end
elseif (n == 1)
    for i = 3:3:m
        coordsOut(i-2 : i,1) = RM * [coordsIn(i-2,1);...
                                     coordsIn(i-1,1);...
                                     coordsIn(i  ,1)];
    end
else
    error('Unknown format for 1st input argument');
end

end