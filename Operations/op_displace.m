% [coordsOut] = op_displace(coordsIn,offsetVec)
% 
%	Function executes a displacement for a group of specified points 
%   'coordsIn' by a user-defined three dimensional vector 'offset'.
% 
%   Example:
%       coordsOut = op_displace(coordsIn,[-1 0 0]);
% 
function [coordsOut] = op_displace(coordsIn,offset,warnQ)

% Input handling
warnMsgQ = true;
if nargin == 3
    warnMsgQ = warnQ;
end


% Error handling
if length(offset) ~= 3
    error('offsetVec needs to be a vector of 3 elements');
end
if (offset(1)==0) && (offset(2)==0) && (offset(3)==0) && (warnMsgQ)
    warning('Zero displacement specified');
end


% Execution
[m,n] = size(coordsIn);
if (m == 3)
    coordsOut(1,:) = coordsIn(1,:) + offset(1);
    coordsOut(2,:) = coordsIn(2,:) + offset(2);
    coordsOut(3,:) = coordsIn(3,:) + offset(3);
elseif (n == 3)
    coordsOut(:,1) = coordsIn(:,1) + offset(1);
    coordsOut(:,2) = coordsIn(:,2) + offset(2);
    coordsOut(:,3) = coordsIn(:,3) + offset(3);
elseif (m == 1)
    coordsOut(1,1 : 3 : n-2) = coordsIn(1,1 : 3 : n) + offset(1);
    coordsOut(1,2 : 3 : n-1) = coordsIn(1,2 : 3 : n) + offset(2);
    coordsOut(1,3 : 3 : n  ) = coordsIn(1,3 : 3 : n) + offset(3);
elseif (n == 1)
    coordsOut(1 : 3 : n-2,1) = coordsIn(1 : 3 : n,1) + offset(1);
    coordsOut(2 : 3 : n-1,1) = coordsIn(2 : 3 : n,1) + offset(2);
    coordsOut(3 : 3 : n  ,1) = coordsIn(3 : 3 : n,1) + offset(3);
else
    error('Unknown format for 1st input argument');
end


end