% [dest,idx] = op_select(prob,src,dim,limit)
% 
%   Functions executes a selection for a group of specified points 'src' in
%   a user-defined dimension 'dim' within a 'limit' and returns optional
%   the indices, which mark the position of 'dest' in 'src'.
%	
%   Arguments permitted for 'prob':
%       'beyond','BEYOND','outOf','out'
%       'within','WITHIN','IN','in','In','iN'
% 
%   Arguments permitted for 'dim':
%       'X','x',1,'Y','y',2,'Z','z',3
% 
%   Argument format permitted for 'limit':
%       [minValue maxValue]
% 
%   Example:
%       coordsOut = op_select('interior',coordsIn,{'X','Y'},0.1);
% 
function [dest,idx] = op_select(prob,src,dim,limit)

% Input handling
msg = 'Unknown dimension choice';
switch nargin
    case 4
    otherwise
        error('Unknown number of input arguments');
end


% Preparations
[m,n] = size(src);
if (m < n)
    s = src;
    colMajor = true;
else
    s = src';
    colMajor = false;
end

nDim = length(dim);
if (iscell(dim))
    dir = char(dim);
else
    dir = dim;
end

if (length(limit) == 1)
    extreme = repmat([-limit limit],[nDim 1]);
else
    extreme = limit;
end

interVal = zeros(2*nDim,length(s));
for ii = 1:nDim
    switch dir(ii)
        case {1,'X','x'}
            row = 1;
        case {2,'Y','y'}
            row = 2;
        case {3,'Z','z'}
            row = 3;
        otherwise
            error(msg);
    end
    interVal(2*ii-1,:) = (s(row,:) > extreme(ii,1));
    interVal(2*ii,:)   = (s(row,:) < extreme(ii,2));
end
interVal(end + 1,:) = sum(interVal);


% Execution
crit = 2*nDim;
switch prob
    case {'beyond','BEYOND','outOf','out'}
        dest = s(:,interVal(end,:) ~= crit);
    case {'within','WITHIN','IN','in','In','iN'}
        dest = s(:,interVal(end,:) == crit);
    otherwise
        error('Unknown type');
end


% Output handling
if (nargout > 1)
    s = s';
    tmpDest = dest';
    m = length(dest);
    idx = zeros(1,m);
    for ii = 1:m
        [~,idx(ii)] = ismember(tmpDest(ii,:),s,'rows');
    end
end

if (~colMajor)
    dest = dest';
    idx = idx';
end

end