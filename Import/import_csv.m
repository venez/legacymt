% function  [A] = import_csv(filename,opts)
%
%      Reads the contents of the .csv-file 'filename' into the matrix 'A'.
% 
%   Fields permitted for 'opts':
%       cols    - column offset 
%       rows    - row offset
%
%   Example: 
%       opts.rows = 2
%       opts.cols = [0 6]
%       dest = import_csv([],opts)
% 
function [A] = import_csv(filename,opts)

% input handling
default.cols = [];
default.ext = '.csv';
default.file = '';
default.rows = [];
switch nargin
    case 0
        options = default;
    case 1
        options = default;
        options.file = filename;
    case 2
        options = completeInput(opts,default);
        options.file = filename;
    otherwise
        error('Unknown number of input arguments');
end

% Preparations
if (isempty(options.file))
    [FileName,PathName] = uigetfile(['*' options.ext], ...
                                     'Select file to Load from CSV');
    options.file = [PathName FileName];
end

% read information and open file
if isempty(options.cols) && isempty(options.rows)
    A = csvread(options.file);
elseif (length(options.cols) == 1) && isempty(options.rows)
    C1 = options.cols(1);
    R1 = 0;
	A = csvread(options.file,R1,C1);
elseif isempty(options.cols) && (length(options.rows) == 1)
    C1 = 0;
    R1 = options.rows(1);
	A = csvread(options.file,R1,C1);
elseif (length(options.cols) == 1) && (length(options.rows) == 1)
    C1 = options.cols(1);
    R1 = options.rows(1);
	A = csvread(options.file,R1,C1);
elseif (length(options.cols) == 2) && (length(options.rows) == 2)
    C1 = options.cols(1);
    R1 = options.rows(1);
    RC = [options.rows(1) options.cols(1) options.rows(2) options.cols(2)];
	A = csvread(options.file,R1,C1,RC);
else
    error('Unknown ');
end


end
