% [elemPts,elemNodes,elemCen,elemVec,elemSize] = import_stl(filename,opts)
% 
% Function imports geometry from an ASCII or binary STL file into MATLAB
% and identifies various parameters for further using.
% 
%   Fields permitted for 'opts':
%       form    - default = 'standard' (uses line breaks)
%                 alternative = 'alternate' (without line breaks)
%       mode    - default = 'ascii'
%                 alternative #1 = 'binary'
%                 alternative #2 = 'unknown' (determines format)
%       scale   - default = [1 1 1] 
%                 alternative = [scale_x, scale_y, scale_z]
%                 alternative = scale_uni
%       warn    - de-/activates warning message for non-coherent import
% 
%   Example: 
%       opts.mode = 'binary'
%       [a,b,c,d,e] = import_stl('file.stl',opts)
% 
function [elemPts,elemNodes,elemCen,elemVec,elemSize] = import_stl(filename,opts)

% input handling
default.form = 'standard';
default.mode = 'ascii';
default.scale = [1 1 1];
default.warnQ = true;

switch nargin
    case 0
        options = default;
        file = [];
    case 1
        options = default;
        file = filename;
    case 2
        options = completeInput(opts,default);
        file = filename;
    otherwise
        error('Unknown number of input arguments');
end

% find file if neccessary
if (isempty(file))
    [FileName,PathName] = uigetfile('*.stl','Select STL file to load');
    file = [PathName FileName];
end

% load STL data
[Facets,Vertices,TriNorms,binaryQ] = read_stl(file,options);
if (~isequal(options.mode,'ascii')) && (~isequal(options.mode,'binary'))
    if (binaryQ)
        options.mode = 'binary';
    else
        options.mode = 'ascii';
    end
end

% identify vertices of each element
[elemPts,Facets] = stl_createUniqueTriangles(Facets,Vertices,options);

% node IDs
if (nargout > 1)
    elemNodes = Facets;
end

% center points of elements
if (nargout > 2)
    elemCen = geom_op_calcElemCen(elemPts,elemNodes);
end

% normal unit vector for each element
if (nargout > 3)
    elemVec = TriNorms';
end

% scale center Points and Poitns 
if (~isequal(options.scale,[1 1 1]) || ~isequal(options.scale,[1 ; 1 ; 1]))
    elemPts = op_scale(elemPts,options.scale);
    if (nargout > 2 )
        elemCen = op_scale(elemCen,options.scale);
    end
end

% calculate element size
if (nargout > 4)
    elemSize = geom_op_calcElemSize(elemPts,elemNodes,true);
end

% last checkup
if (nargout > 3) && (length(elemNodes) ~= length(elemVec)) && (options.warnQ)
    warning('There are less normal vectors than elements');
end


end