% [rows,cols,entries,rep,field,symm,dataline,file] = read_mm(filename)
%
%  Reads the contents of the Matrix Market file 'filename' and extracts 
%  size and storage information.
%
%  In case of coordinate matrices, entries refers to the number of 
%  coordinate entries stored in the file.  The number of non-zero entries
%  in the final matrix cannot be determined until the data is read (and 
%  symmetrized, if necessary).
%
%  In the case of array matrices, entries is the product rows*cols, 
%  regardless of whether symmetry was used to store the matrix efficiently.
% 
%  Output argument 'file' refers to chosen Matrix Market file by dialog, if
%  no input argument was given. 'Dataline' specify the line number where
%  header and other information end.
%  
%   Example: 
%       [a,b,c,d,e,f,g] = mm_info('file.mtx')
%
function [rows,cols,entries,rep,field,symm,dataline,file] = read_mm(filename)

% input handling
default.file = '';
switch nargin
    case 0
        options = default;
    case 1
        options.file = filename;
%     case 2
%         options = completeInput(opts,default);
%         options.file = filename;
    otherwise
        error('Unknown number of input arguments');
end


% find file if neccessary
if (isempty(options.file))
    [FileName,PathName] = uigetfile('*.mtx', ...
                                    'Select MatrixMarket file to load');
    options.file = [PathName FileName];
end
file = options.file;


% open file if possible
mmfile = fopen(options.file,'r');
if (mmfile == -1)
    disp(options.file);
    error('File not found');
end


% read header and extract information
header = fgets(mmfile);
if (header == -1 )
	error('Empty file.')
end   
[head0,header] = strtok(header);
[head1,header] = strtok(header);
[rep,header]   = strtok(header);
[field,header] = strtok(header);
[symm,~]       = strtok(header);

head1 = lower(head1);
rep   = lower(rep);
field = lower(field);
symm  = lower(symm);


% verify file and header format
if isempty(symm)
   disp('Not enough words in header line. Recognized format: ')
   disp('%%MatrixMarket matrix representation field symmetry')
   error('Check header line.')
end
if ~strcmp(head0,'%%MatrixMarket')
   error('Not a valid MatrixMarket header.')
end
if ~ strcmp(head1,'matrix')
   disp(['This seems not to be a MatrixMarket ',head1,' file.']);
   disp('This function only knows how to read MatrixMarket matrix files.');
   error('Not a valid MatrixMarket file.');
end


% read through comments and ignoring them
commentline = fgets(mmfile);
nCommentlines = 0;
while (~isempty(commentline)) && (commentline(1) == '%')
  commentline = fgets(mmfile);
  nCommentlines = nCommentlines + 1;
end


% read size information, then branch according to sparse or dense format
if (strcmp(rep,'coordinate'))
    coordFormatQ = true;
    format = '%d%d%d';
    nCols = 3;
elseif (strcmp(rep,'array'))
    coordFormatQ = false;
    format = '%d%d';
    nCols = 2;
else
    error('Unknown representation specified in header.');
end

[sizeinfo,infolines] = sscanf(commentline,format);
while (infolines == 0)
    commentline = fgets(mmfile);
    if (commentline == -1)
        error('End-of-file reached before size information was found.')
    end

    [sizeinfo,infolines] = sscanf(commentline,format);
    if (infolines > 0) && (infolines ~= nCols)
        error('Invalid size specification line.')
    end
end
dataline = nCommentlines + infolines;

rows = sizeinfo(1);
cols = sizeinfo(2);
if (coordFormatQ)
    entries = sizeinfo(3);
else
    entries = rows*cols;
end


% finish & clean up
fclose(mmfile);

