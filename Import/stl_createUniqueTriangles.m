% [elemPts,elemNodes] = stl_createUniqueTriangles(facets,vertices,opts)
% 
%   Detailed explanation goes here (TODO)
% 
function [elemPts,elemNodes] = stl_createUniqueTriangles(facets,vertices,opts)

% Input handling
default.mode = 'ascii';
default.warnQ = true;
switch nargin
    case 2
        options = default;
    case 3
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end


% Execution
[~,nTriangles] = size(facets);     
if isequal(options.mode,'binary')
    [C,~,ic] = unique(vertices.','rows');
    
    tmp(1,:) = ic(facets(1,:));
    tmp(2,:) = ic(facets(2,:));
    tmp(3,:) = ic(facets(3,:));
    facets = tmp;
elseif isequal(options.mode,'ascii')
    % minimze number of nodes and sort them by their ID 
    C = unique([reshape(facets,[1,3*nTriangles]); vertices].','rows').';
else
    error('Unknown identifation mode');
end


% Output handling
if isequal(options.mode,'binary')
    elemPts = C.';
elseif isequal(options.mode,'ascii')
    elemPts = C(2:end,:);
end

elemNodes = facets;
if (options.warnQ) && (nargout > 1) && (isequal(options.mode,'ascii'))
    warning('2nd output argument is a copy of 1st input argument');
end


end