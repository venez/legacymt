% [A,rows,cols,entries,rep,type,symm] = import_mtx(filename,opts)
%
% Reads the contents of the Matrix Market file 'filename' into the matrix 
% 'A'. 'A' will be either sparse or full, depending on the Matrix Market 
% format indicated by 'coordinate' (coordinate sparse storage), or 'array'
% (dense array storage). The data will be duplicated as appropriate if 
% symmetry is indicated in the header.
%
% Optionally, size information about the matrix can be obtained by using 
% the return values rows, cols, and entries, where latter is the number of
% nonzero entries in the final matrix. Type information can also be 
% retrieved using the optional return values rep (representation)and symm 
% (symmetry).
%
function  [A,nRows,nCols,nEntries,rep,type,symm] = import_mtx(filename,opts)

% input handling
default.file = '';
default.spar = false;
switch nargin
    case 0
        options = default;
    case 1
        options.file = filename;
    case 2
        options = completeInput(opts,default);
        options.file = filename;
    otherwise
        error('Unknown number of input arguments');
end


% read information and open file
[nRows,nCols,nEntries,rep,type,symm,dataline,options.file] = read_mm(options.file);
mmfile = fopen(options.file,'r');


% read size information, then branch according to format
if isequal(rep,'coordinate')
    coordFormatQ = true;
elseif isequal(rep,'array')
    coordFormatQ = false;
else
    error('Unknown representation specified in header.');
end

msg = ['Matrix type: ' type '. Invalid matrix type specification. ' ...
       'Check header against MatrixMarket documentation.'];

A = zeros(nRows,nCols);
if (coordFormatQ)
    switch (type)
        case 'pattern'
        case 'real'
            format = strcat('%d %d %f');
        case 'complex'
            format = strcat('%d %d %f %f');
        otherwise
            error(msg)
    end

    T = textscan(mmfile,format,'Headerlines',dataline - 1);
    I = cell2mat(T(:,1));
    J = cell2mat(T(:,2));
    V = cell2mat(T(:,3));
    chooseCol = 4;
else
    switch (type)
        case 'pattern'
        case 'real'
            format = strcat('%f');
        case 'complex'
            format = strcat('%f %f');
        otherwise
            error(msg)
    end

    T = textscan(mmfile,format,'Headerlines',dataline);
    V = cell2mat(T(:,1));
    if isequal(symm,'general')
        I = reshape(repmat(1:nRows,[1 nCols]),[nEntries 1]);
        J = reshape(repmat(1:nCols,[nRows 1]),[nEntries 1]);
    else
        I = [];
        J = [];
        for ii = 1:nCols
            I = [I ; (ii : nRows)'];
        end
        for jj = 1:nRows
            J = [J ; jj*ones(nRows - jj + 1,1)];
        end
        nEntries = nnz(tril(A + ones(nRows,nCols)));
    end
    chooseCol = 2;
end


% fill target matrix
switch (type)
    case 'real'
        for kk = 1 : nEntries
            A(I(kk),J(kk)) = V(kk);
        end
    case 'complex'
        C = cell2mat(T(:,chooseCol));
        for kk = 1 : nEntries
            A(I(kk),J(kk)) = V(kk) + 1i*C(kk);
        end
    case 'pattern'
end

    
% handle symmetry conditions to complete matrix
switch (symm)
    case {'symmetric','hermitian'}
        A = A + tril(A,-1)';
    case {'skew-symmetric','skew-hermitian'}
        A = A - tril(A,-1)';
%     case 
%         A = A - A' - diag(diag(A));
    otherwise
end
nEntries = nnz(A);


% create sparse matrix if neccessary
if (~isequal(symm,'general')) && (options.spar)    
    [row,col,v] = find(A);
    A = sparse(row,col,v,nRows,nCols,nEntries);
end


% finish & clean up
fclose(mmfile);

end
