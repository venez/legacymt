% [F,V,N,binaryQ] = readSTL(filename,opts)
% 
% Function imports geometry from an ASCII or binary STL file into MATLAB.
% 
%   Fields permitted for 'opts':
%       file    - default = ''
%       form    - default = 'standard'
%                 alternative = custom horizontally concatenated string
%                 (only effective in combination with 'ascii' mode)
%       mode    - default = 'ascii'
%                 alternative #1 = 'binary'
%                 alternative #2 = 'unknown' (determines format)
%   
%   Example: 
%       opts.mode = 'ascii'
%		opts.form = strcat(...
%           '%*s %*s %f32 %f32 %f32 \n ',... % facet normal
%           '%*s %*s \n ',...                % outer loop
%           '%*s %f32 %f32 %f32 \n ',...     % vertex
%           '%*s %f32 %f32 %f32 \n ',...     % vertex
%           '%*s %f32 %f32 %f32 \n ',...     % vertex
%           '%*s \n ', ...                   % endloop
%           '%*s \n');                       % endfacet
%       [faces,vertices,normals] = import_stl_fast('file.stl',opts)
% 
function [F,V,N,binaryQ] = read_stl(filename,opts)

% INPUT HANDLING
default.form = 'standard';
default.mode = 'ascii';

switch nargin
    case 1
        options = default;
    case 2
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end


% ERROR HANDLING
if ~exist(filename,'file')
    error(['File ''%s'' not found. If the file is not on MATLAB''s path'...
           ', be sure to specify the full path to the file.'], filename);
end
    

% IMPORT
fid = fopen(filename,'r');
if ~isempty(ferror(fid))
    error(lasterror);
end
    

% READING
if isequal(options.mode,'unknown')
	test = fread(fid,inf,'uint8=>uint8');
	if (testFormatForBinary(test) == true)
		options.mode = 'binary';
	else
		options.mode = 'ascii';
	end
    fclose(fid);
    fid = fopen(filename,'r');    
end

if isequal(options.mode,'binary')
	data = fread(fid,inf,'uint8=>uint8');
    [f,v,n] = stlbinary(data);
    binaryQ = true;
    
elseif isequal(options.mode,'ascii')
    if isequal(options.form,'standard')
        format = strcat(...
            '%*s %*s %f32 %f32 %f32 \r\n ',... % facet normal
            '%*s %*s \r\n ',...                % outer loop
            '%*s %f32 %f32 %f32 \r\n ',...     % vertex
            '%*s %f32 %f32 %f32 \r\n ',...     % vertex
            '%*s %f32 %f32 %f32 \r\n ',...     % vertex
            '%*s \r\n ', ...                   % endloop
            '%*s \r\n');                       % endfacet
    else
        format = options.form;
    end
    data = textscan(fid, format, 'HeaderLines', 1);
    [f,v,n] = stlascii(data);
    binaryQ = false;
    
else
    error('Unknown file format');
end
    
% OUTPUT
switch nargout        
    case 2
        F = f;
        V = v;
    case {3,4}
        F = f;
        V = v;
        N = n;        
    otherwise
        F = struct('faces',f,'vertices',v);
end
    
% CLEANING
fclose(fid);

end


function [F,V,N] = stlbinary(data)

    % STL format according to wikipedia
    %   UINT8[80] � Header
    %   UINT32 � Number of triangles
    % 
    %   foreach triangle
    %   REAL32[3] � Normal vector
    %   REAL32[3] � Vertex 1
    %   REAL32[3] � Vertex 2
    %   REAL32[3] � Vertex 3
    %   UINT16 � Attribute byte count
    %   end
    
    F = [];
    V = [];
    N = [];
    
    if length(data) < 84
        error('MATLAB:stlread:incorrectFormat', ...
              'Incomplete header information in binary STL file.');
    end
    
    % Bytes 81-84 are an unsigned 32-bit integer specifying the number of faces
    % that follow.
    numFaces = typecast(data(81:84),'uint32');
    if numFaces == 0
        warning('MATLAB:stlread:nodata','No data in STL file.');
        return
    end
    
    T = data(85:end);
    F = NaN(3,numFaces);
    V = NaN(3*numFaces,3);
    N = NaN(numFaces,3);
    
    numRead = 0;
    while numRead < numFaces
        % Each facet is 50 bytes
        %  - Three single precision values specifying the face normal vector
        %  - Three single precision values specifying the first vertex (XYZ)
        %  - Three single precision values specifying the second vertex (XYZ)
        %  - Three single precision values specifying the third vertex (XYZ)
        %  - Two unused bytes
        i1    = 50 * numRead + 1;
        i2    = i1 + 50 - 1;
        facet = T(i1:i2)';
        
        n  = typecast(facet( 1:12),'single');
        v1 = typecast(facet(13:24),'single');
        v2 = typecast(facet(25:36),'single');
        v3 = typecast(facet(37:48),'single');

        n = double(n);
        v = double([v1;v2;v3;]);
        
        % Figure out where to fit these new vertices, and the face, in the
        % larger F and V collections.        
        fInd  = numRead + 1;        
        vInd1 = 3 * (fInd - 1) + 1;
        vInd2 = vInd1 + 3 - 1;

        V(vInd1:vInd2,:) = v;
        F(:,fInd)        = vInd1:vInd2;
        N(fInd,:)        = n;
        
        numRead = numRead + 1;
    end 
    V = V.';
end


function [F,V,N] = stlascii(data)
    %extract normal vectors and vertices
    N = cell2mat(data(1:3));
    N = N(1:end-1,:); %strip off junk from last line

    v1 = cell2mat(data(4:6));
    v2 = cell2mat(data(7:9));
    v3 = cell2mat(data(10:12));

    if (isnan(data{4}))
        v1 = v1(1:end-1,:); %strip off junk from last line
        v2 = v2(1:end-1,:); %strip off junk from last line
        v3 = v3(1:end-1,:); %strip off junk from last line
    end

    v_temp = [v1 v2 v3]';
    V = zeros(3,numel(v_temp)/3);

    V(:) = v_temp(:);
%     V = V';
    
    [~,F] = fv2pt(V,length(V)/3);%gets points and triangles
end

function [p,t] = fv2pt(v,fnum)
    %gets points and trian+gle indexes given vertex and facet number
    c = size(v,2);

    %triangles with vertex id data
    t = zeros(3,fnum);
    t(:) = 1:c;

    %now we have to keep unique points pro vertex
    [p,~,j] = unique(v.','rows'); %now v=p(j) p(i)=v;
    t(:) = j(t(:));
    % t=t';
end

function tf = testFormatForBinary(src)
	% use first line of STL source file 'src' to identify its format, ...
	% where 'solid' is keyword for the ASCII format
	
	if isempty(src) || length(src) < 5
		error('MATLAB:stlread:incorrectFormat', ...
              'File does not appear to be an ASCII or binary STL file.');
	end    
    if strcmpi('solid',char(src(1:5)'))
		tf = false; % ASCII checked
    else
		tf = true;  % Binary checked
    end
end