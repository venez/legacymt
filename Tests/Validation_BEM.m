clearvars
close all
clc


%% Add nessessary MATLAB files 
workDir = 'F:\MatLab\#Workspace\';
addpath(genpath([workDir 'Geometry\']));
addpath(genpath([workDir 'Integration\']));
addpath(genpath([workDir 'PotentialFlow\']));
addpath([workDir 'AddedInertia\'], ...
        [workDir 'BEM\'], ...
        [workDir 'Export\'], ...
        [workDir 'Input\'], ...
        [workDir 'Mathematics\'], ...
        [workDir 'Operations\'], ...
        [workDir 'Output\'] ...
);


%% SETTINGS
twoDimQ = false;
exportQ = false;
dirOut = '';    % must be specified if exportQ equals true

% Setup integral validation
lineLimits = [-1 -2 -3 -4 -5 -6 ;...
               6  5  4  3  2  1];
triangleLimits = [-1 -2 -3 -4 -5 -6 -7 -8 -9 ;...
                   9  8  7  6  5  4  3  2  1];
fun1 = @(x)     1;
fun2 = @(x,y)   x+y;
fun3 = @(x,y,z) x+y+z;
v_fun1l = @(x,y,z,i) fun1(x);
v_fun2l = @(x,y,z,i) fun2(x,y);
v_fun3l = @(x,y,z,i) fun3(x,y,z);
v_fun1t = @(x,y,z,i,j) fun1(x);
v_fun2t = @(x,y,z,i,j) fun2(x,y);
v_fun3t = @(x,y,z,i,j) fun3(x,y,z);
dimFun1 = 1;
dimFun2 = 2;
dimFun3 = 3;

% Setup BEM validation
rLimits = [0.5 5.5];
vLimits = [1 10];
height = 1.0;
divH = 9;
divV = 9;
coarseVsFine = 5;   % only uneven numbers allowed


%% GEOMETRY

% Create line and triangle with randowm coordinates
line = zeros(3,2);
triangle = zeros(3);
for ii = 1:3
    for jj = 1:2
        idx = (ii-1) * 2 + jj;
        line(ii,jj) = lineLimits(1,idx) + (lineLimits(2,idx) - lineLimits(1,idx)).*rand(1,1);
    end
    
    for jj = 1:3
        idx = (ii-1) * 3 + jj;
        triangle(ii,jj) = triangleLimits(1,idx) + (triangleLimits(2,idx) - triangleLimits(1,idx)).*rand(1,1);
    end
end
completeTriangle = [triangle triangle(:,1)];

radius = rLimits(1) + (rLimits(2)-rLimits(1)).*rand(1,1);
if (twoDimQ)
    % create cylinder
    [ptsCoarse,nodesCoarse,cenCoarse,~,sizeCoarse] = geom_2D_circle(radius,divH,0);
    [ptsFine,nodesFine,cenFine,~,sizeFine] = geom_2D_circle(radius,coarseVsFine*divH,0);
    
    % select points
    [ptsCoarse,cenCoarse,chosenPntsCoarse,idxCoarse,angleCoarse] = prepareForScript2D(ptsCoarse,cenCoarse,radius);
    [ptsFine,cenFine,chosenPntsFine,idxFine,angleFine] = prepareForScript2D(ptsFine,cenFine,radius);
else
    % create sphere
    [ptsCoarse,nodesCoarse,cenCoarse,~,sizeCoarse] = geom_3D_sphere(radius,divH,divV);
    [ptsFine,nodesFine,cenFine,~,sizeFine] = geom_3D_sphere(radius,coarseVsFine*divH,coarseVsFine*divV);
    
    % select random points
    rotAngle = randi([20 70],1,1);
    [ptsCoarse,cenCoarse,chosenPntsCoarse,idxCoarse,angleCoarse] = prepareForScript3D(ptsCoarse,cenCoarse,radius,divV,rotAngle);
    [ptsFine,cenFine,chosenPntsFine,idxFine,angleFine] = prepareForScript3D(ptsFine,cenFine,radius,coarseVsFine*divV,rotAngle);
end

% Complete information
elemVecCoarse = geom_op_calcElemVec(ptsCoarse,nodesCoarse,true);
elemVecFine = geom_op_calcElemVec(ptsFine,nodesFine,true);

% visual conformation
opts.showCenQ = true;
opts.showEleQ = false;
opts.showVecQ = false;
opts.showNodQ = false;

opts.figHandle = 1;
geom_show(ptsCoarse,nodesCoarse,cenCoarse,elemVecCoarse,[],opts);
hold on
plot3(chosenPntsCoarse(1,:),chosenPntsCoarse(2,:),chosenPntsCoarse(3,:),'xg');

opts.figHandle = 2;
geom_show(ptsFine,nodesFine,cenFine,elemVecFine,[],opts);
hold on
plot3(chosenPntsFine(1,:),chosenPntsFine(2,:),chosenPntsFine(3,:),'xg');


%% VALIDATION OF INTEGRATION

% preparations
triA = triangle(:,1);
triB = triangle(:,2);
triC = triangle(:,3);
lineA = triA;
lineB = triB;

triX = triangle(1,:);
triY = triangle(2,:);
triZ = triangle(3,:);
lineX = triangle(1,1:2);
lineY = triangle(2,1:2);
lineZ = triangle(2,1:2);

% Analytical solution, because fun(x,y,z) = 1 results in the length / area
% for a line / triangle
refSol(1) = norm(lineA - lineB);
refSol(2) = geom_op_calcElemSize(triangle,[1 ; 2 ; 3]);

% solution with self programmed function
mySol = zeros(12,6);
for order = 2:12
    tic
    mySol(order,2) =   int_line(fun2,dimFun2,order,lineB,lineB);
    mySol(order,3) =   int_triangle(fun3,dimFun3,order,triA,triB,triC);
    
    mySol(order,5) = v_int_line(v_fun2l,dimFun3,order,lineA,lineB);
    mySol(order,6) = v_int_triangle(v_fun3t,dimFun3,order,triA,triB,triC);
    toc
end

% Prepare user functions to use with Matlab
fl2 = @(x,y)   (inpolygon(x,y,lineX,lineY) .* fun2(x,y));
% fl3 = @(x,y,z) (inpolygon(x,y,lineX,lineY) .* fun3(x,y,z));
ft2 = @(x,y)   (inpolygon(x,y,triX,triY) .* fun2(x,y));
% ft3 = @(x,y,z) (inpolygon(x,y,triX,triY) .* fun3(x,y,z));

% Matlab solution with buildin function
tic
matlabSol(2,1) = integral2(ft2,min(lineX),max(lineX),min(lineY),max(lineY),'Method', 'iterated');
matlabSol(2,2) = integral2(ft2,min(triX),max(triX),min(triY),max(triY),'Method', 'iterated');
% matlabSol(3,1) = integral3(ft3,min(lineX),max(lineX),min(lineY),max(lineY),min(lineZ),max(lineZ),'Method', 'iterated');
% matlabSol(3,2) = integral3(ft3,min(triX),max(triX),min(triY),max(triY),min(triZ),max(triZ),'Method', 'iterated');
toc


%% VALIDATION OF BOUNDARY ELEMENT METHOD

% Preparations
vInf = vLimits(1) + (vLimits(2)-vLimits(1)).*rand(1,1);

% Running BEM
if (twoDimQ)
    potCoarseBEM = runBEM2D(ptsCoarse,nodesCoarse,cenCoarse,elemVecCoarse,sizeCoarse,-vInf,5,dirOut);
    potFineBEM = runBEM2D(ptsFine,nodesFine,cenFine,elemVecFine,sizeFine,-vInf,5,dirOut);
else
    potCoarseBEM = runBEM3D(ptsCoarse,nodesCoarse,cenCoarse,elemVecCoarse,sizeCoarse,-vInf,5,dirOut);
    potFineBEM = runBEM3D(ptsFine,nodesFine,cenFine,elemVecFine,sizeFine,-vInf,5,dirOut);
end

% Calculate potential distribution for selected points of the sphere
if (twoDimQ)
    potTheoryCoarse = potFlow_2D_cylinder(chosenPntsCoarse,radius,vInf);
    potInMotionTheoryCoarse = potFlow_2D_cylinderInMotion(chosenPntsCoarse,radius,-vInf);
    potUniformCoarse = potFlow_2D_uniform(chosenPntsCoarse,-vInf);
    potDoubletCoarse = potFlow_2D_doublet(chosenPntsCoarse,2*pi*vInf*radius^3);

    potTheoryFine = potFlow_2D_cylinder(chosenPntsFine,radius,vInf);
    potInMotionTheoryFine = potFlow_2D_cylinderInMotion(chosenPntsFine,radius,-vInf);
    potUniformFine = potFlow_2D_uniform(chosenPntsFine,-vInf);
    potDoubletFine = potFlow_2D_doublet(chosenPntsFine,2*pi*vInf*radius^3);
else
    potTheoryCoarse = potFlow_3D_sphere(chosenPntsCoarse,radius,vInf);
    potInMotionTheoryCoarse = potFlow_3D_sphereInMotion(chosenPntsCoarse,radius,vInf);
    potUniformCoarse = potFlow_3D_uniform(chosenPntsCoarse,-vInf);
    potDoubletCoarse = potFlow_3D_doublet(chosenPntsCoarse,2*pi*vInf*radius^3);

    potTheoryFine = potFlow_3D_sphere(chosenPntsFine,radius,vInf);
    potInMotionTheoryFine = potFlow_3D_sphereInMotion(chosenPntsFine,radius,vInf);
    potUniformFine = potFlow_3D_uniform(chosenPntsFine,-vInf);
    potDoubletFine = potFlow_3D_doublet(chosenPntsFine,2*pi*vInf*radius^3);
end

% Visualization
figure(3);
plot(angleCoarse,potCoarseBEM(idxCoarse,1),'ro', ...
     angleCoarse,potInMotionTheoryCoarse,'b*', ...
     angleCoarse,potTheoryCoarse + potUniformCoarse,'g-');

figure(4);
plot(angleFine,potFineBEM(idxFine,1),'kx', ...
     angleFine,potInMotionTheoryFine,'b*', ...
     angleFine,potTheoryFine + potUniformFine,'g-');

figure(5);
plot(angleCoarse,potCoarseBEM(idxCoarse,1),'ro', ...
     angleFine,potFineBEM(idxFine,1),'kx', ...
     angleFine,potInMotionTheoryFine,'b*', ...
     angleFine,potTheoryFine + potUniformFine,'g-');

radius
rotAngle
vInf
solTheory = [angleFine' potInMotionTheoryFine'];
solCoarse = [angleCoarse' potCoarseBEM(idxCoarse,1)];
solFine = [angleFine' potFineBEM(idxFine,1)];


%% EXPORT
if exportQ
    exportToCSV(solTheory,dirOut,length(cenFine),true,twoDimQ);
    exportToCSV(solCoarse,dirOut,length(cenCoarse),false,twoDimQ);
    exportToCSV(solFine,dirOut,length(cenFine),false,twoDimQ);
end


%% SUPPORT FUNCTIONS
function exportToCSV(src,dir,n,theoryQ,twoDimQ)

    if (twoDimQ)
        body = 'Cylinder';
    else
        body ='Sphere';
    end
    
    if (theoryQ)
        file = [dir 'fluidPot_theory' body '_validation.csv'];
    else
        file = [dir 'fluidPot_bem' body num2str(n) '_validation.csv'];
    end
    opts.head = {'angle,pot'};
    export_csv(file,src,opts);
end

function [newElemPts,newElemCen,chosenPnts,idx,angles] = prepareForScript2D(elemPts,elemCen,r)
    % Select points
    [chosenPnts,idx] = op_select('in',elemCen,'Y',[0 1.1*r]);
    
    % Calculate basis for comparison
    angles = rad2deg(atan2(chosenPnts(2,:),chosenPnts(1,:)));
   
    % Rotate sphere and selected points by a random rotational angle
    newElemPts = elemPts;
    newElemCen = elemCen;
end

function [newElemPts,newElemCen,chosenPnts,idx,angles] = prepareForScript3D(elemPts,elemCen,r,div,rot)
    % Preparation
    displacement = r/div/2;
    tmpCen = op_displace(elemCen,[0 0 -displacement]);
    
    % Select points
    [tmpPnts,idx] = op_select('in',tmpCen,'Z',[-0.05 0.05]);
    chosenPnts = op_displace(tmpPnts,[0 0 displacement]);
    
    % Calculate basis for comparison
    angles = rad2deg(atan2(chosenPnts(2,:),chosenPnts(1,:)));
   
    % Rotate sphere and selected points by a random rotational angle
    newElemPts = op_rotate(elemPts,'X','-',rot);
    newElemCen = op_rotate(elemCen,'X','-',rot);
    chosenPnts = op_rotate(chosenPnts,'X','-',rot);
end

function [pot] = runBEM3D(elemPts,elemNodes,elemCen,elemVec,elemSize,v,n,dir)
    opts.type = 'tri';
    opts.nInt = n;
    opts.dimG = 3;
    opts.dimI = 3;
    opts.prec = 'double';
    opts.save = dir;
    opts.prop = 'explicit';

    BC    = ai_v_bc_motion(elemCen,elemVec,v,opts);
    [L,M] = bem_v_LaplaceMatrices(elemPts,elemNodes,elemCen,elemVec,elemSize,opts);
    pot   = bem_solve(L,M,BC,opts);
end

function [pot] = runBEM2D(elemPts,elemNodes,elemCen,elemVec,elemSize,v,n,dir)
    opts.type = 'line';
    opts.nInt = n;
    opts.dimG = 2;
    opts.dimI = 2;
    opts.prec = 'double';
    opts.save = dir;
    opts.prop = 'explicit';

    BC    = ai_v_bc_motion(elemCen,elemVec,v,opts);
    [L,M] = bem_v_LaplaceMatrices(elemPts,elemNodes,elemCen,elemVec,elemSize,opts);
    pot   = bem_solve(L,M,BC,opts);
end