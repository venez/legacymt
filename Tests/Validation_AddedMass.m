clearvars
close all
clc


%% Add nessessary MATLAB files 
workDir = 'F:\MatLab\#Workspace\';
addpath(genpath([workDir 'Geometry\']));
addpath(genpath([workDir 'Integration\']));
addpath(genpath([workDir 'PotentialFlow\']));
addpath([workDir 'BEM\'], ...
        [workDir 'Export\'], ...
        [workDir 'Input\'], ...
        [workDir 'Mathematics\'], ...
        [workDir 'Operations\'], ...
        [workDir 'Output\'] ...
); 


%% SETTINGS
sphereQ = true;
wallQ = false;
exportSaveQ = false;
dirOut = [workDir 'Resources\BEM\'];

if (sphereQ) && (~wallQ) && (exportSaveQ)
    dirBEM = [dirOut 'Out_Sphere\Validation\'];
elseif (sphereQ) && (wallQ) && (exportSaveQ)
    dirBEM = [dirOut 'Out_Sphere_Wall\Validation\'];
elseif (exportSaveQ)
    dirBEM = [dirOut 'Out_Ellipsoid\Validation\'];
end

% Setup added mass validation
divC = 60;
divH = 5 : 5 : 15;
divL = 50;
divR = 60;
divV = divH;
wallToSphere = 5;
rLimits = [0.5 5.5];
theoryLimits = [1.0 10.0];
numericLimits = [1.1 10.0];
wallDistanceLimits = [1.2 4.0];
deltaTheory = 0.2;
deltaNumeric = 1.0;

ratioNumericValues = numericLimits(1) : deltaNumeric : numericLimits(end);
ratioTheoryValues = theoryLimits(1) : deltaTheory : theoryLimits(end);
wallDistanceNumericValues = wallDistanceLimits(1) : deltaNumeric/5 : wallDistanceLimits(end);
wallDistanceTheoryValues = wallDistanceLimits(1) : deltaTheory/10 : wallDistanceLimits(end);

% according to 'H. Lamb: The inertia-coefficients of an ellipsoid moving
% in fluid, 1918, NACA, Reports and Memoranda No. 623' the values of ratios
% would be : [1 1.5 2 2.51 2.99 3.99 4.99 6.01 6.97 8.01 9.02 9.97]


%% EXECUTION
% Preparations
nDivisionsSphere = length(divH);
nRatioNumericValues = length(ratioNumericValues);
nRatioTheoryValues = length(ratioTheoryValues);
nWallDistancesNumeric = length(wallDistanceNumericValues);
nWallDistancesTheory = length(wallDistanceTheoryValues);

if (sphereQ) && (~wallQ)
    % Initialisation
    dim = zeros(nDivisionsSphere,4);
    coeffsBEM = zeros(nDivisionsSphere,6);
    coeffsTheory = zeros(nDivisionsSphere,6);
    resultsBEM = zeros(6,6,nDivisionsSphere);
    
    for ii = 1 : nDivisionsSphere
        %% GEOMETRY
        radius = rLimits(1) + (rLimits(2)-rLimits(1)).*rand(1,1);
        [elemPts,elemNodes,elemCen,elemVec,elemSize] = geom_3D_sphere(radius,divH(ii),divV(ii));
        vol = 4 / 3 * pi * radius^3;
        dim(ii,1:2) = [radius ; vol];

        % Controlling
%         close all
%         opts.showCenQ = true;
%         opts.showEleQ = false;
%         opts.showVecQ = true;
%         opts.showNodQ = false;
%         opts.figHandle = 1; 
%         geom_show(elemPts,elemNodes,elemCen,elemVec,[],opts);
%         pause


        %% VALIDATION ADDED MASSES BY THE BOUNDARY ELEMENT METHOD
        [mh,pot,bc] = runSingleBodyBEM(elemPts,elemNodes,elemCen,elemVec,elemSize);
        resultsBEM(:,:,ii) = mh;

        % Saving
        [~,dim(ii,3)] = size(elemCen);
        dim(ii,4) = mean(elemSize);
        if (exportSaveQ)
            tmpFile = [dirBEM datestr(now,'yyyy-mm-dd') ' BEM_matrix_nElemOf_' num2str(dim(ii,3))];
            saveData(tmpFile,mh,pot,bc);
            saveModel(tmpFile,elemPts,elemNodes,elemCen,elemVec,elemSize);
        end

        % Theory values
        tmpMh = validationAddedMassSphere(radius);

        % Information of interest
        coeffsBEM(ii,:) = calcCoeffs(diag(mh),radius,radius,radius);
        coeffsTheory(ii,:) = calcCoeffs(tmpMh',radius,radius,radius);
    end
    
    % Saving results
    if (exportSaveQ)
        tmpFile = [dirBEM datestr(now,'yyyy-mm-dd')];
        save_MatLabMatrix(resultsBEM,[tmpFile ' BEM_matrix_AddedMassForElemVar.mat']);
    end
    
    
    %% COMPARISON
    figure(1)
    plot(dim(:,3),coeffsTheory(:,1),'k', ...
         dim(:,3),coeffsTheory(:,2),'r', ...
         dim(:,3),coeffsTheory(:,3),'b');
    hold on
    plot(dim(:,3),coeffsBEM(:,1),'ko', ...
         dim(:,3),coeffsBEM(:,2),'rx', ...
         dim(:,3),coeffsBEM(:,3),'b*');
    legend('k11 Theory','k22 Theory','k33 Theory','k11 BEM','k22 BEM','k33 BEM');

    % Export
    solTheory = [dim(:,3) coeffsTheory];
    solBEM = [dim(:,3) coeffsBEM];
    if (exportSaveQ)
        exportToCSV(solTheory,dirBEM,0,true,sphereQ,wallQ);
        exportToCSV(solBEM,dirBEM,0,false,sphereQ,wallQ);
    end
    
elseif (sphereQ) && (wallQ)
    % Initialisation
    dim = zeros(nWallDistancesNumeric,5);
    coeffsBEM = zeros(nWallDistancesNumeric,6);
    coeffsTheory = zeros(nWallDistancesTheory,6);
    resultsBEM = zeros(6,6,nWallDistancesNumeric);
    
    for ii = 1 : nWallDistancesNumeric
        %% GEOMETRY
        radius = rLimits(1) + (rLimits(2)-rLimits(1)).*rand(1,1);
        [elemPtsS,elemNodesS,elemCenS,elemVecS,elemSizeS] = geom_3D_sphere(radius,divC,divC);

        % Walls
        radiusW = wallToSphere * radius;
        [elemPtsW,elemNodesW,elemCenW,elemVecW,elemSizeW] = geom_2D_plate_circle(radiusW,divC,divR,0.0);
        vol = 4 / 3 * pi * radius^3;
        distance = wallDistanceNumericValues(ii)*radius;
        elemPtsW = op_displace(elemPtsW,[0 0 distance]);
        elemCenW = op_displace(elemCenW,[0 0 distance]);

        % Merge information
        [~,n]     = size(elemPtsS);
        elemPts   = [elemPtsS elemPtsW];
        elemNodes = [elemNodesS elemNodesW + n];
        elemCen   = [elemCenS elemCenW];
        elemVec   = [elemVecS -elemVecW];
        elemSize  = [elemSizeS elemSizeW];
        [~,nS]    = size(elemCenS);
        [~,nW]    = size(elemCenW);
        [~,n]     = size(elemCen);

        % Collection of information
        dim(ii,1:5) = [wallDistanceNumericValues(ii) ; distance; radius ; radiusW ; vol];

        % Controlling
%         close all
%         opts.showCenQ = true;
%         opts.showEleQ = false;
%         opts.showVecQ = true;
%         opts.showNodQ = false;
%         opts.figHandle = 1; 
%         geom_show(elemPts,elemNodes,elemCen,elemVec,[],opts);
%         pause


        %% VALIDATION ADDED MASSES BY THE BOUNDARY ELEMENT METHOD
        [mh,pot,bc] = runDoubleBodyBEM(elemPts,elemNodes,elemCen,elemVec,elemSize,nS);
        resultsBEM(:,:,ii) = mh;

        % Saving
        tmpFile = [dirBEM datestr(now,'yyyy-mm-dd') ' BEM_matrix_distanceOf_' num2str(dim(ii,1))];
        if (exportSaveQ)
            saveData(tmpFile,mh,pot,bc);
            saveModel(tmpFile,elemPts,elemNodes,elemCen,elemVec,elemSize);
        end

        % Information of interest
        coeffsBEM(ii,:) = calcCoeffs(diag(mh),radius,radius,radius);

    end
    
    % Theory values
    for ii = 1 : nWallDistancesTheory
        radius = rLimits(1) + (rLimits(2)-rLimits(1)).*rand(1,1);
        distance = wallDistanceTheoryValues(ii)*radius;
        tmpMh = validationAddedMassSphereWall(radius,distance);
        coeffsTheory(ii,:) = calcCoeffs(tmpMh',radius,radius,radius);
    end
    
    % Save total information
    if (eqportQ)
        tmpFile = [dirBEM datestr(now,'yyyy-mm-dd')];
        save_MatLabMatrix(resultsBEM,[tmpFile ' BEM_matrix_AddedMassForDistanceVar.mat']);
    end
    
    
    %% COMPARISON
    figure(1)
    plot(wallDistanceTheoryValues,coeffsTheory(:,1),'k', ...
         wallDistanceTheoryValues,coeffsTheory(:,3),'b');
    hold on
    plot(wallDistanceNumericValues,coeffsBEM(:,1),'ko', ...
         wallDistanceNumericValues,coeffsBEM(:,3),'b*');
    legend('k11 Theory','k33 Theory','k11 BEM','k33 BEM');

    % Export
    solTheory = [wallDistanceTheoryValues' coeffsTheory];
    solBEM = [wallDistanceNumericValues' coeffsBEM];
    if (exportSaveQ)
        exportToCSV(solTheory,dirBEM,0,true,sphereQ,wallQ);
        exportToCSV(solBEM,dirBEM,length(elemCen),false,sphereQ,wallQ);
    end
else
    % Initialisation
    dim = zeros(nRatioNumericValues,6);
    coeffsBEM = zeros(nRatioNumericValues,6);
    resultsBEM = zeros(6,6,nRatioNumericValues);

    for ii = 1:nRatioNumericValues
        %% GEOMETRY
        a = rLimits(1) + (rLimits(2)-rLimits(1)).*rand(1,1);
        b = a / ratioNumericValues(ii);
        c = b;

        % Ellipsoids
        [elemPts,elemNodes,elemCen,elemVec,elemSize] = geom_3D_ellipsoid(a,b,c,divC,divL);
        dim(ii,:) = [a ; b ; c ; b/a ; c/a ; b/c];

        % Controlling
%         close all
%         opts.showCenQ = true;
%         opts.showEleQ = false;
%         opts.showVecQ = true;
%         opts.showNodQ = false;
%         opts.figHandle = 1; 
%         geom_show(elemPts,elemNodes,elemCen,elemVec,[],opts);
%         pause


        %% VALIDATION ADDED MASSES BY THE BOUNDARY ELEMENT METHOD
        [mh,pot,bc] = runSingleBodyBEM(elemPts,elemNodes,elemCen,elemVec,elemSize);
        resultsBEM(:,:,ii) = mh;

        % Saving
        tmpFile = [dirBEM datestr(now,'yyyy-mm-dd') ' BEM_matrix_ratioOf_' num2str(ratioTheoryValues(ii),2)];
        if (exportSaveQ)
            saveData(tmpFile,mh,pot,bc);
            saveModel(tmpFile,elemPts,elemNodes,elemCen,elemVe,elemSize);
        end
        
        % Information of interest
        coeffsBEM(ii,:) = calcCoeffs(diag(mh,a,b,c));
    end
    
    % Save total information
    [~,n] = size(elemCen);
    tmpFile = [dirBEM datestr(now,'yyyy-mm-dd')];
    if (exportSaveQ)
        save_MatLabMatrix(resultsBEM,[tmpFile ' BEM_matrix_AddedMassForRatioVar_' num2str(n) '.mat']);
    end
    
    
    %% COMPARISON
    coeffsTheory = zeros(nRatioTheoryValues,6);
    for ii = 1:nRatioTheoryValues
        c = rLimits(1) + (rLimits(2)-rLimits(1)).*rand(1,1);
        a = c / ratioTheoryValues(ii);
        b = a;

        coeffsTheory(ii,:) = validationAddedMassEllipsoid(a,b,c);
    end

    % Visualization
    figure(1)
    plot(ratioTheoryValues,coeffsTheory(:,1),'k', ...
         ratioTheoryValues,coeffsTheory(:,2),'r', ...
         ratioTheoryValues,coeffsTheory(:,5),'b');
    hold on
    plot(ratioNumericValues,coeffsBEM(:,1),'ko', ...
         ratioNumericValues,coeffsBEM(:,2),'rx', ...
         ratioNumericValues,coeffsBEM(:,5),'b*');
    legend('k11 Theory','k22 Theory','k55 Theory','k11 BEM','k22 BEM','k55 BEM');

    % Export
    solTheory = [ratioTheoryValues' coeffsTheory];
    solBEM = [ratioNumericValues' coeffsBEM];
    if (exportSaveQ)
        exportToCSV(solTheory,dirBEM,length(elemCen),true,sphereQ,wallQ);
        exportToCSV(solBEM,dirBEM,length(elemCen),false,sphereQ,wallQ);
    end
end


%% SUPPORT FUNCTIONS
function [k,m,J] = calcCoeffs(src,a,b,c)
    % mass and inertia moments are normed against density
    
    k = zeros(1,6);
    m = 4/3 * pi * a * b * c;
    J = 4 / 15 * pi * a * b * c * [b^2 + c^2 ; a^2 + c^2 ; a^2 + b^2];
    
    k(1:3) = src(1:3) / m;
    k(4:6) = src(4:6) ./ J(1:3);
end

function exportToCSV(src,dir,n,theoryQ,sphereQ,wallQ)

    if (sphereQ) && (~wallQ)
        body = 'Sphere';
    elseif (sphereQ) && (wallQ)
        body ='SphereAtWall';
    else
        body ='Ellipsoid';
    end
    
    if (theoryQ)
        file = [dir 'fluidAddedMass_theory' body '_validation.csv'];
    else
        file = [dir 'fluidAddedMass_bem' body num2str(n) '_validation.csv'];
    end
    opts.head = {'c/a,k11,k22,k33,k44,k55,k66'};
    export_csv(file,src,opts);
end

function [gamma,alpha,e] = lambProps(a,c)
    % according to H. Lamb: The inertia-coefficients of an ellipsoid moving
    % in fluid, 1918, NACA, Reports and Memoranda No. 623
    e = sqrt(1 - a^2 / c^2);

    tmp = 1 - e^2;
    tmpP = 1 + e;
    tmpN = 1 - e;
    tmpLog = log(tmpP/ tmpN);
    
    gamma = 2 * tmp / e^3 * (1/2 * tmpLog - e);
    alpha = 1/e^2 - tmp / 2 / e^3 * tmpLog;
end

function [mh,pot,bc] = runSingleBodyBEM(elemPts,elemNodes,elemCen,elemVec,elemSize)
    opts.type = 'tri';
    opts.nInt = 5;
    opts.dimG = 3;
    opts.dimI = 3;
    opts.prec = 'double';
    opts.prop = 'explicit';
    
    [mh,pot,bc] = bem_v_addedMass(elemPts,elemNodes,elemCen,elemVec,elemSize,opts);
end

function [mh,pot,bc] = runDoubleBodyBEM(pts,nodes,cen,vec,sizes,n0)
    [~,n] = size(cen);
    opts.type = 'tri';
    opts.nInt = 5;
    opts.dimG = 3;
    opts.dimI = 3;
    opts.prec = 'double';
    opts.prop = 'explicit';
    
    BC	= bem_v_bc_addedMass(cen(:,1:n0),vec(:,1:n0));
    bc  = [BC ; zeros(n-n0,6)];
    [L,M] = bem_v_LaplaceMatrices(pts,nodes,cen,vec,sizes,opts);
    pot   = bem_solve(L,M,bc,opts);

    mh = zeros(6,6);
    for i = 1:6
        for j = i:6
            mh(i,j) = -sum(pot(1:n0,i).*BC(:,j).*sizes(1,1:n0)');        
        end
    end
    mh = mh + triu(mh,+1)';
end

function saveData(file,mh,pot,bc)
    save_MatLabMatrix(mh,[file '_AddedMass.mat']);
    save_MatLabMatrix(pot,[file '_Pot.mat']);
    save_MatLabMatrix(bc,[file '_BC.mat']);
end

function saveModel(file,pts,nodes,cen,vec,sizes)
    save_bem_Model([file '_model.mat'],pts,nodes,cen,vec,sizes);
    export_stl([file '_model.stl'],pts,nodes,vec);
end

function [lambda] = validationAddedMassEllipsoid(a,b,c)
    if (a ~= b)
        error('First two arguments must be the same acc. to source');
    end

    % according to H. Lamb: The inertia-coefficients of an ellipsoid moving
    % in fluid, 1918, NACA, Reports and Memoranda No. 623
    lambda = zeros(1,6);

    [gamma,alpha,e] = lambProps(a,c);
    tmp2e = 2 - e^2;
    tmpDT = alpha - gamma;
    
    lambda(1) = gamma / (2 - gamma);    % for moving end-on (in X)
    lambda(2) = alpha / (2 - alpha);    % for moving broadside
    lambda(3) = lambda(2);
    lambda(4) = 0;
    lambda(5) = e^4 * tmpDT / (tmp2e * (2 * e^2 - tmp2e * tmpDT));
    lambda(6) = lambda(5);              % for rotation
end

function [lambda] = validationAddedMassSphere(r)
    % according to A. Korotkin: Added Mass of Ship Structures, pp. 86
    lambda(1,1:3) = 2/3 * pi * r^3;
    lambda(1,4:6) = 0.0;
end

function [lambda] = validationAddedMassSphereWall(r,h)
    % according to A. Korotkin: Added Mass of Ship Structures, pp. 110 - 111
    d = r / h;
    lambda = zeros(1,6);
    
    % parallel to wall
    lambda(1,1) = 2/3 * pi * r^3 * (1 + 3/16 * d^3 + 3/2^8 * d^6 + ...
                  3/2^8 * d^8 + 3/2^12 * d^9 + 27/2^12 * d^10 + ...
                  3/2^11 * d^11 + 195 / 2^16 * d^12);
    % orthogonal to wall
	lambda(1,3) = 2/3 * pi * r^3 * (1 + 3/8 * d^3 + 3/2^6 * d^6 + ...
                  9/2^8 * d^8 + 3/2^9 * d^9 + 9/2^9 * d^10 + ...
                  9/2^10 * d^11 + 33 / 2^12 * d^12);
end