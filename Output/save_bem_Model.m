% save_bem_Model(elemPts,elemNodes,elemCen,elemVec,elemSize,opts)
% 
%	Function saves a BEM conform model specified by the variables 'elemPts'
%	, 'elemNodes', 'elemCen', 'elemVec' and 'elemSize'. The optional
%   argument 'opts' contains the possibility to specify a filename and
%   .MAT-version. For an empty or non specified field 'file' the function 
%   opens a dialog.
% 
%   Fields permitted for 'opts':
%       vers    - default = '-v7.3'
% 
%   Example:
%       file = 'C:\Documents\bem_model';
%       opts.vers = '-v6';
%       save_bem_Model(file,elemPts,elemNodes,elemCen,elemVec,elemSize,opts);
% 
function save_bem_Model(file,elemPts,elemNodes,elemCen,elemVec,elemSize,opts)

% Input handling
default.vers = '-v7.3';

switch nargin
    case 6
        options = default;
    case 7
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end


% Preparations
if (isempty(file))
    [FileName,PathName] = uiputfile('*.mat','Select the MATLAB matrices');
    location = [PathName FileName];
else
    location = file;
end


% Execution
save(location,'elemPts','elemNodes','elemCen','elemVec','elemSize',...
     options.vers);

end