% save_MatLabMatrix(src,filename,opts)
% 
%	Function saves a specified variable 'src' with user-defined options
%	'opts'. Latter contains the possibility to specify a header and a
%	.MAT-version. For an empty argument 'filename' the function opens a 
%   dialog.
% 
%   Fields permitted for 'opts':
%       head    - default = {};
%       vers    - default = '-v7.3'
% 
%   Example:
%       filename = 'C:\Documents\matlabMatrix';
%       opts.header = {contains data};
%       opts.vers = '-v6';
%       save_MatLabMatrix(results,opts);
% 
function save_MatLabMatrix(filename,src,opts)


% Input handling
default.head = {};
default.vers = '-v7.3';
switch nargin
    case {1,2}
        options = default;
    case 3
        options = completeInput(opts,default);
    otherwise
        error('Unknown number of input arguments');
end


% Preparations
if (isempty(filename))
    [FileName,PathName] = uiputfile('*.mat','Save as MATLAB matrix');
    location = [PathName FileName];
else
    location = filename;
end


% Execution
if isempty(options.head)
    save(location,'src',options.vers);
else
    if iscell(options.head)
        header = options.head;
    else
        header = {options.head};
    end
    save(location,'src','header',options.vers);
end

end